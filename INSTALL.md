# Installation of OEB Scientific admin python tools

## Latest commit in current python environment

```bash
pip install --force-reinstall git+https://gitlab.bsc.es/inb/elixir/openebench/oeb-sci-admin-tools.git
```

## Installation of an specific "stable" version 

```bash
pip install git+https://gitlab.bsc.es/inb/elixir/openebench/oeb-sci-admin-tools.git@v0.9.0
```

## Declaration of dependency line in a `requirements.txt` file

```
# Old style
git+https://gitlab.bsc.es/inb/elixir/openebench/oeb-sci-admin-tools.git@v0.9.0#egg=oeb_scientific_admin_tools
# Newer style
oeb_scientific_admin_tools @ git+https://gitlab.bsc.es/inb/elixir/openebench/oeb-sci-admin-tools.git@v0.9.0
```

## Development install (it requires having venv module properly installed)

```bash
git clone https://gitlab.bsc.es/inb/elixir/openebench/oeb-sci-admin-tools.git
cd oeb-sci-admin-tools
python -mvenv .sci
source .sci/bin/activate
pip install --upgrade pip wheel
pip install -r requirements.txt
pip install -r dev-requirements.txt
pre-commit install
```
