#!/usr/bin/env python
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2023 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import re
import sys

import setuptools

# In this way, we are sure we are getting
# the installer's version of the library
# not the system's one
setupBaseDir = os.path.dirname(__file__)
sys.path.insert(0, setupBaseDir)

from oebtools import version as oebtools_version

# Populating the long description
with open(os.path.join(setupBaseDir, "README.md"), "r", encoding="utf-8") as fh:
	long_description = fh.read()

# Populating the install requirements
with open(os.path.join(setupBaseDir, "requirements.txt"), "r", encoding="utf-8") as f:
	requirements = []
	egg = re.compile(r"#[^#]*egg=([^=&]+)")
	for line in f.read().splitlines():
		m = egg.search(line)
		requirements.append(line if m is None else m.group(1))


setuptools.setup(
	name="oeb_scientific_admin_tools",
	version=oebtools_version,
	scripts=[
		"data-attacher.py",
		"data-detacher.py",
		"oeb-differ.py",
		"oeb-dumper.py",
		"oeb-migrator.py",
		"oeb-sandbox.py",
		"oeb-uploader.py",
		"oeb-participant-publish.py",
	],
	author="José Mª Fernández",
	author_email="jose.m.fernandez@bsc.es",
	description="OpenEBench Scientific shared python libraries and admin tools",
	license="Apache-2.0",
	long_description=long_description,
	long_description_content_type="text/markdown",
	url="https://gitlab.bsc.es/inb/elixir/openebench/oeb-sci-admin-tools",
	project_urls={
		"Bug Tracker": "https://gitlab.bsc.es/inb/elixir/openebench/oeb-sci-admin-tools/-/issues"
	},
	packages=setuptools.find_packages(),
	package_data={
		"": [
			"oeb_api_auth.json.template",
			"migration-examples",
		],
		"oebtools": [
			"py.typed",
		],
	},
	include_package_data=True,
	install_requires=requirements,
	entry_points={
		"console_scripts": [
			"be_offline_aggregation_generator=oebtools.cli.be_offline_aggregation_generator:main",
			"data-attacher=oebtools.cli.data_attacher:main",
			"data-detacher=oebtools.cli.data_detacher:main",
			"oeb-challenge-dumper=oebtools.cli.oeb_challenge_dumper:main",
			"oeb-differ=oebtools.cli.oeb_differ:main",
			"oeb-dumper=oebtools.cli.oeb_dumper:main",
			"oeb-migrator=oebtools.cli.oeb_migrator:main",
			"oeb-participant-publish=oebtools.cli.oeb_participant_publish:main",
			"oeb-payload-uploader=oebtools.cli.oeb_payload_uploader:main",
			"oeb-sandbox=oebtools.cli.oeb_sandbox:main",
			"oeb-uploader=oebtools.cli.oeb_uploader:main",
		],
	},
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: Apache Software License",
		"Operating System :: OS Independent",
	],
	python_requires=">=3.6",
)
