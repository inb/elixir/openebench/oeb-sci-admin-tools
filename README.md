# OEB Scientific admin tools which help on consolidation and migration among different OpenEBench instances

These tools (and the underlying libraries) help on management of the OpenEBench scientific benchmarking
database entries management and lifecycle.

See [INSTALL.md](INSTALL.md) in order to learn the different installation modes.

# Usages

## Usage 1: Checking for collisions

This script checks the collision at the identifier level

```bash
python oeb-differ.py --url https://dev-openebench.bsc.es/api/scientific/staged --dest-url https://openebench.bsc.es/api/scientific/access/ --output-dir Comparison
```

and it will provide both the number of collisions, new entries and entries unique in the dest URL (considered as old).

The output directory will contain a directory for each concept, with at most four files: ids in origin, ids in destination, collisions and new entries.

## Usage 2: Getting a copy of the whole database

```bash
python oeb-dumper.py --url 'https://dev-openebench.bsc.es/api/scientific/staged/' -cr oeb_api_auth_new.json --output-dir DEV
python oeb-dumper.py --url https://openebench.bsc.es/api/scientific/access/ -cr oeb_api_auth_new.json --output-dir OLD
```

The credentials file describes what it is needed to obtain a valid bearer token in order to use the REST API.
It should have the same structure as the template [oeb_api_auth.json.template](oeb_api_auth.json.template).
The credentials should have enough grant access to obtain ALL the entries, otherwise the program is only getting the entries which can be fetched by that user.


The output directory will contain a directory for each concept, with a file for each entry.

## Usage 3: Validating a local copy of the whole OpenEBench database

Create a environment in order to install the needed dependencies:

```bash
python3 -m venv .m
source .m/bin/activate
pip install --upgrade pip wheel

# Optionally install the extended JSON validator
# It is needed if you have not installed this package yet
# (as it has it as dependency)
pip install -r requirements.txt

# This is only needed to fetch the schemas
git clone -b 2.0.x https://github.com/inab/benchmarking-data-model.git benchmarking-data-model_2.0.x
```

This is to validate the DEV entries against Benchmarking Data Model 1.0.
The report file will have only the entries related to troublesome files,
describing both the errors it found and the `_id` of the entry.

```bash
ext-json-validate --report DEV-err-eport.json --error-report --annotation '$._id' -q benchmarking-data-model_2.0.x/json-schemas/1.0.x DEV
```

If you want to validate only an specific kind of entries (for instance `Dataset`), then you have to create a JSON file with next structure

```json
{
	"primary_key": {
		"provider": [
			"https://dev-openebench.bsc.es/api/scientific/staged/"
		],
		"allow_provider_duplicates": true,
		"schema_prefix": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/",
		"accept": "text/uri-list"
	}
}
```

If the configuration file is named `ejv.conf`, then you have to use next command line:

```bash
ext-json-validate -C ejv.conf --report DEV-err-eport.json --error-report --annotation '$._id' -q benchmarking-data-model_2.0.x/json-schemas/1.0.x DEV
```

The drawback is you are getting lots of duplicated primary key validation errors.

## Usage 4: Migration from one set of JSON Schemas to another

First, you need to have a proper definition of the transformations to be applied. The transformation scripts are written in [jq](https://stedolan.github.io/jq/). There are several examples at [migration-examples](migration-examples).

Once the transformation script is ready, the execution is like:

```bash
python oeb-migrator.py --input DEV/Tool/entries-json.array --recipes to_2.0.x.yaml --output-dir TOMA
```

or

```bash
python oeb-migrator.py --input C/Tool/000000 --recipes to_2.0.x.yaml --output-dir TOMA2
```

A naïve transformation scenario is changing the value of the `_schema` key, which is obtained with next yaml recipes file:

```yaml
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/BenchmarkingEvent
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/BenchmarkingEvent"
    }
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/Challenge
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/Challenge"
    }
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/Community
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/Community"
    }
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/Contact
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/Contact"
    }
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/Dataset
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/Dataset"
    }
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/idSolv
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/idSolv"
    }
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/Metrics
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/Metrics"
    }
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/Reference
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/Reference"
    }
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/TestAction
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/TestAction"
    }
- schema: https://www.elixir-europe.org/excelerate/WP2/json-schemas/1.0/Tool
  script: |-
    . as {
        "_id": $id
    }
    |
    . * {
        "_id": $id,
        "_schema": "https://www.elixir-europe.org/excelerate/WP2/json-schemas/2.0/Tool"
    }
```

## Usage 5: sandbox management

Report (default _stdout_) is populated with server answers, in any of the cases.
You should use `--report` parameter in order to save it to a file, for further processing.

### Sandbox status

```bash
python oeb-sandbox.py --base_url https://dev-openebench.bsc.es/api/scientific -cr oeb_api_auth_new.json status
```

### Discard sandbox contents

```bash
python oeb-sandbox.py --base_url https://dev-openebench.bsc.es/api/scientific -cr oeb_api_auth_new.json discard
```

### Dry-run the stage process from sandbox to staged

```bash
python oeb-sandbox.py --base_url https://dev-openebench.bsc.es/api/scientific -cr oeb_api_auth_new.json dry-run
```

### Transfer from sandbox to staged

```bash
python oeb-sandbox.py --base_url https://dev-openebench.bsc.es/api/scientific -cr oeb_api_auth_new.json stage
```

## Usage 6: uploading with validation

Error report (default `error-report.json`) is produced either on validation errors or failed server answers.

### Dry-run (no operation)

```bash
python oeb-uploader.py -cr oeb_api_auth_new.json --trust-rest-bdm --dry-run DEV/Dataset/alphaentry.json
```

### Upload (selectively uploads to sandbox or staged, needs a community id)

```bash
python oeb-uploader.py -cr oeb_api_auth_new.json --trust-rest-bdm --community-id OEBC001 DEV/Dataset/alphaentry.json
```

### Entries removed by id (when API is available)

```bash
python oeb-uploader.py -cr oeb_api_auth_new.json --trust-rest-bdm --community-id OEBC001 --rm-ids-file file_with_ids.txt
```

## Usage 7: attaching a data URL to an existing JSON (to be chained to other previously explained patterns)

The `data-attacher.py` script can be used standalone to attach a file as a data URL into a JSON. For instance, if there is a file called `new.json` with next content:

```json
{
    "links": [
        {
            "label": "hola",
            "comment": "nothing to declare"
        },
        {
            "label": "other",
            "comment": "@logo"
        }
    ]
}
```

and you want to encode a file (for instance `image.jpg`) as a [data URL](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URLs) and attach it as a value to a new `uri` key in the second element of the `"links"` array, the usage would be:

```bash
python data-attacher.py --json-file new.json --jq-expr '.links[1].uri' -o new-attached.json image.jpg
```

where the `--jq-expr` parameter tells where in the JSON the data URL has to be assigned.

In order to attach files as data URLs to existing OpenEBench entries, avoiding errors, it is recommended to first fetch the entry:

```bash
# This one will download the file to Community_entries/OEBC009_entry.json
python oeb-dumper.py --base_url https://dev-openebench.bsc.es/api/scienfic/ -cr oeb_api_auth_new.json --concepts --input-id OEBC009

# Or
curl -o OEBC009.json https://dev-openebench.bsc.es/api/scientific/public/Community/OEBC009
```

Then attach the whole file translated to a data URL as a property. For instance, a fetched file to `._metadata.summary_url`:

```bash
curl -o OEBC009-summary.md https://raw.githubusercontent.com/inab/openEBench-nuxt/4db678412dc45b087025e1a718bec2d774fab2f8/static/markdown/projects/OEBC009.md
python data-attacher.py --json-file Community_entries/OEBC009_entry.json --jq-expr '._metadata.summary_url' -o OEBC009-updated.json OEBC009-summary.md
```

And last, follow the pattern of upload with validation (first doing a dry-run):

```bash
python oeb-uploader.py --base_url https://dev-openebench.bsc.es/api/scienfic/ -cr oeb_api_auth_new.json --trust-rest-bdm --dry-run OEBC009-updated.json
python oeb-uploader.py --base_url https://dev-openebench.bsc.es/api/scienfic/ -cr oeb_api_auth_new.json --trust-rest-bdm --community-id OEBC009 OEBC009-updated.json
```

## Usage 8: decoding a data URL embedded in an existing JSON

If we have a file generated using the techniques from previous examples, and we want to materialize the data url into a file, the command would be:

```bash
python data-detacher.py --json-file new-attached.json --jq-expr '.links[1].uri' -o compara.jpg
```

The program should print the name of the output file (provided through `-o` parameter) and the number of written bytes.

## Usage 9: upload a payload

If we have some serious payload which should be kept along with the database,
for instance a too large inline data (>32MB) from a Dataset, it should be transferred to the payload section of the instance.

As a payload may have associated metadata, the program looks for a file with the very same name and the extension `.metadata`.

```bash
python oeb-payload-uploader.py --base_url https://openebench.bsc.es/api/scientific -cr oeb_api_auth_new.json README.md
```

The created URL depends on the content of the file (i.e. its bytes) instead of the original name, as it is a [nih URI](https://webconcepts.info/concepts/uri-scheme/nih) as defined at [RFC6920](https://www.rfc-editor.org/rfc/rfc6920.html)

## Usage 10: fetch the subgraph of related entries from a given challenge id, patching the needed dataset entries referring other challenges or communities.

```bash
python oeb-challenge-dumper.py --base_url https://openebench.bsc.es/api/scientific -cr oeb_api_auth_new.json --output-dir OEBX002000001C_entries --input-id OEBX002000001C
```

You can check the coherence of the subset of related entries just using `ext-json-validate` and a local copy of the [benchmarking-data-model](https://github.com/inab/benchmarking-data-model):

```bash
ext-json-validate -c ~/projects/OpenEBench/benchmarking-data-model/json-schemas/1.0.x  OEBX002000001C_entries/
```

If the number of errors is more than 0, please contact both the author of the tool and OpenEBench team, as it could be either an unforseen corner case, an issue in the database contents or some lack of privileges in the provided credentials.

## Usage 11: publish participant datasets in B2SHARE.

In case one or more participant datasets from an specific benchmarking event have to be publicly shared through [B2SHARE](https://b2share.eudat.eu) at the [OpenEBench community](https://b2share.eudat.eu/communities/OpenEBench), you need to use next tool. These are the pre-requisites:

1. You need to have a B2SHARE user who is included in the OpenEBench community. You might need to create an user for <https://b2share.eudat.eu> (production) as well as <https://trng-b2share.eudat.eu> (sandbox).

2. You need to create an application token for each service, which needs to be included in your credentials file.

3. You should have two credentials files, one for B2SHARE production and another for B2SHARE sandbox. Credentials files have to include the `publishServer` block, properly set up for either production or sandbox:
  
  ```json
          "publishServer": {
                  "type": "b2share",
                  "token": "SANDBOX_PUBLISH_TOKEN",
                  "sandbox": true
          }
  ```
  
  so, if `sandbox` property is `true`, the token generated from <https://trng-b2share.eudat.eu> should be used, otherwise the generated one from <https://b2share.eudat.eu>.

The program is called in this way:

```bash
python oeb-participant-publish.py -cr oeb_api_auth_new_sandbox.json --trust-rest-bdm --input-id OEBE0020000003 --dataset-uri-prefix https://dev-openebench.bsc.es/nextcloud/
```

where the credentials file is provided. What is going to be processed is controlled through the id of the benchmarking event to process (provided with `--input-id`), and the `--dataset-uri-prefix`. This last one is used to filter out participant dataset entries whose `datalink.uri` value does not start with this prefix. 

This is the internal work of the tool:

1. The participant dataset "winner" entries are first grouped by `datalink.uri`, as more than one participant dataset could depend on the very same resource.

2. Then, a draft entry is created in the corresponding B2SHARE service.

3. The contents behind `datalink.uri` are fetched.

4. The fetched contents are uploaded to the corresponding B2SHARE service, being associated to the draft entry.

5. Metadata associated both to the dataset, challenge and benchmarking event, and related contacts are used to generate the metadata which is attached to the draft entry.

6. The draft entry is published, obtaining a DOI link as result.

7. DOI link is used to substitute the previous value of `datalink.uri` in the affected participant datasets, preparing updated entries. A copy of the previous value at `datalink.uri` is preserved under `_metadata` dictionary of the entry.

8. The updated participant dataset entries are uploaded to OpenEBench.

At the end, all the properly processed datasets are residing in OpenEBench sandbox, so you have to use `oeb-sandbox.py` (explained above) to confirm their migration to the staged database.

Sometimes B2SHARE can have intermittent issues, so the failing B2SHARE entry could be either in draft state or even properly created. These cases usually require manual intervention: the former, removing the draft entry from the B2SHARE service; the latter, updating the value of `datalink.uri` by hand.