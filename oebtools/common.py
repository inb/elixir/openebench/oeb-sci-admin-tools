#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

from dataclasses import dataclass
import gzip
import json
import logging
import os
import pathlib
import sys
import tempfile

from typing import (
	cast,
	overload,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	from types import ModuleType

	from typing import (
		Any,
		Callable,
		IO,
		Iterable,
		Iterator,
		Mapping,
		MutableSequence,
		Optional,
		Sequence,
		Tuple,
		Type,
		TypeVar,
		Union,
	)

	from typing_extensions import (
		Literal,
		TypeAlias,
	)

	from mypy_extensions import (
		NamedArg,
		DefaultNamedArg,
	)

	IOMode = TypeVar("IOMode")
	BasicOpener: TypeAlias = Callable[
		[
			Union[str, os.PathLike[str]],
			NamedArg(str, "mode"),
			DefaultNamedArg(Optional[str], "encoding"),
		],
		IO[IOMode],
	]
	BasicAnyOpener: TypeAlias = Union[BasicOpener[bytes], BasicOpener[str]]

import yaml


DEFAULT_BASEURL = "https://dev-openebench.bsc.es/api/scientific/"

FLAVOR_STAGED = "staged"
FLAVOR_SANDBOX = "sandbox"
DEFAULT_FLAVOR = FLAVOR_STAGED

DEFAULT_BDM_TAG = "1.0.x"

if sys.version_info >= (3, 11):
	FakingSpooledTemporaryFile = tempfile.SpooledTemporaryFile
else:
	# https://stackoverflow.com/q/47160211

	class FakingSpooledTemporaryFile(tempfile.SpooledTemporaryFile):  # type: ignore
		@property
		def readable(self) -> "Any":
			return self._file.readable  # type: ignore

		@property
		def writable(self) -> "Any":
			return self._file.writable  # type: ignore

		@property
		def seekable(self) -> "Any":
			return self._file.seekable  # type: ignore


@dataclass
class YieldedInput:
	content: "Any"
	path: "Union[pathlib.Path, str]"
	payload: "Optional[FakingSpooledTemporaryFile]" = None


@overload
def processInputs(
	inputs_iter: "Iterable[str]",
	yieldFName: "Literal[False]" = False,
	newer_than: "Optional[Union[int, float]]" = None,
	ignore_entries: "Optional[Sequence[str]]" = None,
	logger: "Union[logging.Logger, ModuleType]" = logging,
) -> "Iterator[Any]":
	...


@overload
def processInputs(
	inputs_iter: "Iterable[str]",
	yieldFName: "Literal[True]" = True,
	newer_than: "Optional[Union[int, float]]" = None,
	ignore_entries: "Optional[Sequence[str]]" = None,
	logger: "Union[logging.Logger, ModuleType]" = logging,
) -> "Iterator[YieldedInput]":
	...


def processInputs(
	inputs_iter: "Iterable[str]",
	yieldFName: "bool" = False,
	newer_than: "Optional[Union[int, float]]" = None,
	ignore_entries: "Optional[Sequence[str]]" = None,
	logger: "Union[logging.Logger, ModuleType]" = logging,
) -> "Iterator[Union[Any, YieldedInput]]":
	inputs: "MutableSequence[pathlib.Path]" = list(
		map(lambda f: pathlib.Path(f), inputs_iter)
	)

	l_inputs = len(inputs)
	ign_set = set(ignore_entries) if ignore_entries is not None else set()
	for inputF_i, inputF in enumerate(inputs):
		if inputF.is_dir():
			try:
				inputD = inputF
				for relFile in inputD.iterdir():
					if relFile.name.startswith("."):
						continue

					if relFile.is_dir():
						inputs.append(relFile)
					elif relFile.is_file():
						if (
							relFile.name.endswith("json.array")
							or relFile.name.endswith("json.array.gz")
							or relFile.name.endswith(".json")
							or relFile.name.endswith(".json.gz")
							or relFile.name.endswith("yaml.array")
							or relFile.name.endswith("yaml.array.gz")
							or relFile.name.endswith("yml.array")
							or relFile.name.endswith("yml.array.gz")
							or relFile.name.endswith(".yaml")
							or relFile.name.endswith(".yaml.gz")
							or relFile.name.endswith(".yml")
							or relFile.name.endswith(".yml.gz")
						):
							if (
								newer_than is not None
								and relFile.stat().st_mtime <= newer_than
							):
								logger.debug(
									f"Skipping file {relFile} (older than {newer_than})"
								)
								continue
							inputs.append(relFile)
						else:
							logger.debug(
								f"Skipping file {relFile} (neither json nor yaml extension)"
							)
					else:
						logger.debug(f"Skipping {relFile} (neither file nor directory)")
			except IOError as ioe:
				logger.error(
					f"Directory {inputD} could not be processed. Reason: {ioe.strerror}"
				)
		elif inputF.is_file():
			try:
				if (
					inputF_i < l_inputs
					and newer_than is not None
					and inputF.stat().st_mtime <= newer_than
				):
					logger.debug(f"Skipping file {inputF} older than {newer_than}")
					continue

				if inputF.name.endswith(".gz"):
					opener = cast("BasicAnyOpener", gzip.open)
				else:
					opener = cast("BasicAnyOpener", open)
				try:
					with opener(inputF, mode="rt", encoding="utf-8") as iFH:
						inputContent = json.load(iFH)
				except json.decoder.JSONDecodeError as jde:
					with opener(inputF, mode="rt", encoding="utf-8") as iFH:
						try:
							inputContent = yaml.safe_load(iFH)
						except yaml.error.MarkedYAMLError as mye:
							logger.critical(
								f"Unable to parse file {inputF} neither as JSON or YAML. Reasons:\nJSON => {jde.msg}\nYAML => {str(mye)}"
							)
							continue

				# Streaming the results one by one
				if isinstance(inputContent, list) and (
					inputF.name.endswith("json.array")
					or inputF.name.endswith("yaml.array")
					or inputF.name.endswith("yml.array")
					or inputF.name.endswith("json.array.gz")
					or inputF.name.endswith("yaml.array.gz")
					or inputF.name.endswith("yml.array.gz")
				):
					for i_ent, ent in enumerate(inputContent):
						if ignore_entries is not None and isinstance(ent, dict):
							the_id = ent.get("_id")
							if the_id in ign_set:
								logger.debug(
									f"Skipping file {inputF}[{i_ent}] as id {the_id} can be skipped"
								)
								continue

						if yieldFName:
							yield YieldedInput(content=ent, path=f"{inputF}[{i_ent}]")
						else:
							yield ent
				else:
					if ignore_entries is not None and isinstance(inputContent, dict):
						the_id = inputContent.get("_id")
						if the_id in ign_set:
							logger.debug(
								f"Skipping file {inputF} as id {the_id} can be skipped"
							)
							continue

					if yieldFName:
						yield YieldedInput(content=inputContent, path=inputF)
					else:
						yield inputContent
			except IOError as ioe:
				logger.error(
					f"File {inputF} could not be processed. Reason: {ioe.strerror}"
				)
		else:
			logger.debug(f"Skipping {inputF} (neither file nor directory)")


PAYLOAD_METADATA_SUFFIX = ".metadata"


def filterPayloads(
	inputs_iter: "Iterable[str]",
	newer_than: "Optional[Union[int, float]]" = None,
	default_metadata: "Mapping[str, Any]" = {},
	logger: "Union[logging.Logger, ModuleType]" = logging,
) -> "Iterator[Tuple[pathlib.Path, Mapping[str, Any]]]":
	"""
	This method takes as input a list of files or directories
	and optionally a reference timestamp filtering condition.

	It emits tuples containing the file paths which fulfil the
	reference timestamp condition, as well as the associated metadata.
	"""
	inputs: "MutableSequence[pathlib.Path]" = list(
		map(lambda f: pathlib.Path(f), inputs_iter)
	)

	l_inputs = len(inputs)
	for inputF_i, inputF in enumerate(inputs):
		if inputF.is_dir():
			try:
				inputD = inputF
				for relFile in inputD.iterdir():
					if relFile.name.startswith("."):
						continue

					if relFile.is_dir():
						inputs.append(relFile)
					elif relFile.is_file() and not relFile.name.endswith(
						PAYLOAD_METADATA_SUFFIX
					):
						# Only process files which are not metadata
						if (
							newer_than is not None
							and relFile.stat().st_mtime <= newer_than
						):
							logger.debug(f"Skipping older {relFile}")
							continue
						inputs.append(relFile)
					else:
						logger.debug(f"Skipping {relFile}")
			except IOError as ioe:
				logger.error(
					f"Directory {inputD} could not be processed. Reason: {ioe.strerror}"
				)
		elif inputF.is_file():
			try:
				if (
					inputF_i < l_inputs
					and newer_than is not None
					and inputF.stat().st_mtime <= newer_than
				):
					logger.debug(f"Skipping older {inputF}")
					continue

				# TODO: how should we declare the metadata?
				inputF_metadata_file = inputF.with_suffix(
					inputF.suffix + PAYLOAD_METADATA_SUFFIX
				)
				if inputF_metadata_file.exists():
					logger.debug(
						f"For file {inputF}, reading metadata {inputF_metadata_file}"
					)
					try:
						with open(
							inputF_metadata_file, mode="r", encoding="utf-8"
						) as iFH:
							inputF_metadata = json.load(iFH)
					except json.decoder.JSONDecodeError as jde:
						with open(
							inputF_metadata_file, mode="r", encoding="utf-8"
						) as iFH:
							try:
								inputF_metadata = yaml.safe_load(iFH)
							except yaml.error.MarkedYAMLError as mye:
								logger.critical(
									f"Skipping {inputF} because metadata file {inputF_metadata_file} could not be parsed neither as JSON or YAML. Reasons:\nJSON => {jde.msg}\nYAML => {str(mye)}"
								)
								continue
					if not isinstance(inputF_metadata, dict):
						logger.critical(
							f"Skipping {inputF} because metadata read from {inputF_metadata_file} is not a dictionary. Got {inputF_metadata}"
						)
						continue
				else:
					logger.debug(f"Using default metadata for file {inputF}")
					inputF_metadata = default_metadata

				yield (inputF, inputF_metadata)
			except IOError as ioe:
				logger.error(
					f"File {inputF} could not be processed. Reason: {ioe.strerror}"
				)
		else:
			logger.debug(f"Skipping {inputF}")
