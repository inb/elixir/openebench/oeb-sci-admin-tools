#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2023 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import atexit
import copy
import dataclasses
import email.utils
import gzip
import hashlib
import io
import json
import logging
import magic
import os
import shutil
import subprocess
import tempfile
import time
import urllib.parse
import urllib.request
import lzma

import diskcache  # type: ignore[import]
import ijson  # type: ignore[import]
import yaml
import xdg.BaseDirectory

from typing import (
	cast,
	NamedTuple,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	import http.client

	from types import (
		ModuleType,
	)

	from typing import (
		Any,
		Callable,
		IO,
		Iterable,
		Iterator,
		Mapping,
		MutableMapping,
		MutableSequence,
		Optional,
		Sequence,
		Set,
		Tuple,
		TypeVar,
		Union,
	)

	from typing_extensions import (
		Buffer,
		Final,
		TypeAlias,
	)

	import sqlite3

	import diskcache.core  # type: ignore[import]

	from extended_json_schema_validator.extensions.abstract_check import (
		BootstrapErrorDict,
	)

	from .auth import (
		OEBCredentials,
	)

	IdAndOrigId: TypeAlias = Tuple[str, Optional[str]]

	from .common import (
		BasicAnyOpener,
	)


class FetchedBinaryData(NamedTuple):
	fetchable_uri: "str"
	payload: "FakingSpooledTemporaryFile"


class FetchedInlineData(NamedTuple):
	data: "Any"
	schema_uri: "Optional[str]"
	remote: "Optional[FetchedBinaryData]"
	is_json: "bool"


@dataclasses.dataclass
class CachedConceptEntriesDir:
	directory: "str"
	flavor: "str"
	mapped_concepts: "MutableMapping[str, str]" = dataclasses.field(
		default_factory=dict
	)


from .auth import (
	OEBCredentialsManager,
)

from .common import (
	DEFAULT_FLAVOR,
	FakingSpooledTemporaryFile,
	FLAVOR_STAGED,
)


class OEBQueryException(Exception):
	pass


# This description tells which are the concepts, their graphql counterparts
# and whether these concepts have an orig_id
DEFAULT_CONCEPT_DESC = {
	"BenchmarkingEvent": ("getBenchmarkingEvents", True, ("OEBE",)),
	"Challenge": ("getChallenges", True, ("OEBX",)),
	"Community": ("getCommunities", False, ("OEBC",)),
	"Contact": ("getContacts", False, ("orcid:",)),
	"Dataset": ("getDatasets", True, ("OEBD",)),
	# 	'idSolv',
	"Metrics": ("getMetrics", True, ("OEBM",)),
	"Reference": (
		None,
		False,
		(
			"doi:",
			"pubmed:",
		),
	),
	"TestAction": ("getTestActions", True, ("OEBA",)),
	"Tool": ("getTools", True, ("OEBT",)),
}

OEB_ID_PREFIX = [
	(prefix, concept)
	for concept, desc in DEFAULT_CONCEPT_DESC.items()
	for prefix in desc[2]
]

OEB_ID_CONCEPT_PREFIX = [
	(concept, desc[2]) for concept, desc in DEFAULT_CONCEPT_DESC.items()
]

CONCEPT_ID_PREFIXES = dict(OEB_ID_PREFIX)

OEB_CONCEPT_PREFIXES = dict(OEB_ID_CONCEPT_PREFIX)

DEFAULT_CONCEPTS = sorted(DEFAULT_CONCEPT_DESC.keys())

MIME_IDLIST = "text/uri-list"

GRAPHQL_TYPE_MAPPER = {
	int: "Int!",
	float: "Float!",
	bool: "Boolean!",
	str: "String!",
}

BDM_REPO = "https://github.com/inab/benchmarking-data-model.git"
DEFAULT_DEEP_REPOURL = "https://github.com/inab/OEB_level2_data_migration.git"
DEFAULT_DEEP_REL_PATH = "oeb_level2/schemas"
MIME_SCHEMA = "application/schema+json"
DEFAULT_GIT_CMD = "git"


class CompressorFromStreamIOWrapper(io.RawIOBase):
	def __init__(
		self,
		stream: "IO[bytes]",
		compress_module: "ModuleType" = lzma,
		blocksize: "int" = 1024 * 1024,
	):
		self.stream: "Optional[IO[bytes]]" = stream
		self.readbuffer = bytearray()
		self.bio = io.BytesIO()
		self.zs: "Optional[IO[bytes]]" = compress_module.open(self.bio, mode="wb")
		self.blocksize: "int" = blocksize

	def readinto(self, buf: "Buffer") -> "int":
		if len(self.bio.getvalue()) == 0:
			# Does the read buffer have contents?
			if len(self.readbuffer) == 0:
				# Can we add contents to the read buffer?
				if self.stream is not None:
					rbytes = self.stream.read(self.blocksize)
					self.readbuffer += rbytes

			# Does the read buffer have contents to be compressed?
			if len(self.readbuffer) > 0:
				assert self.zs is not None
				numbytes = self.zs.write(self.readbuffer)
				del self.readbuffer[0:numbytes]
			elif self.stream is not None:
				assert self.zs is not None
				# Close the streams
				self.stream.close()
				self.stream = None
				self.zs.close()
				self.zs = None

		biolen = len(self.bio.getvalue())
		numbytes = len(buf)  # type: ignore[arg-type]
		if numbytes > biolen:
			numbytes = biolen

		if numbytes > 0:
			bioview = self.bio.getbuffer()
			buf[0:numbytes] = bioview[0:numbytes]  # type: ignore[index]
			remainder = len(bioview) - numbytes
			if remainder > 0:
				bioview[:remainder] = bioview[numbytes:]

			# This is needed to avoid a BufferError
			bioview.release()
			del bioview
			self.bio.seek(remainder)
			self.bio.truncate()

		return numbytes

	def readable(self) -> "bool":
		return True


class CompressedDisk(diskcache.Disk):  # type: ignore[misc]
	"""Cache key and value using a compression module (default lzma)."""

	def __init__(
		self, directory: "str", compress_module: "ModuleType" = lzma, **kwargs: "Any"
	):
		"""Initialize Compressed disk instance.

		Values are compressed using a compression module library. The
		`compress_module` is the module having the open, compress and decompress
		methods.

		:param str directory: directory path
		:param int compress_module: compression module (default lzma)
		:param kwargs: super class arguments

		"""
		self.compress_module = compress_module
		super().__init__(directory, **kwargs)

	def store(
		self,
		value: "Union[str, int, float, bytes, IO[bytes]]",
		read: "bool",
		key: "diskcache.core.Constant" = diskcache.UNKNOWN,
	) -> (
		"Tuple[int, int, Optional[str], Optional[Union[str, int, float, sqlite3.Binary]]]"
	):
		if read:
			value = cast(
				"IO[bytes]",
				CompressorFromStreamIOWrapper(
					cast("IO[bytes]", value), compress_module=self.compress_module
				),
			)
		else:
			value = self.compress_module.compress(value)
		return cast(
			"Tuple[int, int, Optional[str], Optional[Union[str, int, float, sqlite3.Binary]]]",
			super().store(value, read, key=key),
		)

	def fetch(
		self,
		mode: "int",
		filename: "Optional[str]",
		value: "Optional[Union[str, int, float, sqlite3.Binary]]",
		read: "bool",
	) -> "Union[bytes, IO[bytes]]":
		data = super().fetch(mode, filename, value, read)
		if not read:
			return cast("bytes", self.compress_module.decompress(data))

		return cast("IO[bytes]", self.compress_module.open(data, mode="rb"))


class OEBAbstractFetcher(OEBCredentialsManager):
	DEFAULT_CACHE_ENTRY_EXPIRE: "int" = 60

	def __init__(
		self,
		baseUrl: "str",
		oeb_credentials: "Optional[Union[OEBCredentials, str]]" = None,
		cache_entry_expire: "Optional[Union[int, float]]" = None,
		cache_dir: "Optional[str]" = None,
		raw_cache_dir: "Optional[str]" = None,
		do_compress_raw: "bool" = True,
	):
		super().__init__(baseUrl, oeb_credentials=oeb_credentials)

		# Caching is only enabled when timeout is set
		self._cache: "Optional[diskcache.Cache]" = None
		self._cache_raw: "Optional[diskcache.Cache]" = None
		self._cache_entry_expire: "Optional[Union[int, float]]" = None
		if cache_entry_expire is not None:
			if cache_dir is not None:
				os.makedirs(cache_dir, exist_ok=True)
			else:
				cache_dir = xdg.BaseDirectory.save_cache_path(
					"es.elixir.openebench.oeb_sci_admin_tools"
				)
			# Is the directory writable?
			if not os.access(cache_dir, os.W_OK):
				cache_dir = None

			if raw_cache_dir is not None:
				os.makedirs(raw_cache_dir, exist_ok=True)
			else:
				raw_cache_dir = xdg.BaseDirectory.save_cache_path(
					"es.elixir.openebench.oeb_sci_admin_tools_RAW"
				)
			# Is the directory writable?
			if not os.access(raw_cache_dir, os.W_OK):
				raw_cache_dir = None

			# Eviction policy to none is needed to
			# avoid implicit culling of entries
			# when there are more than 10 entries
			# and those entries do not have an attached
			# expiration timestamp
			self._cache = diskcache.Cache(
				directory=cache_dir,
				disk=diskcache.JSONDisk,
				disk_compress_level=9,
				eviction_policy="none",
			)

			self._cache_raw = diskcache.Cache(
				directory=raw_cache_dir,
				disk=CompressedDisk if do_compress_raw else diskcache.Disk,
				eviction_policy="none",
			)
			if cache_entry_expire > 0:
				self._cache_entry_expire = cache_entry_expire
			else:
				self._cache_entry_expire = self.DEFAULT_CACHE_ENTRY_EXPIRE

	def gen_cache_key(
		self, url: "str", query: "Optional[Any]" = None, use_auth: "bool" = True
	) -> "str":
		"""
		This method generates cache keys to store pre-computed queries
		"""
		h = hashlib.new("sha256")
		h.update(url.encode("utf-8"))
		# Separator
		h.update(b"\0")
		h.update(json.dumps(query, sort_keys=True).encode("utf-8"))
		if use_auth:
			user = self.get_credentials_user()
			if len(user) > 0:
				# Separator
				h.update(b"\0")
				h.update(user.encode("utf-8"))
		return h.hexdigest()

	def gen_cache_key_from_req(self, req: "urllib.request.Request") -> "str":
		url = req.full_url
		use_auth = req.has_header("Authorization")
		anon_headers: "MutableMapping[str, str]" = dict(req.header_items())
		if use_auth:
			del anon_headers["Authorization"]
		return self.gen_cache_key(
			req.full_url,
			None if len(anon_headers) == 0 else anon_headers,
			use_auth=use_auth,
		)


class OEBFetcher(OEBAbstractFetcher):
	CACHING_HEADERS: "Final[Mapping[str, str]]" = {
		"Last-Modified": "If-Modified-Since",
		"ETag": "If-None-Match",
	}

	TEST_GRAPHQL_QUERY: "Final[Mapping[str, Any]]" = {
		"query": """\
query {
 __typename
}
"""
	}

	def __init__(
		self,
		baseUrl: "str",
		oeb_credentials: "Optional[Union[OEBCredentials, str]]" = None,
		cache_entry_expire: "Optional[Union[int, float]]" = None,
		cache_dir: "Optional[str]" = None,
		raw_cache_dir: "Optional[str]" = None,
		do_compress_raw: "bool" = True,
	):
		super().__init__(
			baseUrl,
			oeb_credentials=oeb_credentials,
			cache_entry_expire=cache_entry_expire,
			cache_dir=cache_dir,
			raw_cache_dir=raw_cache_dir,
			do_compress_raw=do_compress_raw,
		)

		self._fetchedConceptIdsMap: "Optional[Mapping[str, Sequence[IdAndOrigId]]]" = (
			None
		)
		self._cached_concept_entries_dirs: "MutableMapping[str, CachedConceptEntriesDir]" = (
			{}
		)

	def openurl(
		self,
		url: "str",
		headers: "MutableMapping[str, str]",
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "IO[bytes]":
		req = urllib.request.Request(url, headers=headers)
		return self.openreq(
			req,
			use_cache=use_cache,
			override_cache=override_cache,
		)

	def openreq(
		self,
		req: "urllib.request.Request",
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "IO[bytes]":
		# Do not cache unless we know what to do
		if req.get_method() != "GET":
			return cast("IO[bytes]", urllib.request.urlopen(req))

		# Is the cache enabled?
		cache_key: "Optional[str]"
		ir: "Optional[http.client.HTTPResponse]" = None
		if self._cache_raw is not None and use_cache:
			cache_key = self.gen_cache_key_from_req(req)
			if not override_cache:
				handler, expire_stamp, caching_tags_str = self._cache_raw.get(
					cache_key, read=True, expire_time=True, tag=True
				)
				caching_tags: "Mapping[str, str]" = {}
				if isinstance(caching_tags_str, str):
					try:
						caching_tags = json.loads(caching_tags_str)
					except:
						pass
				# Do we have caching hints?
				if isinstance(caching_tags, dict) and len(caching_tags) > 0:
					for tag, tag_value in caching_tags.items():
						req.add_header(tag, tag_value)
					try:
						ir = urllib.request.urlopen(req)

						# Remove the cache instance, as
						# it didn't have an explicit timeout
						handler.close()
						self._cache_raw.delete(cache_key)
					except urllib.error.HTTPError as he:
						# Content should be the same
						if he.code == 304:
							return cast("IO[bytes]", handler)
						else:
							handler.close()
							raise he
				# Is the answer in the cache?
				elif expire_stamp is not None:
					return cast("IO[bytes]", handler)
		else:
			cache_key = None

		if ir is None:
			ir = urllib.request.urlopen(req)
		# Fast answer when it is not needed to cache it
		if cache_key is None or self._cache_raw is None:
			return cast("IO[bytes]", ir)

		# Does the stream has caching hints to be preserved?
		cache_tags = {}
		for header_name, check_name in self.CACHING_HEADERS.items():
			if header_name in ir.headers:
				cache_tags[check_name] = ir.headers[header_name]

		# We are setting an explicit expire when no server hint is received
		expire: "Optional[float]" = None
		if len(cache_tags) == 0:
			if "Expires" in ir.headers:
				tup = email.utils.parsedate_tz(ir.headers["Expires"])
				if tup is not None:
					expire = email.utils.mktime_tz(tup) - time.time()
				else:
					expire = 0

				# Is it a reliable expiration lapse?
				if expire <= 0:
					expire = self._cache_entry_expire
			else:
				expire = self._cache_entry_expire
		try:
			self._cache_raw.set(
				cache_key, ir, read=True, tag=json.dumps(cache_tags), expire=expire
			)
		finally:
			ir.close()

		return cast("IO[bytes]", self._cache_raw.get(cache_key, read=True))

	def fetchIds(
		self,
		outputDir: "str",
		flavor: "Optional[str]" = None,
		relOutputFile: "str" = "ids.txt",
		concepts: "Sequence[str]" = DEFAULT_CONCEPTS,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Mapping[str, str]":
		# Security token
		auth_header = self.getHTTPAuthHeader()

		# Preparation
		baseUrl = self.baseUrl
		if flavor is not None:
			baseUrl += urllib.parse.quote_plus(flavor) + "/"

		# Being sure the output directory is in place
		os.makedirs(outputDir, exist_ok=True)

		# Now, let's dump
		idlist_headers = {
			"Accept": MIME_IDLIST,
		}

		if auth_header:
			self.logger.debug(f"Fetching ids from {concepts} using bearer token")
			idlist_headers.update(auth_header)

		self.logger.debug(f"Base URL for fetching ids requests is {baseUrl}")
		conceptIdsMap = {}
		for conceptName in concepts:
			self.logger.debug(f"Fetching concept {conceptName} ids")
			cDir = os.path.join(outputDir, conceptName)
			os.makedirs(cDir, exist_ok=True)

			url = baseUrl + urllib.parse.quote_plus(conceptName) + "/"
			idreq = urllib.request.Request(url, headers=idlist_headers)

			# Get the list of identifiers to be used
			idspath = os.path.join(cDir, relOutputFile)
			with open(idspath, mode="wb") as iH, self.openreq(
				idreq,
				use_cache=use_cache,
				override_cache=override_cache,
			) as r:
				shutil.copyfileobj(r, iH)
			self.logger.debug(f"\tFetched ids")
			conceptIdsMap[conceptName] = idspath

		return conceptIdsMap

	def fetchIdsInMemory(
		self,
		flavor: "Optional[str]" = None,
		concepts: "Sequence[str]" = DEFAULT_CONCEPTS,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Mapping[str, Sequence[str]]":
		# Security token
		auth_header = self.getHTTPAuthHeader()

		# Preparation
		baseUrl = self.baseUrl
		if flavor is not None:
			baseUrl += urllib.parse.quote_plus(flavor) + "/"

		# Now, let's dump
		idlist_headers = {
			"Accept": MIME_IDLIST,
		}

		if auth_header:
			self.logger.debug(f"Fetching ids from {concepts} using bearer token")
			idlist_headers.update(auth_header)

		self.logger.debug(f"Base URL for fetching ids requests is {baseUrl}")
		conceptIdsMap = {}
		for conceptName in concepts:
			self.logger.debug(f"Fetching concept {conceptName} ids in memory")

			url = baseUrl + urllib.parse.quote_plus(conceptName) + "/"

			# Is the cache enabled?
			cache_key: "Optional[str]"
			if self._cache is not None and use_cache:
				cache_key = self.gen_cache_key(url)
				if not override_cache:
					ids, expire_stamp = self._cache.get(cache_key, expire_time=True)
					# Is the answer in the cache?
					if expire_stamp is not None:
						self.logger.debug(f"\tFetched {len(ids)} ids (cached)")
						conceptIdsMap[conceptName] = ids
						continue
			else:
				cache_key = None

			# We are managing the cache here because authentication
			# has to be taken into account
			idreq = urllib.request.Request(url, headers=idlist_headers)

			# Get the list of identifiers
			with self.openreq(
				idreq, use_cache=use_cache, override_cache=override_cache
			) as r:
				ids = list(
					filter(lambda l: len(l) > 0, r.read().decode("utf-8").split("\n"))
				)

			self.logger.debug(f"\tFetched {len(ids)} ids")
			conceptIdsMap[conceptName] = ids

			# Preserve in the cache
			if cache_key is not None and self._cache is not None:
				self._cache.set(cache_key, ids, expire=self._cache_entry_expire)

		return conceptIdsMap

	def query_graphql(
		self,
		query: "str",
		variables: "Mapping[str, Any]" = {},
		query_name: "str" = "the_query",
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Any":
		# First, bundling the query
		if len(variables) > 0:
			query_parameters = ", ".join(
				map(
					lambda v: f"${v[0]}: {GRAPHQL_TYPE_MAPPER.get(type(v[1]), 'String')}",
					variables.items(),
				)
			)
			json_query = {
				"query": f"""query {query_name}({query_parameters}) {{
		{query}
	}}""",
				"variables": variables,
			}
		else:
			json_query = {
				"query": f"""query {query_name} {{
		{query}
	}}"""
			}

		self.logger.debug(f"Query to be issued:\n{{\n{query}\n}}")

		# Second, endpoint preparation
		url = self.baseUrl
		if not url.endswith("/graphql"):
			if not url.endswith("/graphql/"):
				if not url.endswith("/"):
					url += "/"
				url += urllib.parse.quote_plus("graphql")
			else:
				# Graphql endpoint does not like slashes
				url = url[0:-1]

		# Third, credentials
		auth_header = self.getHTTPAuthHeader()

		if auth_header:
			self.logger.debug(f"Querying {url} using bearer token")

		# Is the cache enabled?
		cache_key: "Optional[str]"
		if self._cache is not None and use_cache:
			cache_key = self.gen_cache_key(url, json_query)
			if not override_cache:
				json_response, expire_stamp, caching_tags_str = self._cache.get(
					cache_key, expire_time=True, tag=True
				)
				caching_tags: "Mapping[str, str]" = {}
				if isinstance(caching_tags_str, str):
					try:
						caching_tags = json.loads(caching_tags_str)
					except:
						pass

				# Do we have caching hints?
				if isinstance(caching_tags, dict) and len(caching_tags) > 0:
					# As graphql endpoint does not answer a 304
					# if we send it hints, let's use the inner
					# knowledge of Last-Modified at the service level
					# which does not change until the database changes
					testreq = urllib.request.Request(
						url,
						data=json.dumps(self.TEST_GRAPHQL_QUERY).encode("utf-8"),
						headers=auth_header,
					)

					with urllib.request.urlopen(testreq) as testir:
						for tk, tv in caching_tags.items():
							# No match
							if tv != testir.headers.get(tk):
								self._cache.delete(cache_key)
								break
						else:
							# All matched
							return json_response
				# Is the answer in the cache?
				elif expire_stamp is not None:
					return json_response
		else:
			cache_key = None

		# Fourth, let's dance!
		req = urllib.request.Request(
			url, data=json.dumps(json_query).encode("utf-8"), headers=auth_header
		)

		cache_tags: "MutableMapping[str, str]" = {}
		expire: "Optional[float]" = None
		with urllib.request.urlopen(req) as ir:
			# Does the stream has caching hints to be preserved?
			for header_name, check_name in self.CACHING_HEADERS.items():
				if header_name in ir.headers:
					cache_tags[header_name] = ir.headers[header_name]

			if len(cache_tags) == 0:
				if "Expires" in ir.headers:
					tup = email.utils.parsedate_tz(ir.headers["Expires"])
					if tup is not None:
						expire = email.utils.mktime_tz(tup) - time.time()
					else:
						expire = 0

					# Is it a reliable expiration lapse?
					if expire <= 0:
						expire = self._cache_entry_expire
				else:
					expire = self._cache_entry_expire

			json_response = json.load(ir)

		if cache_key is not None and self._cache is not None:
			# We are setting an explicit expire when no server hint is received

			self._cache.set(
				cache_key, json_response, tag=json.dumps(cache_tags), expire=expire
			)

		return json_response

	def fetchIdsAndOrigIds(
		self,
		concepts: "Sequence[str]" = DEFAULT_CONCEPTS,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Mapping[str, Sequence[str]]":
		"""
		This method linearizes the gathered ids and orig_ids
		"""

		conceptIdsMap = self.getFetchedConceptIdsMap(
			use_cache=use_cache, override_cache=override_cache
		)

		ids_and_orig_ids: "MutableMapping[str, Sequence[str]]" = {}
		for c in concepts:
			if c in conceptIdsMap:
				c_ids_and_orig_ids = []
				for the_id, orig_id in conceptIdsMap[c]:
					c_ids_and_orig_ids.append(the_id)
					if orig_id is not None:
						c_ids_and_orig_ids.append(orig_id)

				ids_and_orig_ids[c] = c_ids_and_orig_ids

		return ids_and_orig_ids

	def getFetchedConceptIdsMap(
		self,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Mapping[str, Sequence[IdAndOrigId]]":
		"""
		This method queries the database once through graphql
		returning the cached gathered values in subsequent calls
		"""

		# Let's build the queries
		if self._fetchedConceptIdsMap is None:
			concepts: "Sequence[str]" = DEFAULT_CONCEPTS
			fetchedConceptIdsMap: "MutableMapping[str, Sequence[IdAndOrigId]]" = {}
			rest_concepts = []
			for concept in concepts:
				concept_desc = DEFAULT_CONCEPT_DESC.get(concept)
				if concept_desc is not None:
					if concept_desc[0] is not None and concept_desc[1]:
						result_key = concept_desc[0]
						graphql_query = f"""	{concept_desc[0]} {{
		_id
		orig_id
	}}
"""
						g_result = self.query_graphql(
							graphql_query,
							use_cache=use_cache,
							override_cache=override_cache,
						)
						if isinstance(g_result, dict):
							d_result = g_result.get("data")
							if isinstance(d_result, dict):
								ids_and_orig_ids = []
								for result in d_result.get(result_key, []):
									ids_and_orig_ids.append(
										(result["_id"], result.get("orig_id"))
									)
								self.logger.debug(
									f"Fetched ids from {len(ids_and_orig_ids)} {concept} entries"
								)
								fetchedConceptIdsMap[concept] = ids_and_orig_ids
							else:
								raise OEBQueryException(
									f"graphql query:\n{graphql_query}\nreturned errors:\n{g_result}"
								)
						else:
							raise OEBQueryException(
								f"Empty graphql answer for query:\n{graphql_query}"
							)
					else:
						rest_concepts.append(concept)

			# Now, the fast REST queries
			if len(rest_concepts) > 0:
				restConceptIdsMap = {
					c: list(map(lambda the_id: (the_id, None), ids))
					for c, ids in self.fetchIdsInMemory(
						concepts=rest_concepts,
						flavor=DEFAULT_FLAVOR,
						use_cache=use_cache,
						override_cache=override_cache,
					).items()
				}
				fetchedConceptIdsMap.update(restConceptIdsMap)

			self._fetchedConceptIdsMap = fetchedConceptIdsMap

		return self._fetchedConceptIdsMap

	def fetchConceptEntries(
		self,
		outputDir: "str",
		flavor: "Optional[str]" = None,
		relOutputFile: "str" = "entries-json.array",
		do_spread: "bool" = True,
		keep_monolithic: "bool" = True,
		concepts: "Sequence[str]" = DEFAULT_CONCEPTS,
		do_compress: "bool" = False,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Mapping[str, str]":
		assert (
			do_spread or keep_monolithic
		), "At least one style of content preservation should be allowed to avoid wasting resources fetching and removing"
		# Security token
		auth_header = self.getHTTPAuthHeader()

		# Preparation
		baseUrl = self.baseUrl
		if flavor is not None:
			baseUrl += urllib.parse.quote_plus(flavor) + "/"

		# Being sure the output directory is in place
		os.makedirs(outputDir, exist_ok=True)

		# Now, let's dump
		entries_headers = {
			"Accept": "application/json",
		}

		if auth_header:
			self.logger.debug(f"Fetching entries from {concepts} using bearer token")
			entries_headers.update(auth_header)

		self.logger.debug(
			f"Base URL for fetching concept entries requests is {baseUrl}"
		)
		conceptEntsMap = {}
		opener: "BasicAnyOpener"
		if do_compress:
			opener = cast("BasicAnyOpener", gzip.open)
			spread_filename_pat = "{0:06d}.json.gz"
		else:
			opener = cast("BasicAnyOpener", open)
			spread_filename_pat = "{0:06d}.json"

		for conceptName in concepts:
			self.logger.debug(f"Fetching concept {conceptName} entries")
			cDir = os.path.join(outputDir, conceptName)
			os.makedirs(cDir, exist_ok=True)

			# Fetch the entries
			url = baseUrl + urllib.parse.quote_plus(conceptName) + "/"
			req = urllib.request.Request(url, headers=entries_headers)
			entpath = os.path.join(cDir, relOutputFile)
			with opener(entpath, mode="wb") as eH, self.openreq(
				req,
				use_cache=use_cache,
				override_cache=override_cache,
			) as ir:
				# This is masked due an issue with mypy
				shutil.copyfileobj(ir, eH)  # type: ignore[misc]

			conceptEntsMap[conceptName] = entpath

			if do_spread:
				# Now, spread the entries for further processing
				with opener(entpath, mode="rt", encoding="utf-8") as eH:
					prevreldir = None
					for iEnt, entry in enumerate(
						ijson.items(eH, "item", use_float=True)
					):
						reldir = "{0:06d}".format(iEnt // 10000)
						if reldir != prevreldir:
							os.makedirs(os.path.join(cDir, reldir), exist_ok=True)
							prevreldir = reldir
						idpath = os.path.join(
							cDir, reldir, spread_filename_pat.format(iEnt)
						)
						with open(idpath, mode="w", encoding="utf-8") as dH:
							json.dump(entry, dH)

			if not keep_monolithic:
				os.unlink(entpath)

		return conceptEntsMap

	@staticmethod
	def filter_by(
		datares_raw: "Iterable[Mapping[str, Any]]",
		filtering_keys: "Mapping[str, Union[Sequence[str], Set[str]]]",
		negative_filtering_keys: "Mapping[str, Union[Sequence[str], Set[str]]]" = {},
	) -> "Iterator[Mapping[str, Any]]":
		# There are both positive and negative filtering conditions
		if len(filtering_keys) > 0 or len(negative_filtering_keys) > 0:
			fk_set = {
				fk_key: fk_values if isinstance(fk_values, set) else set(fk_values)
				for fk_key, fk_values in filtering_keys.items()
			}
			fk_neg_set = {
				fk_neg_key: fk_neg_values
				if isinstance(fk_neg_values, set)
				else set(fk_neg_values)
				for fk_neg_key, fk_neg_values in negative_filtering_keys.items()
			}
			for dr in datares_raw:
				still_yield = True
				# Positive filtering conditions
				for filt_key, filt_values in fk_set.items():
					if filt_key in dr:
						if isinstance(dr[filt_key], list):
							if all(map(lambda dv: dv not in filt_values, dr[filt_key])):
								# Skip this entry
								still_yield = False
								break
						elif dr[filt_key] not in filt_values:
							# Skip this entry
							still_yield = False
							break

				if still_yield:
					# Negative filtering conditions
					for neg_filt_key, neg_filt_values in fk_neg_set.items():
						if neg_filt_key in dr:
							if isinstance(dr[neg_filt_key], list):
								if any(
									map(
										lambda dv: dv in neg_filt_values,
										dr[neg_filt_key],
									)
								):
									# Skip this entry
									still_yield = False
									break
							elif dr[neg_filt_key] in neg_filt_values:
								# Skip this entry
								still_yield = False
								break
				if still_yield:
					# Passed all tests
					yield dr
		else:
			yield from datares_raw

	def iterateConceptEntries(
		self,
		concept: "str",
		flavor: "str" = FLAVOR_STAGED,
		filtering_keys: "Mapping[str, Union[Sequence[str], Set[str]]]" = {},
		negative_filtering_keys: "Mapping[str, Union[Sequence[str], Set[str]]]" = {},
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Iterator[Mapping[str, Any]]":
		"""
		This method fetches and caches the entries of a given
		data type, in the chosen "flavor".
		Once fetched, the entries are filtered and yielded
		"""

		# First, get the directory where the concepts are mirrored
		cce_dir = self._cached_concept_entries_dirs.get(flavor)
		if cce_dir is None:
			# Creating the temporary directory where to cache the contents
			cached_concept_entries_dir = tempfile.mkdtemp(
				prefix="oeb", suffix="entries"
			)
			atexit.register(shutil.rmtree, cached_concept_entries_dir)
			cce_dir = CachedConceptEntriesDir(cached_concept_entries_dir, flavor)
			self._cached_concept_entries_dirs[flavor] = cce_dir

		# Get the path to the compressed file to be used
		mapped_path = cce_dir.mapped_concepts.get(concept)
		if mapped_path is None:
			# Store it in an efficient way
			mapped_paths = self.fetchConceptEntries(
				cce_dir.directory,
				concepts=[concept],
				relOutputFile="entries-json.array.gz",
				do_compress=True,
				flavor=cce_dir.flavor,
				do_spread=False,
				keep_monolithic=True,
				use_cache=use_cache,
				override_cache=override_cache,
			)
			cce_dir.mapped_concepts.update(mapped_paths)

			mapped_path = mapped_paths[concept]

		# Iterate and yield over the entries
		with gzip.open(mapped_path, mode="rt", encoding="utf-8") as eH:
			datares_raw = ijson.items(eH, "item", use_float=True)
			if len(filtering_keys) > 0 or len(negative_filtering_keys) > 0:
				yield from self.filter_by(
					datares_raw, filtering_keys, negative_filtering_keys
				)
			else:
				yield from datares_raw

	def fetchEntriesFromIds(
		self,
		input_ids: "Iterable[Union[str, Tuple[str, str]]]",
		flavor: "Optional[str]" = None,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Iterator[Tuple[str, Mapping[str, Any]]]":
		if flavor is None:
			flavor = DEFAULT_FLAVOR

		input_ids_by_concept: "MutableMapping[str, MutableSequence[str]]" = {}

		failed_validation = 0
		for input_id_t in input_ids:
			concept: "str"
			input_id: "str"
			if isinstance(input_id_t, tuple):
				concept, input_id = input_id_t
			else:
				id_prefix = input_id_t[0:4]
				concept_p = CONCEPT_ID_PREFIXES.get(id_prefix)
				if concept_p is None:
					self.logger.error(
						f"Unable to map {id_prefix} to a predictable concept. Is it a Contact, a Reference or a typo?"
					)
					failed_validation += 1
					continue
				concept = concept_p
				input_id = input_id_t

			input_ids_by_concept.setdefault(concept, []).append(input_id)

		if failed_validation > 0:
			raise OEBQueryException(
				f"{failed_validation} entries could not be mapped to a concept through their prefixes"
			)

		# Security token
		auth_header = self.getHTTPAuthHeader()

		# Preparation
		baseUrl = self.baseUrl
		if flavor is not None:
			baseUrl += urllib.parse.quote_plus(flavor) + "/"

		# Now, let's fetch
		entries_headers = {
			"Accept": "application/json",
		}

		if auth_header:
			self.logger.debug(
				f"Fetching cherry-picked entries from {list(input_ids_by_concept.keys())} using bearer token"
			)
			entries_headers.update(auth_header)

		self.logger.debug(
			f"Base URL for fetching cherry-picked entries requests is {baseUrl}"
		)
		for conceptName, concept_input_ids in input_ids_by_concept.items():
			unique_input_ids = set(concept_input_ids)
			self.logger.debug(
				f"Fetching {len(unique_input_ids)} entries from concept {conceptName}"
			)

			# Fetch the entries
			url_prefix = baseUrl + urllib.parse.quote_plus(conceptName) + "/"
			for unique_input_id in unique_input_ids:
				url = url_prefix + urllib.parse.quote_plus(unique_input_id, safe="/")
				req = urllib.request.Request(url, headers=entries_headers)
				bio_json = io.BytesIO()
				try:
					with self.openreq(
						req, use_cache=use_cache, override_cache=override_cache
					) as ir:
						shutil.copyfileobj(ir, bio_json)
				except urllib.error.HTTPError as he:
					errmsg = f"Unable to fetch {unique_input_id} ({conceptName}) through {url}. Error code {he.code} => {he.reason}"
					self.logger.exception(errmsg)
					raise OEBQueryException(errmsg) from he
				# Preparing for the parsing work
				bio_json.flush()
				bio_json.seek(0)
				tiow_json = io.TextIOWrapper(bio_json, encoding="utf-8")
				entry = json.load(tiow_json)
				yield conceptName, entry

	def fetchEntriesFromIdsIntoDirectory(
		self,
		input_ids: "Iterable[Union[str, Tuple[str, str]]]",
		outputDir: "str",
		flavor: "Optional[str]" = None,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Mapping[str, Sequence[Tuple[str, str]]]":
		if flavor is None:
			flavor = DEFAULT_FLAVOR

		input_ids_by_concept: "MutableMapping[str, MutableSequence[str]]" = {}

		failed_validation = 0
		for input_id_t in input_ids:
			concept: "str"
			input_id: "str"
			if isinstance(input_id_t, tuple):
				concept, input_id = input_id_t
			else:
				id_prefix = input_id_t[0:4]
				concept_p = CONCEPT_ID_PREFIXES.get(id_prefix)
				if concept_p is None:
					self.logger.error(
						f"Unable to map {id_prefix} to a predictable concept. Is it a Contact, a Reference or a typo?"
					)
					failed_validation += 1
					continue
				concept = concept_p
				input_id = input_id_t

			input_ids_by_concept.setdefault(concept, []).append(input_id)

		if failed_validation > 0:
			raise OEBQueryException(
				f"{failed_validation} entries could not be mapped to a concept through their prefixes"
			)

		# Security token
		auth_header = self.getHTTPAuthHeader()

		# Preparation
		baseUrl = self.baseUrl
		if flavor is not None:
			baseUrl += urllib.parse.quote_plus(flavor) + "/"

		# Being sure the output directory is in place
		os.makedirs(outputDir, exist_ok=True)

		# Now, let's dump
		entries_headers = {
			"Accept": "application/json",
		}

		if auth_header:
			self.logger.debug(
				f"Fetching cherry-picked entries from {list(input_ids_by_concept.keys())} using bearer token"
			)
			entries_headers.update(auth_header)

		self.logger.debug(
			f"Base URL for fetching cherry-picked entries requests is {baseUrl}"
		)
		conceptEntsMap = {}
		for conceptName, concept_input_ids in input_ids_by_concept.items():
			unique_input_ids = set(concept_input_ids)
			cDir = os.path.join(outputDir, conceptName + "_entries")
			self.logger.debug(
				f"Fetching {len(unique_input_ids)} entries from concept {conceptName} into {cDir}"
			)
			os.makedirs(cDir, exist_ok=True)

			# Fetch the entries
			url_prefix = baseUrl + urllib.parse.quote_plus(conceptName) + "/"
			fetched = []
			for unique_input_id in unique_input_ids:
				url = url_prefix + urllib.parse.quote_plus(unique_input_id, safe="/")
				req = urllib.request.Request(url, headers=entries_headers)
				# This is needed when the identifier contains "problematic"
				# characters, like slashes or colons
				entpath = os.path.join(
					cDir, urllib.parse.quote_plus(unique_input_id) + "_entry.json"
				)
				try:
					with open(entpath, mode="wb") as eH, self.openreq(
						req,
						use_cache=use_cache,
						override_cache=override_cache,
					) as ir:
						shutil.copyfileobj(ir, eH)
				except urllib.error.HTTPError as he:
					errmsg = f"Unable to fetch {unique_input_id} ({conceptName}) through {url}. Error code {he.code} => {he.reason}"
					self.logger.exception(errmsg)
					raise OEBQueryException(errmsg) from he
				fetched.append((unique_input_id, entpath))

			conceptEntsMap[conceptName] = fetched

		return conceptEntsMap

	def fetchInlineDataFromDatalink(
		self,
		source: "str",
		datalink: "Mapping[str, Any]",
		discard_unvalidable: "bool" = False,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Tuple[Optional[BootstrapErrorDict], Optional[Union[FetchedInlineData, FetchedBinaryData]]]":
		failed_validation: "Optional[BootstrapErrorDict]" = None
		is_json = True

		the_data: "Optional[Any]" = datalink.get("inline_data")
		fetchable_uri: "Optional[str]" = None
		schema_uri = datalink.get("schema_uri")
		# Now, create a temporary local file for the payload
		orig_validable_content = None
		if the_data is not None:
			schema_uri = datalink.get("schema_url")
		elif schema_uri is not None or not discard_unvalidable:
			orig_validable_content = FakingSpooledTemporaryFile(max_size=1024 * 1024)
			is_json = False
			fetchable_uri = datalink["uri"]
			assert fetchable_uri is not None
			try:
				# Now, fetch the payload into the temporary file
				# The content must be publicly available
				# or at least readable using OpenEBench user credentials
				f_req: "urllib.request.Request"
				# Needed
				headers = {
					"Accept": "*/*",
				}
				if fetchable_uri.startswith(self.baseUrl):
					headers.update(self.getHTTPAuthHeader())
				f_req = urllib.request.Request(fetchable_uri, headers=headers)
				with self.openreq(
					f_req, use_cache=use_cache, override_cache=override_cache
				) as fH:
					shutil.copyfileobj(fH, orig_validable_content)
				# Size
				vc_size = orig_validable_content.tell()

				# Get first 4k as sample
				orig_validable_content.seek(0)
				# Only parse JSON/YAML contents if there is a schema
				if schema_uri is not None:
					vc_sample = orig_validable_content.read(4096)

					# Now, guess whether it is JSON or YAML
					vc_mime = magic.from_buffer(vc_sample, mime=True)

					if vc_mime in ("application/json", "text/plain"):
						self.logger.debug(f"{fetchable_uri} mime {vc_mime}")

						orig_validable_content.seek(0)
						try:
							vc_text = io.TextIOWrapper(
								orig_validable_content, encoding="utf-8"
							)
							the_data = json.load(vc_text)
							is_json = True
						except json.decoder.JSONDecodeError as jde:
							orig_validable_content.seek(0)
							try:
								vc_text = io.TextIOWrapper(
									orig_validable_content, encoding="utf-8"
								)
								the_data = yaml.safe_load(vc_text)
								is_json = False
							except yaml.error.MarkedYAMLError as mye:
								self.logger.exception(
									f"Failed validation of dataset {source}, as {fetchable_uri} did not provide valid YAML content"
								)

								failed_validation = {
									"reason": "unexpected",
									"description": f"{fetchable_uri} did not provide valid JSON or YAML content",
								}
						finally:
							del vc_text
					else:
						self.logger.error(
							f"Cannot validate dataset {source}, as {fetchable_uri} content was identified as {vc_mime}, which is not JSON or YAML"
						)

						failed_validation = {
							"reason": "unexpected",
							"description": f"{fetchable_uri} content was identified as {vc_mime}, which is not JSON or YAML",
						}
			except urllib.error.HTTPError as he:
				self.logger.exception(
					f"Failed validation of dataset {source}, as contents from {fetchable_uri} could not be fetched"
				)

				failed_validation = {
					"reason": "unexpected",
					"description": f"Contents from {fetchable_uri} could not be fetched",
				}
			except urllib.error.URLError as ue:
				self.logger.exception(
					f"Failed validation of dataset {source}, as contents from {fetchable_uri} could not be fetched"
				)

				failed_validation = {
					"reason": "unexpected",
					"description": f"Contents from {fetchable_uri} could not be fetched",
				}

		fetched_binary_data: "Optional[FetchedBinaryData]"
		if orig_validable_content is not None:
			assert fetchable_uri is not None
			fetched_binary_data = FetchedBinaryData(
				fetchable_uri=fetchable_uri,
				payload=orig_validable_content,
			)
		else:
			fetched_binary_data = None

		fetched_inline_data: "Optional[FetchedInlineData]"
		if the_data is not None:
			fetched_inline_data = FetchedInlineData(
				data=the_data,
				schema_uri=schema_uri,
				remote=fetched_binary_data,
				is_json=is_json,
			)
		else:
			fetched_inline_data = None

		return (
			failed_validation,
			fetched_inline_data
			if fetched_inline_data is not None
			else fetched_binary_data,
		)
