#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2023 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import inspect
import json
import logging
import urllib.parse
import urllib.request
import uuid

from typing import (
	cast,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	from types import ModuleType
	from typing import (
		Any,
		IO,
		Mapping,
		MutableMapping,
		Optional,
		Union,
	)

	from typing_extensions import (
		TypedDict,
		Required,
		NotRequired,
	)

	OEBPublishServer = TypedDict(
		"OEBPublishServer",
		{
			"type": "Required[str]",
			"token": "NotRequired[str]",
			"sandbox": "NotRequired[bool]",
		},
	)

	OEBCredentials = TypedDict(
		"OEBCredentials",
		{
			"user": "Required[str]",
			"pass": "Required[str]",
			"authURI": "NotRequired[str]",
			"clientId": "NotRequired[str]",
			"grantType": "NotRequired[str]",
			"publishServer": "NotRequired[OEBPublishServer]",
		},
	)

DEFAULT_AUTH_URI = (
	"https://inb.bsc.es/auth/realms/openebench/protocol/openid-connect/token"
)
DEFAULT_CLIENT_ID = "THECLIENTID"
DEFAULT_GRANT_TYPE = "password"


class OEBCredentialsManager:
	def __init__(
		self, baseUrl: "str", oeb_credentials: "Optional[Union[OEBCredentials, str]]"
	):
		if not baseUrl.endswith("/"):
			baseUrl += "/"
		self.baseUrl = baseUrl

		self.oeb_credentials: "Optional[OEBCredentials]" = None
		self.oeb_token: "Optional[str]" = None
		if isinstance(oeb_credentials, str):
			self.oeb_token = oeb_credentials
		elif isinstance(oeb_credentials, dict):
			self.oeb_credentials = oeb_credentials

		self.logger = logging.getLogger(
			dict(inspect.getmembers(self))["__module__"]
			+ "::"
			+ self.__class__.__name__
		)

	def get_credentials_user(self) -> "str":
		"""
		This method is needed for caching purposes
		"""

		return "" if self.oeb_credentials is None else self.oeb_credentials["user"]

	def getAccessToken(self) -> "Optional[str]":
		if self.oeb_token is None:
			if self.oeb_credentials is None:
				return None

			authURI = self.oeb_credentials.get("authURI", DEFAULT_AUTH_URI)
			payload = {
				"client_id": self.oeb_credentials.get("clientId", DEFAULT_CLIENT_ID),
				"grant_type": self.oeb_credentials.get("grantType", DEFAULT_GRANT_TYPE),
				"username": self.oeb_credentials["user"],
				"password": self.oeb_credentials["pass"],
			}

			req = urllib.request.Request(
				authURI,
				data=urllib.parse.urlencode(payload).encode("utf-8"),
				method="POST",
			)
			with urllib.request.urlopen(req) as t:
				token = json.load(t)

				# self.logger.info("Token {}".format(token['access_token']))
				# Token is cached
				self.oeb_token = cast("str", token["access_token"])

		return self.oeb_token

	def getHTTPAuthHeader(self) -> "MutableMapping[str, str]":
		# Security token
		bearer: "Optional[str]" = None
		bearer = self.getAccessToken()

		if bearer is not None:
			return {
				"Authorization": f"Bearer {bearer}",
			}
		else:
			return {}

	def oebRequest(
		self,
		flavor: "Optional[str]",
		concept: "Optional[str]" = None,
		_id: "Optional[str]" = None,
		method: "str" = "GET",
		accept: "str" = "application/json",
		params: "Optional[Mapping[str, str]]" = None,
		data: "Optional[IO[bytes]]" = None,
	) -> "urllib.request.Request":
		# Security token
		auth_header = self.getHTTPAuthHeader()
		# Preparation
		baseUrl = self.baseUrl
		if flavor is not None:
			baseUrl += urllib.parse.quote_plus(flavor) + "/"
			if concept is not None:
				baseUrl += urllib.parse.quote_plus(concept) + "/"
				if _id is not None:
					baseUrl += urllib.parse.quote_plus(_id) + "/"

		# Now, the inline params
		if isinstance(params, dict) and len(params) > 0:
			baseUrl += "?" + urllib.parse.urlencode(params, encoding="utf-8")

		oebHeaders = {
			"Accept": accept,
		}
		if auth_header:
			self.logger.debug("Fetching sandbox report bearer")
			oebHeaders.update(auth_header)
		if method != "GET":
			oebHeaders["Content-Type"] = accept

		req = urllib.request.Request(
			baseUrl, headers=oebHeaders, method=method, data=data
		)

		return req

	def oebPayloadRequest(
		self,
		_id: "Optional[str]" = None,
		method: "str" = "PUT",
		content_type: "str" = "application/octet-stream",
		metadata: "Optional[Mapping[str, Any]]" = None,
		data: "Optional[IO[bytes]]" = None,
	) -> "urllib.request.Request":
		# Security token
		auth_header = self.getHTTPAuthHeader()

		# Preparation
		baseUrl = self.baseUrl
		baseUrl += "payload/"

		# Should we generate a random id?
		if _id is None:
			_id = uuid.uuid4().urn

		baseUrl += urllib.parse.quote_plus(_id)

		oebHeaders: "MutableMapping[str, str]" = {}
		if auth_header:
			self.logger.debug("Using bearer token to push")
			oebHeaders.update(auth_header)
		if data is None:
			method = "GET"
		# Now, the metadata
		if metadata is not None and len(metadata) > 0:
			oebHeaders["X-OEB-METADATA"] = json.dumps(metadata)

		oebHeaders[
			"Accept" if method in ("GET", "HEAD") else "Content-Type"
		] = content_type

		req = urllib.request.Request(
			baseUrl, headers=oebHeaders, method=method, data=data
		)

		return req
