#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2023 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import atexit
import datetime
import enum
import gzip
import io
import json
import logging
import os
import shutil
import sys
import tempfile
import urllib.error
import urllib.parse
import urllib.request

from typing import (
	cast,
	no_type_check,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	import pathlib

	from types import ModuleType

	from typing import (
		Any,
		IO,
		Iterator,
		Mapping,
		MutableSequence,
		Optional,
		Sequence,
		Tuple,
		Union,
	)

	from extended_json_schema_validator.extensions.abstract_check import (
		SchemaHashEntry,
	)

	from .auth import (
		OEBCredentials,
	)

	from .common import (
		YieldedInput,
	)

# This is needed to properly compute when to externalize an inline_data
import bson
from pymongo.common import MAX_BSON_SIZE

import rfc6920.methods
import yaml

from .common import (
	FLAVOR_SANDBOX,
	FLAVOR_STAGED,
)

from .fetch import (
	OEBFetcher,
	MIME_IDLIST,
	OEB_ID_PREFIX,
)

from .schemas import (
	OEBSchemasManager,
)

STAGED_ROUTING = {"Reference", "Community", "Contact"}


class PayloadMode(enum.Enum):
	# As is, no change to the dataset entry
	AS_IS = "as-is"
	THRESHOLD = "threshold"
	FORCE_INLINE = "force-inline"
	FORCE_PAYLOAD = "force-payload"

	def __str__(self) -> "str":
		return self.value


class OEBUploader(OEBFetcher):
	def __init__(
		self,
		baseUrl: "str",
		oeb_credentials: "Optional[Union[OEBCredentials, str]]" = None,
		cache_entry_expire: "Optional[Union[int, float]]" = None,
		cache_dir: "Optional[str]" = None,
		raw_cache_dir: "Optional[str]" = None,
	):
		super().__init__(
			baseUrl,
			oeb_credentials=oeb_credentials,
			cache_entry_expire=cache_entry_expire,
			cache_dir=cache_dir,
			raw_cache_dir=raw_cache_dir,
		)

		self.schemas_manager = OEBSchemasManager(self)

	def data_upload(
		self,
		community_id: "str",
		entries: "Sequence[YieldedInput]",
		valReportFile: "str",
		data_model_dir: "Union[str, Tuple[str, Sequence[SchemaHashEntry]]]",
		deep_schemas_dir: "Optional[Union[str, Tuple[str, Sequence[SchemaHashEntry]]]]" = None,
		payload_mode: "PayloadMode" = PayloadMode.AS_IS,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> None:
		concept_ids_map = self.fetchIdsAndOrigIds(
			use_cache=use_cache, override_cache=override_cache
		)
		# First, let's check the validity of the entries
		# keeping the external payloads for possible further processing
		# when we have to change from external to internal
		schema_prefix = self.schemas_manager.validate(
			entries,
			valReportFile,
			data_model_dir=data_model_dir,
			flavor=FLAVOR_STAGED,
			concept_ids_map=concept_ids_map,
			deep_schemas_dir=deep_schemas_dir,
			keep_external_payloads=payload_mode == PayloadMode.FORCE_INLINE,
			use_cache=use_cache,
			override_cache=override_cache,
		)

		# Time to classify entries
		_, sandboxF = tempfile.mkstemp(prefix="oeb-up", suffix="sandbox.gz")
		# logger.debug(f"tmp file {sandboxF}")
		atexit.register(os.remove, sandboxF)
		_, stagedF = tempfile.mkstemp(prefix="oeb-up", suffix="staged.gz")
		atexit.register(os.remove, stagedF)

		len_schema_prefix = len(schema_prefix)
		sandboxH = None
		stagedH = None
		for y_entry in entries:
			entry = y_entry.content
			_id = entry.get("_id")
			schema_id = entry.get("_schema")
			if (schema_id is not None) and schema_id.startswith(schema_prefix):
				relConcept = schema_id[len_schema_prefix:]
				isFirst = False
				if relConcept in STAGED_ROUTING:
					self.logger.debug(f"Staged entry {_id} [{y_entry.path}]")
					if stagedH is None:
						isFirst = True
						stagedH = gzip.open(stagedF, mode="wt", encoding="utf-8")

					theH = stagedH
				else:
					self.logger.debug(f"Sandbox entry {_id} [{y_entry.path}]")
					if sandboxH is None:
						isFirst = True
						sandboxH = gzip.open(sandboxF, mode="wt", encoding="utf-8")

					theH = sandboxH

				sep = "[" if isFirst else ","

				# Print the entry separator
				print(sep, file=theH)

				# Now, the possible conversions
				if payload_mode != PayloadMode.AS_IS and relConcept == "Dataset":
					if payload_mode == PayloadMode.FORCE_INLINE:
						# Remote payloads have to be inlined
						# Let's trust we got here the payload
						if y_entry.payload is not None:
							orig_datalink = entry["datalink"]
							vc_text = io.TextIOWrapper(
								y_entry.payload, encoding="utf-8"
							)
							inline_data = json.load(vc_text)
							del vc_text

							entry["datalink"] = {
								"inline_data": inline_data,
								"schema_url": orig_datalink["schema_uri"],
							}
					else:
						# Inline payloads have to externalized
						# either over a threshold or always
						orig_datalink = entry["datalink"]
						inline_data = entry.get("inline_data")

						if inline_data is not None:
							externalize_it = payload_mode == PayloadMode.FORCE_PAYLOAD

							# Measure by threshold
							if not externalize_it:
								externalize_it = (
									len(bson.encode(entry)) >= MAX_BSON_SIZE
								)

							if externalize_it:
								payload_uri = self.upload_payload_from_json(
									inline_data,
									metadata={"comment": "Externalized inline data"},
								)
								entry["datalink"] = {
									"schema_uri": orig_datalink["schema_url"],
									"uri": payload_uri,
									"attrs": [
										"inline",
									],
									"validation_date": datetime.datetime.utcnow()
									.astimezone()
									.isoformat(),
									"status": "ok",
								}

				json.dump(entry, theH)
			else:
				self.logger.error(
					f"Something strange happening for entry {_id} from {y_entry.path}, with unmatching schema_id {schema_id}"
				)

		# Assuring all the content is stored,
		# and preparing reading it from the requests
		commonParams = {"community_id": community_id}

		if stagedH is not None:
			print("]", file=stagedH)
			stagedH.close()
			with gzip.open(stagedF, mode="rb") as stagedHB:
				stagedReq = self.oebRequest(
					flavor=FLAVOR_STAGED,
					method="POST",
					params=commonParams,
					data=cast("IO[bytes]", stagedHB),
				)
				self.logger.debug(
					f"{stagedReq.get_method()} {stagedReq.get_full_url()}"
				)

				try:
					with urllib.request.urlopen(stagedReq) as ir:
						shutil.copyfileobj(ir, sys.stderr.buffer)
				except urllib.error.HTTPError as he:
					# Saving the error report to the file
					with open(valReportFile, mode="wb") as vH:
						shutil.copyfileobj(he, vH)
					self.logger.error(
						f"Error {he.code} {he.reason} (see server report at {valReportFile})"
					)
					raise he

		else:
			stagedReq = None

		if sandboxH is not None:
			print("]", file=sandboxH)
			sandboxH.close()
			with gzip.open(sandboxF, mode="rb") as sandboxHB:
				sandboxReq = self.oebRequest(
					flavor=FLAVOR_SANDBOX,
					method="POST",
					params=commonParams,
					data=cast("IO[bytes]", sandboxHB),
				)
				self.logger.debug(
					f"{sandboxReq.get_method()} {sandboxReq.get_full_url()}"
				)

				try:
					with urllib.request.urlopen(sandboxReq) as ir:
						shutil.copyfileobj(ir, sys.stderr.buffer)
				except urllib.error.HTTPError as he:
					# Saving the error report to the file
					with open(valReportFile, mode="wb") as vH:
						shutil.copyfileobj(he, vH)
					self.logger.error(
						f"Error {he.code} {he.reason} (see server report at {valReportFile})"
					)
					raise he
		else:
			sandboxReq = None

	def upload_payload_from_file(
		self,
		file_to_upload: "Union[str, pathlib.Path]",
		metadata: "Optional[Mapping[str, Any]]" = None,
		force: "bool" = False,
	) -> "str":
		with open(file_to_upload, mode="rb") as upstream:
			return self.upload_payload_from_stream(
				upstream, metadata=metadata, force=force
			)

	def upload_payload_from_json(
		self,
		json_payload: "Any",
		metadata: "Optional[Mapping[str, Any]]" = None,
		force: "bool" = False,
	) -> "str":
		bio_json = io.BytesIO()
		tiow_json = io.TextIOWrapper(bio_json, encoding="utf-8")
		json.dump(json_payload, tiow_json, sort_keys=True)
		tiow_json.flush()
		return self.upload_payload_from_stream(
			bio_json,
			metadata=metadata,
			force=force,
		)

	def upload_payload_from_stream(
		self,
		stream: "IO[bytes]",
		metadata: "Optional[Mapping[str, Any]]" = None,
		force: "bool" = False,
	) -> "str":
		# Step 0: get the nih from the rewindable stream
		rewind_point = stream.tell()
		nih_id = rfc6920.methods.generate_nih(stream)
		stream.seek(rewind_point)

		do_upload = force
		full_url = ""
		if not force:
			cReq = self.oebPayloadRequest(
				method="HEAD",
				_id=nih_id,
				content_type="*/*",
			)
			try:
				headR = urllib.request.urlopen(cReq)
				full_url = cReq.full_url
			except urllib.error.HTTPError as he:
				# A 404 means the entry does not exist
				if he.code != 404:
					raise he
				do_upload = True

		if do_upload:
			pReq = self.oebPayloadRequest(
				_id=nih_id,
				data=stream,
				metadata=metadata,
			)
			with urllib.request.urlopen(pReq) as pr:
				shutil.copyfileobj(pr, sys.stderr.buffer)
			full_url = pReq.full_url

		return full_url

	def payload_uploader(
		self,
		entries: "Sequence[Tuple[pathlib.Path, Mapping[str, Any]]]",
	) -> "Sequence[Tuple[pathlib.Path, str]]":
		uploaded = []
		for entry in entries:
			payload_url = self.upload_payload_from_file(
				entry[0],
				metadata=entry[1],
				force=False,
			)
			self.logger.debug(f"\t{entry[0]} => {payload_url}")
			uploaded.append((entry[0], payload_url))

		return uploaded

	def remove_ids(
		self,
		ids_to_remove: "Sequence[str]",
		flavor: "str" = FLAVOR_STAGED,
	) -> "Tuple[int, int, int, int]":
		numRemoved = 0
		numErr = 0
		numIgnored = 0
		numTotal = len(ids_to_remove)
		prev_batch_remove: "Sequence[str]" = []
		batch_remove = ids_to_remove
		gen = 0
		while len(batch_remove) > 0 and len(prev_batch_remove) != len(batch_remove):
			gen += 1
			self.logger.debug(f"Removal iteration {gen}")
			# Switching roles for the "swarm"
			prev_batch_remove = batch_remove
			batch_remove = []
			# This number has to be resetted everytime
			numErr = 0
			for id_to_remove in prev_batch_remove:
				for idPref, concept in OEB_ID_PREFIX:
					if id_to_remove.startswith(idPref):
						try:
							rReq = self.oebRequest(
								flavor=flavor,
								concept=concept,
								_id=id_to_remove,
								method="DELETE",
							)
							with urllib.request.urlopen(rReq) as rr:
								shutil.copyfileobj(rr, sys.stderr.buffer)

							numRemoved += 1
						except urllib.error.HTTPError as he:
							if he.code == 404:
								self.logger.info(
									f"Discarding {id_to_remove}, does not exist"
								)
								numIgnored += 1
							else:
								batch_remove.append(id_to_remove)
								numErr += 1
								self.logger.exception(
									f"[NOT REMOVED iter {gen}] Entry {id_to_remove}\n{he.read().decode()}"
								)
						except Exception as e:
							batch_remove.append(id_to_remove)
							numErr += 1
							self.logger.exception(f"Unexpected exception {e}")

						break
				else:
					self.logger.info(f"Ignoring {id_to_remove}, could not process")
					numIgnored += 1

		self.logger.debug(
			f"Removal stats: removed {numRemoved}, error {numErr}, ignored {numIgnored}, total {numTotal}, iterations {gen}"
		)

		return numRemoved, numErr, numIgnored, numTotal
