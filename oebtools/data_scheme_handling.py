#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2023 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
from typing import (
	cast,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	from typing import (
		Any,
		IO,
	)

import data_url  # type: ignore[import]
import magic
import jq  # type: ignore[import]


def file2data(filename: "str") -> "str":
	with open(filename, mode="rb") as fH:
		content = fH.read()
		mime_type = magic.from_buffer(content, mime=True)

		if mime_type is None:
			mime_type = "application/octet-stream"

	return cast(
		"str",
		data_url.construct_data_url(
			mime_type=mime_type, base64_encoded=True, data=content
		),
	)


def data2stream(raw_url: "str", oH: "IO[bytes]") -> "int":
	url = data_url.DataURL.from_url(raw_url)

	return oH.write(url.data)


def attach_to_json(json_input: "Any", jq_expr: "str", data_url: "str") -> "Any":
	json_output = jq.compile(f'{jq_expr} = "{data_url}"').input(json_input).all()

	if not isinstance(json_input, list):
		json_output = json_output[0]

	return json_output


def detach_from_json(json_input: "Any", jq_expr: "str", oH: "IO[bytes]") -> "int":
	json_output = jq.compile(jq_expr).input(json_input).all()

	if not isinstance(json_input, list):
		json_output = json_output[0]

	return data2stream(json_output, oH)
