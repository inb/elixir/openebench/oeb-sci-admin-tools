#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2023 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import atexit
import inspect
import io
import json
import logging
import os
import shutil
import subprocess
import sys
import tempfile
import urllib.parse
import urllib.request

import ijson  # type: ignore[import]
import magic
import yaml

from typing import (
	cast,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	import pathlib

	from typing import (
		Any,
		Iterator,
		Mapping,
		MutableMapping,
		MutableSequence,
		Optional,
		Sequence,
		Tuple,
		Union,
	)

	from extended_json_schema_validator.extensions.abstract_check import SchemaHashEntry

	from extended_json_schema_validator.extensible_validator import (
		ParsedContentEntry,
	)

	from .auth import (
		OEBCredentialsManager,
	)

	from .common import (
		YieldedInput,
	)

	from .fetch import (
		OEBAbstractFetcher,
		OEBFetcher,
	)

from extended_json_schema_validator.extensible_validator import ExtensibleValidator

from .common import (
	DEFAULT_BDM_TAG,
	FakingSpooledTemporaryFile,
	FLAVOR_SANDBOX,
	FLAVOR_STAGED,
)

from .fetch import (
	DEFAULT_CONCEPTS,
	FetchedInlineData,
	MIME_IDLIST,
)

BDM_REPO = "https://github.com/inab/benchmarking-data-model.git"
DEFAULT_DEEP_REPOURL = "https://github.com/inab/OEB_level2_data_migration.git"
DEFAULT_DEEP_REL_PATH = "oeb_level2/schemas"
MIME_SCHEMA = "application/schema+json"
DEFAULT_GIT_CMD = "git"

MIN_W3ID_OEB_SCHEMAS_VERSION = "2.0"

W3ID_OPENEBENCH_PREFIX = "https://w3id.org/openebench/"
W3ID_OPENEBENCH_SCIENTIFIC_SCHEMAS_PREFIX = (
	W3ID_OPENEBENCH_PREFIX + "scientific-schemas/"
)


class OEBSchemasManager:
	def __init__(self, fetcher: "OEBFetcher"):
		self.credentials_manager: "OEBCredentialsManager" = fetcher
		self.fetcher = fetcher
		self.baseUrl = self.fetcher.baseUrl

		self.logger = logging.getLogger(
			dict(inspect.getmembers(self))["__module__"]
			+ "::"
			+ self.__class__.__name__
		)

	def checkoutRepo(
		self,
		checkout_dir: "Optional[str]" = None,
		git_repo: "Optional[str]" = None,
		tag: "str" = DEFAULT_BDM_TAG,
		rel_path: "Optional[str]" = None,
	) -> "str":
		schema_prefix = None
		if checkout_dir is not None:
			os.makedirs(checkout_dir, exist_ok=True)
		else:
			# Creation of the temporary directory where we are going to
			# check out the benchmarking data model
			checkout_dir = tempfile.mkdtemp(prefix="oeb", suffix="fetched-schema")
			atexit.register(shutil.rmtree, checkout_dir, ignore_errors=True)

		# Pull only if it is needed
		if git_repo is not None and len(os.listdir(checkout_dir)) == 0:
			# Try cloning the repository without initial checkout
			gitclone_params = [
				DEFAULT_GIT_CMD,
				"clone",
				"-n",
				"--recurse-submodules",
				git_repo,
				checkout_dir,
			]

			# Now, checkout the specific commit
			gitcheckout_params = [DEFAULT_GIT_CMD, "checkout", tag]

			# Last, initialize submodules
			gitsubmodule_params = [DEFAULT_GIT_CMD, "submodule", "update", "--init"]

			with tempfile.NamedTemporaryFile() as git_stdout:
				with tempfile.NamedTemporaryFile() as git_stderr:
					# First, bare clone
					retval = subprocess.call(
						gitclone_params, stdout=git_stdout, stderr=git_stderr
					)
					# Then, checkout
					if retval == 0:
						retval = subprocess.Popen(
							gitcheckout_params,
							stdout=git_stdout,
							stderr=git_stderr,
							cwd=checkout_dir,
						).wait()
					# Last, submodule preparation
					if retval == 0:
						retval = subprocess.Popen(
							gitsubmodule_params,
							stdout=git_stdout,
							stderr=git_stderr,
							cwd=checkout_dir,
						).wait()

					# Proper error handling
					if retval != 0:
						# Reading the output and error for the report
						with open(git_stdout.name, "r") as c_stF:
							git_stdout_v = c_stF.read()
						with open(git_stderr.name, "r") as c_stF:
							git_stderr_v = c_stF.read()

						errstr = "ERROR:  could not pull '{}' (tag '{}'). Retval {}\n======\nSTDOUT\n======\n{}\n======\nSTDERR\n======\n{}".format(
							git_repo, tag, retval, git_stdout_v, git_stderr_v
						)
						self.logger.fatal(errstr)
						raise Exception(errstr)

		self.logger.debug(f"Checkout {git_repo} in dir {checkout_dir}")
		schemas_path = checkout_dir
		if rel_path is not None:
			schemas_path = os.path.join(checkout_dir, rel_path)

		return schemas_path

	def checkoutFromW3ID(
		self,
		checkout_dir: "Optional[str]" = None,
		tag: "str" = MIN_W3ID_OEB_SCHEMAS_VERSION,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
		rel_path: "Optional[str]" = None,
	) -> "Union[Tuple[None, str], Tuple[str, MutableSequence[SchemaHashEntry]]]":
		memory_catalog: "Optional[MutableSequence[SchemaHashEntry]]"
		if checkout_dir is not None:
			memory_catalog = None
			os.makedirs(checkout_dir, exist_ok=True)
		else:
			memory_catalog = []

		# The REST prefix is provided in this parameter
		schema_headers = {
			"Accept": MIME_SCHEMA,
		}
		# Assuring properly built URLs
		restURL = W3ID_OPENEBENCH_SCIENTIFIC_SCHEMAS_PREFIX
		restURL += urllib.parse.quote(tag) + "/"
		schema_prefix: "Optional[str]" = None
		for conceptName in ("_shared", "", *DEFAULT_CONCEPTS):
			url = restURL + urllib.parse.quote(conceptName)
			self.logger.debug(f"Fetching a schema from {url}")
			with self.fetcher.openurl(
				url,
				headers=schema_headers,
				use_cache=use_cache,
				override_cache=override_cache,
			) as ir:
				if memory_catalog is not None:
					schemaObj: "SchemaHashEntry" = {
						"schema": json.load(ir),
						"file": url,
						"errors": [],
					}
					# Guessing the schema prefix for the whole catalog
					# from the first loaded JSON Schema
					if schema_prefix is None:
						theId = schemaObj["schema"].get("$id")
						if theId is not None:
							schema_prefix = theId[0 : theId.rindex("/") + 1]

					memory_catalog.append(schemaObj)
				else:
					assert checkout_dir is not None
					if len(conceptName) == 0:
						conceptName = "schema"
					schema_path = os.path.join(checkout_dir, conceptName + ".json")
					with open(schema_path, mode="wb") as sH:
						shutil.copyfileobj(ir, sH)

		if memory_catalog is not None:
			self.logger.debug(f"Checkout {restURL} schemas in memory")
		else:
			self.logger.debug(f"Checkout {restURL} schemas at {checkout_dir}")

		if checkout_dir is not None:
			return None, checkout_dir

		assert memory_catalog is not None
		assert schema_prefix is not None
		return schema_prefix, memory_catalog

	def checkoutSchemas(
		self,
		checkoutDir: "Optional[str]" = None,
		git_repo: "str" = BDM_REPO,
		tag: "str" = DEFAULT_BDM_TAG,
		fetchFromREST: "bool" = False,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Union[str, Tuple[str, Sequence[SchemaHashEntry]]]":
		schema_prefix = None
		memory_catalog: "Optional[MutableSequence[SchemaHashEntry]]"
		if checkoutDir is not None:
			memory_catalog = None
		else:
			memory_catalog = []

		if fetchFromREST:
			# The REST prefix is provided in this parameter
			schema_headers = {
				"Accept": MIME_SCHEMA,
			}
			# Assuring properly built URLs
			restURL = self.baseUrl
			restURL += FLAVOR_STAGED + "/"
			for conceptName in ("_shared", *DEFAULT_CONCEPTS):
				url = restURL + urllib.parse.quote(conceptName)
				self.logger.debug(f"Fetching a schema from {url}")
				with self.fetcher.openurl(
					url,
					headers=schema_headers,
					use_cache=use_cache,
					override_cache=override_cache,
				) as ir:
					if memory_catalog is not None:
						schemaObj: "SchemaHashEntry" = {
							"schema": json.load(ir),
							"file": url,
							"errors": [],
						}
						# Guessing the schema prefix for the whole catalog
						# from the first loaded JSON Schema
						if schema_prefix is None:
							theId = schemaObj["schema"].get("$id")
							if theId is not None:
								schema_prefix = theId[0 : theId.rindex("/") + 1]

						memory_catalog.append(schemaObj)
					else:
						assert checkoutDir is not None
						schema_path = os.path.join(checkoutDir, conceptName + ".json")
						with open(schema_path, mode="wb") as sH:
							shutil.copyfileobj(ir, sH)

			self.logger.debug(f"Checkout {restURL} schemas in memory")
		else:
			# Now, let's decide whether we are using w3id or GitHub paths
			tag_tuple = tuple(tag.split("."))
			if len(tag_tuple) == 2 and tag_tuple >= tuple(
				MIN_W3ID_OEB_SCHEMAS_VERSION.split(".")
			):
				schema_prefix, memory_catalog_or_checkout_dir = self.checkoutFromW3ID(
					checkoutDir,
					tag=tag,
					use_cache=use_cache,
					override_cache=override_cache,
				)
				if isinstance(memory_catalog_or_checkout_dir, str):
					checkoutDir = memory_catalog_or_checkout_dir
				else:
					memory_catalog = memory_catalog_or_checkout_dir
			else:
				checkoutDir = self.checkoutRepo(checkoutDir, git_repo=git_repo, tag=tag)

		if memory_catalog is not None:
			assert schema_prefix is not None
			return schema_prefix, memory_catalog
		elif checkoutDir is not None:
			return checkoutDir
		else:
			raise AssertionError("Should be impossible")

	def loadSchemas(
		self,
		data_model_dir: "Union[str, Tuple[str, Sequence[SchemaHashEntry]]]",
		tag: "str" = DEFAULT_BDM_TAG,
	) -> "Tuple[str, Union[str, Sequence[SchemaHashEntry]]]":
		# All the work was already done before arriving here
		if isinstance(data_model_dir, tuple):
			return data_model_dir

		base_schema_dir = os.path.join(data_model_dir, "json-schemas")
		if os.path.isdir(base_schema_dir):
			schema_dir = os.path.join(base_schema_dir, tag)
		else:
			schema_dir = data_model_dir
		self.logger.debug(f"Schema dir {schema_dir}")
		# Now, guessing the prefix
		schema_prefix = None
		with os.scandir(schema_dir) as dmit:
			for entry in dmit:
				if (
					entry.name.endswith(".json")
					or entry.name.endswith(".yaml")
					or entry.name.endswith(".yml")
				) and entry.is_file():
					with open(entry.path, mode="r", encoding="utf-8") as jschH:
						jsch = json.load(jschH)
						theId = jsch.get("$id")
						if theId is not None:
							schema_prefix = theId[0 : theId.rindex("/") + 1]
							break
					try:
						try:
							with open(entry.path, mode="r", encoding="utf-8") as jschH:
								jsch = json.load(jschH)
						except json.decoder.JSONDecodeError as jde:
							with open(entry.path, mode="r", encoding="utf-8") as jschH:
								try:
									jsch = yaml.safe_load(jschH)
								except yaml.error.MarkedYAMLError as mye:
									self.logger.critical(
										f"Unable to parse schema file {entry.path} neither as JSON or YAML. Reasons: {jde.msg} {str(mye)}"
									)
									continue

						theId = jsch.get("$id")
						if theId is not None:
							schema_prefix = theId[0 : theId.rindex("/") + 1]
							break
					except IOError as ioe:
						self.logger.critical(
							f"Unable to open/read schema file {entry.path}. Reason: {ioe.strerror}"
						)

		assert schema_prefix is not None

		return schema_prefix, schema_dir

	def _setupValidator(
		self,
		data_model_dir: "Union[str, Tuple[str, Sequence[SchemaHashEntry]]]",
		flavor: "str" = FLAVOR_SANDBOX,
		concept_ids_map: "Mapping[str, Sequence[str]]" = {},
	) -> "Tuple[ExtensibleValidator, str]":
		baseUrl = self.baseUrl
		baseUrl += urllib.parse.quote_plus(flavor) + "/"

		schema_prefix, schemas_dir = self.loadSchemas(data_model_dir)
		self.logger.debug(f"Schema prefix {schema_prefix}")

		# Populate with both OEB ids *and* orig_ids
		ejvConfig = {
			"primary_key": {
				"provider": [baseUrl],
				"allow_provider_duplicates": True,
				"schema_prefix": schema_prefix,
				"accept": MIME_IDLIST,
			}
		}
		self.logger.debug(
			f"Validation config (before inline provider)\n{json.dumps(ejvConfig, indent=4, sort_keys=True)}"
		)
		# Now, the inline, fetched ids and orig_ids
		if len(concept_ids_map) > 0:
			inline_provider = {}
			for concept_name, ids_and_orig_ids in concept_ids_map.items():
				inline_provider[schema_prefix + concept_name] = ids_and_orig_ids

			ejvConfig["primary_key"]["inline_provider"] = inline_provider

		# logger.debug(f"Validation config\n{json.dumps(ejvConfig, indent=4, sort_keys=True)}")
		ejv = ExtensibleValidator(config=ejvConfig, isRW=True)
		if isinstance(schemas_dir, list):
			schemas_param = schemas_dir
		else:
			schemas_param = [schemas_dir]
		numSchemas = ejv.loadJSONSchemas(*schemas_param, verbose=False)
		self.logger.debug(f"Number of schemas {numSchemas}")

		if numSchemas == 0:
			self.logger.error("No correct schema was found. Quack!")
			raise Exception("No correct schema was found. Quack!")

		return ejv, schema_prefix

	def _setupDeepValidator(
		self,
		data_model_dir: "Union[str, Tuple[str, Sequence[SchemaHashEntry]]]",
	) -> "ExtensibleValidator":
		schema_prefix, schemas_dir = self.loadSchemas(data_model_dir)

		# logger.debug(f"Validation config\n{json.dumps(ejvConfig, indent=4, sort_keys=True)}")
		deep_ejv = ExtensibleValidator(isRW=True)
		if isinstance(schemas_dir, list):
			schemas_param = schemas_dir
		else:
			schemas_param = [schemas_dir]
		numSchemas = deep_ejv.loadJSONSchemas(*schemas_param, verbose=False)
		self.logger.debug(f"Number of deep validation schemas {numSchemas}")

		if numSchemas == 0:
			self.logger.error(
				"No correct schema for deep validation was found. Baaaahh!"
			)
			raise Exception("No correct schema for deep validation was found. Baaaahh!")

		return deep_ejv

	def _validate(
		self,
		entries: "Sequence[YieldedInput]",
		data_model_dir: "Union[str, Tuple[str, Sequence[SchemaHashEntry]]]",
		flavor: "str" = FLAVOR_SANDBOX,
		concept_ids_map: "Mapping[str, Sequence[str]]" = {},
		deep_schemas_dir: "Optional[Union[str, Tuple[str, Sequence[SchemaHashEntry]]]]" = None,
		keep_external_payloads: "bool" = False,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "Tuple[Sequence[ParsedContentEntry], str]":
		ejv, schema_prefix = self._setupValidator(
			data_model_dir=data_model_dir,
			flavor=flavor,
			concept_ids_map=concept_ids_map,
		)

		# Should we instantiate a an additional validator?
		deep_ejv: "Optional[ExtensibleValidator]" = None
		if deep_schemas_dir is not None:
			deep_ejv = self._setupDeepValidator(data_model_dir=deep_schemas_dir)
			# Avoid confusing messages from the deep validator
			deep_ejv.logger.setLevel(logging.CRITICAL)

		ejv.warmUpCaches()
		evEntries: "MutableSequence[ParsedContentEntry]" = []
		# Giving the right infrastructure to be validated
		evEntryMap = {}
		for entry in entries:
			evEntry: "ParsedContentEntry" = {
				"file": str(entry.path),
				"json": entry.content,
				"errors": [],
			}
			evEntryMap[id(evEntry)] = entry
			evEntries.append(evEntry)
		failed = []
		deep_entries = []
		for evaEnt in ejv.jsonValidateIter(*evEntries, verbose=False):
			if len(evaEnt["errors"]) > 0:
				if isinstance(evaEnt["json"], dict):
					evaEnt["annot"] = evaEnt["json"].get("_id")
				del evaEnt["json"]
				failed.append(evaEnt)
			elif (
				deep_ejv is not None
				and evaEnt["schema_id"] == schema_prefix + "Dataset"
			):
				# Deep validation is only available for Datasets
				# which have a datalink fulfilling the preconditions
				datalink = evaEnt["json"].get("datalink")
				if isinstance(datalink, dict):
					schema_uri = None
					if isinstance(datalink.get("uri"), str):
						schema_uri = datalink.get("schema_uri")

					elif isinstance(datalink.get("inline_data"), dict):
						schema_uri = datalink.get("schema_url")

					if schema_uri is not None:
						if schema_uri in deep_ejv.getValidSchemas():
							deep_entries.append(
								(schema_uri, datalink, evaEnt, evEntryMap[id(evaEnt)])
							)
						else:
							self.logger.debug(
								f"Skipping deep validation of {evaEnt['file']} as schema {schema_uri} is not found"
							)

		# Now, the deep validation
		if deep_ejv is not None and len(deep_entries) > 0:
			auth_header = self.credentials_manager.getHTTPAuthHeader()
			for schema_uri, datalink, evaEnt, entry in deep_entries:
				failed_validation = False

				# First, fetch content (but only the validable one!)
				(
					the_error,
					fetched_inline_data,
				) = self.fetcher.fetchInlineDataFromDatalink(
					evaEnt["file"],
					datalink,
					discard_unvalidable=True,
					use_cache=use_cache,
					override_cache=override_cache,
				)

				if the_error is not None:
					evaEnt["errors"].append(the_error)

					failed_validation = True

				if isinstance(fetched_inline_data, FetchedInlineData):
					assert schema_uri == fetched_inline_data.schema_uri
					the_data = fetched_inline_data.data
					is_json = fetched_inline_data.is_json
					fetchable_uri: "Optional[str]"
					orig_validable_content: "Optional[FakingSpooledTemporaryFile]"
					if fetched_inline_data.remote is not None:
						fetchable_uri = fetched_inline_data.remote.fetchable_uri
						orig_validable_content = fetched_inline_data.remote.payload
					else:
						fetchable_uri = None
						orig_validable_content = None

					if fetched_inline_data.schema_uri is None:
						guess_unmatched = True
					else:
						guess_unmatched = [fetched_inline_data.schema_uri]
					deep_eval = deep_ejv.jsonValidate(
						{
							"json": the_data,
							"file": evaEnt["file"]
							+ "<"
							+ ("inline" if fetchable_uri is None else fetchable_uri)
							+ ">",
							"errors": [],
						},
						guess_unmatched=guess_unmatched,
						verbose=False,
					)
					if len(deep_eval[0]["errors"]) > 0:
						evaEnt = deep_eval[0]
						failed_validation = True
					elif orig_validable_content is not None:
						if keep_external_payloads:
							if is_json:
								validable_content = orig_validable_content
							else:
								orig_validable_content.close()
								# We need to save it as JSON, in order to ease
								# the work for the upload tasks
								validable_content = FakingSpooledTemporaryFile(
									max_size=1024 * 1024
								)
								vc_text = io.TextIOWrapper(
									validable_content, encoding="utf-8"
								)
								json.dump(the_data, vc_text)
								vc_text.flush()
								del vc_text

							# Saving the payload for further processing
							validable_content.seek(0)
							entry.payload = validable_content
						else:
							# Freeing resources
							orig_validable_content.close()

				if failed_validation:
					if isinstance(evaEnt["json"], dict):
						evaEnt["annot"] = evaEnt["json"].get("_id")
					del evaEnt["json"]
					failed.append(evaEnt)

		return failed, schema_prefix

	def validate(
		self,
		entries: "Sequence[YieldedInput]",
		valReportFile: "str",
		data_model_dir: "Union[str, Tuple[str, Sequence[SchemaHashEntry]]]",
		flavor: "str" = FLAVOR_SANDBOX,
		concept_ids_map: "Mapping[str, Sequence[str]]" = {},
		deep_schemas_dir: "Optional[Union[str, Tuple[str, Sequence[SchemaHashEntry]]]]" = None,
		keep_external_payloads: "bool" = False,
		use_cache: "bool" = True,
		override_cache: "bool" = False,
	) -> "str":
		self.logger.debug(f"Received {len(entries)} entries to validate")
		failed, schema_prefix = self._validate(
			entries,
			data_model_dir=data_model_dir,
			flavor=flavor,
			concept_ids_map=concept_ids_map,
			deep_schemas_dir=deep_schemas_dir,
			keep_external_payloads=keep_external_payloads,
			use_cache=use_cache,
			override_cache=override_cache,
		)

		if len(failed) > 0:
			with open(valReportFile, mode="w", encoding="utf-8") as vH:
				json.dump(failed, vH, indent=4, sort_keys=True)
			self.logger.error(
				f"Contents did not validate, see report at {valReportFile}"
			)
			raise Exception(f"Contents did not validate, see report at {valReportFile}")
		else:
			self.logger.debug(f"All the {len(entries)} entries validated properly")

		return schema_prefix
