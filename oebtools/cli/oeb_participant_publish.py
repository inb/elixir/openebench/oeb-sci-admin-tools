#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import cgi
import copy
import io
import json
import logging
import os
import posixpath
import re
import shutil
import sys
import tempfile
import urllib.request

from typing import (
	cast,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	from typing import (
		Any,
		IO,
		Mapping,
		MutableMapping,
		MutableSequence,
		Optional,
		Sequence,
		Tuple,
		Type,
		Union,
	)

	from oebtools.publish import (
		AbstractPublisher,
	)

	from extended_json_schema_validator.extensions.abstract_check import (
		SchemaHashEntry,
	)

from . import (
	gen_base_param_parser,
	parse_args_and_init_app,
)

from ..common import (
	YieldedInput,
)

from ..uploader import (
	OEBUploader,
)

from ..publish.b2share import (
	B2SHAREPublisher,
)


PUBLISHERS = {
	B2SHAREPublisher.NAME(): B2SHAREPublisher,
}


PARTICIPANT_DATASETS_FROM_BE_GRAPHQL = """\
    getBenchmarkingEvents(benchmarkingEventFilters:{id: $benchmarking_event_id}) {
        _id
        name
        community_id
    }
    getChallenges(challengeFilters: {benchmarking_event_id: $benchmarking_event_id}) {
        _id
        participant_datasets: datasets(datasetFilters: {type: "participant"}) {
            _id
        }
    }
"""


def gen_inline_data_label_from_participant_dataset(
	par_dataset: "Mapping[str, Any]", default_label: "Optional[str]" = None
) -> "str":
	# First, look for the label in the participant dataset
	par_metadata = par_dataset.get("_metadata", {})
	par_label = (
		None if par_metadata is None else par_metadata.get("level_2:participant_id")
	)
	# Then, look for it in the assessment dataset
	if par_label is None:
		par_label = default_label

	# Now, trying pattern matching
	# to extract the label
	if par_label is None:
		match_p = re.search(
			r"Predictions made by (.*) participant", par_dataset["description"]
		)
		if match_p:
			par_label = match_p.group(1)

	# Last chance is guessing from the original id!!!!
	if par_label is None:
		par_orig_id = par_dataset.get("orig_id", par_dataset["_id"])
		if ":" in par_orig_id:
			par_label = par_orig_id[par_orig_id.index(":") + 1 :]
		else:
			par_label = par_orig_id

		# Removing suffix
		if par_label.endswith("_P"):
			par_label = par_label[:-2]

	return par_label


def main_method(
	benchmarking_event_id: "str",
	dryrun: "bool",
	uri_prefix: "Optional[str]",
	publisher_name: "str",
	publisher_clazz: "Type[AbstractPublisher]",
	publish_api_token: "str",
	sandbox: "bool",
	uploader: "OEBUploader",
	override_cache: "bool",
	skip_participant_update: "bool",
	logger: "logging.Logger",
	data_model_dir: "Union[str, Tuple[str, Sequence[SchemaHashEntry]]]",
) -> "int":
	logger.info(
		f"* Using {publisher_name} {'sandbox' if sandbox else 'production'} service"
	)

	publisher = publisher_clazz(
		sandbox=sandbox,
		token=publish_api_token,
	)

	logger.info(
		f"* Querying participant datasets from benchmarking event {benchmarking_event_id}"
	)
	part_by_challenge = uploader.query_graphql(
		PARTICIPANT_DATASETS_FROM_BE_GRAPHQL,
		variables={
			"benchmarking_event_id": benchmarking_event_id,
		},
		query_name="DatasetsFromBenchmarkingEvent",
		override_cache=override_cache,
	)

	be_mini_entry = part_by_challenge["data"]["getBenchmarkingEvents"][0]
	event_str = be_mini_entry["name"]
	community_id = be_mini_entry["community_id"]
	community_entry: "Optional[Mapping[str, Any]]" = None
	for concept_name, fetched_entry in uploader.fetchEntriesFromIds(
		[community_id], override_cache=override_cache
	):
		community_entry = fetched_entry
		break

	assert (
		community_entry is not None
	), f"No community entry could be found using {community_id}"
	community_acronym = community_entry.get("acronym", community_id)

	unique_participant_datasets = set(
		participant_dataset["_id"]
		for challenge in part_by_challenge["data"]["getChallenges"]
		for participant_dataset in challenge["participant_datasets"]
	)
	logger.info(
		f"\t{len(unique_participant_datasets)} unique participant datasets from {benchmarking_event_id}"
	)

	unique_contacts = set()
	logger.info(
		f"* Processing participant datasets from {benchmarking_event_id} in order to filter out and find unique contacts"
	)

	num_participant_datasets = 0
	clustered_participant_datasets: "MutableMapping[str, MutableSequence[Mapping[str, Any]]]" = (
		dict()
	)
	for concept_name, participant_dataset in uploader.fetchEntriesFromIds(
		unique_participant_datasets, override_cache=override_cache
	):
		participant_label = gen_inline_data_label_from_participant_dataset(
			participant_dataset
		)
		datalink = participant_dataset["datalink"]

		inline_data = datalink.get("inline_data")
		do_include = inline_data is not None
		if not do_include:
			uri = datalink["uri"]
			do_include = uri_prefix is None or uri.startswith(uri_prefix)

		if do_include:
			if inline_data is None:
				clustered_participant_datasets.setdefault(uri, []).append(
					participant_dataset
				)
			else:
				clustered_participant_datasets[
					"inline_" + participant_dataset["_id"]
				] = [
					participant_dataset
				]

			logger.debug(f"\t{participant_dataset['_id']} ({participant_label})")
			num_participant_datasets += 1
			unique_contacts.update(participant_dataset["dataset_contact_ids"])
		else:
			logger.debug(
				f"\tSKIPPED {participant_dataset['_id']} ({participant_label})"
			)

	logger.info(
		f"* {num_participant_datasets} processable participant datasets from {benchmarking_event_id}"
	)
	if dryrun:
		logger.info("DRY-RUN finishing here")
		return 0

	contacts = dict()
	for concept_name, contact in uploader.fetchEntriesFromIds(
		map(lambda uc: ("Contact", uc), unique_contacts),
		override_cache=override_cache,
	):
		contacts[contact["_id"]] = contact

	report = []
	for uri, participant_dataset_cluster in clustered_participant_datasets.items():
		first_participant_dataset = participant_dataset_cluster[0]
		first_participant_dataset_id = first_participant_dataset["_id"]
		participant_label = gen_inline_data_label_from_participant_dataset(
			first_participant_dataset
		)

		compound_participant_dataset_id = " ".join(
			map(lambda pd: pd["_id"], participant_dataset_cluster)
		)
		dotted_compound_participant_dataset_id = " ".join(
			map(lambda pd: pd["_id"] + ".1", participant_dataset_cluster)
		)

		logging.info(
			f"* Processing {len(participant_dataset_cluster)} datasets ({participant_label})"
		)

		internal_pid, doi, draft_record = publisher.book_pid()
		logging.info(f"\tCreated draft record {internal_pid} (future {doi})")

		# Let's either materialize or fetch the dataset
		datalink = first_participant_dataset["datalink"]

		inline_data = datalink.get("inline_data")
		filename_or_bio: "Union[str, IO[bytes]]"
		remote_filename: "Optional[str]"
		if inline_data is not None:
			ser = json.dumps(inline_data).encode("utf-8")
			filename_or_bio = io.BytesIO(ser)
			remote_filename = f"inline_data_{compound_participant_dataset_id}.json"
		else:
			# Some fast guess about whether it is a Nextcloud instance
			is_nextcloud = "/s/" in uri
			if is_nextcloud and "/download" not in uri:
				download_uri = uri + "/download"
			else:
				download_uri = uri

			filename_or_bio = tempfile.SpooledTemporaryFile(max_size=1024 * 1024)

			logging.info(f"\tFetching {download_uri}")
			with urllib.request.urlopen(download_uri) as dF:
				_, disp_params = cgi.parse_header(
					dF.headers.get("Content-Disposition", "")
				)
				remote_filename = disp_params.get("filename")
				if remote_filename is None:
					remote_filename = posixpath.basename(
						urllib.parse.urlparse(uri).path
					)
				if len(remote_filename) == 0:
					remote_filename = f"data_{compound_participant_dataset_id}"

				shutil.copyfileobj(dF, filename_or_bio)

			filename_or_bio.seek(0)

		logging.info(
			f"\tUploading {remote_filename} to draft record {internal_pid} (future {doi})"
		)
		uploaded_file = publisher.upload_file_to_draft(
			draft_record, filename_or_bio, remote_filename
		)

		logger.debug(f"{json.dumps(uploaded_file, indent=4)}")

		# Assigning authorship
		contact_email = None
		creators = []
		processed_contact_id = set()
		for participant_dataset in participant_dataset_cluster:
			for dataset_contact_id in participant_dataset["dataset_contact_ids"]:
				if dataset_contact_id in processed_contact_id:
					continue

				processed_contact_id.add(dataset_contact_id)

				contact = contacts[dataset_contact_id]
				if contact_email is None:
					contact_email = contact["email"][0]

				creators.append(
					{
						"creator_name": f"{contact['givenName']} {contact['surname']}",
					}
				)

		assert contact_email is not None
		updated_draft_record = publisher.update_record_metadata(
			draft_record,
			metadata={
				"titles": [
					{
						"title": f"{event_str} {first_participant_dataset['type']} dataset submitted by {participant_label}",
					}
				],
				"descriptions": [
					{
						"description": f"{first_participant_dataset['type'].capitalize()} dataset {compound_participant_dataset_id} submitted by {participant_label} for event {event_str}, organized by community {community_acronym}. {first_participant_dataset['description']}",
						"description_type": "Abstract",
					}
				],
				"disciplines": [
					{
						"classification_code": "3.1.4",
						"discipline_identifier": "3.1.4 Biology Bioinformatics",
						"discipline_name": "3.1.4 Biology Bioinformatics",
						"scheme": "b2share.legacy",
						"scheme_uri": "http://b2share.eudat.eu/suggest/disciplines.json",
					}
				],
				"keywords": [
					{
						"keyword": "benchmarking",
					},
				],
				"languages": [
					{"language_identifier": "eng", "language_name": "English"}
				],
				"license": {
					"license": "Creative Commons Attribution (CC-BY)",
					"license_uri": "http://creativecommons.org/licenses/by/4.0/",
				},
				"resource_types": [
					{
						"resource_type_description": "OpenEBench Benchmarking dataset",
						"resource_type_general": "Dataset",
					}
				],
				"contact_email": contact_email,
				"creators": creators,
			},
			community_specific_metadata={
				"oeb_community": community_acronym,
				"oeb_dataset_version": first_participant_dataset["version"],
				# This one does not currently work
				# with the OpenEBench B2SHARE schema
				# "oeb_id": dotted_compound_participant_dataset_id,
				"oeb_id": first_participant_dataset_id + ".1",
				"oeb_type": first_participant_dataset["type"],
			},
		)

		record = publisher.publish_draft_record(updated_draft_record)

		report.append((participant_label, record["metadata"]["DOI"]))
		if not skip_participant_update:
			entries_to_update = []
			for participant_dataset in participant_dataset_cluster:
				writable_participant_dataset = cast(
					"MutableMapping[str, Any]", copy.copy(participant_dataset)
				)
				datalink = writable_participant_dataset["datalink"]
				if "inline_data" in datalink:
					datalink = {}
					writable_participant_dataset["datalink"] = datalink

				datalink["attrs"] = ["archive"]
				writable_participant_dataset.setdefault("_metadata", {}).setdefault(
					"oeb:past_datalink_uris", []
				).append(datalink["uri"])
				datalink["uri"] = record["metadata"]["DOI"]
				datalink["status"] = "ok"
				datalink["validation_date"] = record["updated"]

				entries_to_update.append(
					YieldedInput(
						content=writable_participant_dataset,
						path=participant_dataset["_id"],
					)
				)

			uploader.data_upload(
				community_id=community_id,
				entries=entries_to_update,
				valReportFile="/dev/null",
				data_model_dir=data_model_dir,
				override_cache=override_cache,
			)

	logger.info("* Created entries:")
	for participant_label, doi in report:
		logging.info(f"\t{participant_label}\t{doi}")

	return 0


def main() -> None:
	ap = gen_base_param_parser(
		description="oeb-participant-publish",
		add_bdm_flags=True,
		required_creds_params=True,
		base_url="https://openebench.bsc.es/api/scientific",
	)

	ap.add_argument(
		"-ptk",
		"--publish_api_token",
		dest="publish_api_token",
		help="Token used to authenticate at publishing moment. If it is provided, it is used instead of the one in the configuration file.",
	)

	ap.add_argument(
		"--dataset-uri-prefix",
		dest="dataset_uri_prefix",
		help="Only process datasets which are either inline or have this URI prefix",
	)

	sch_sandbox = ap.add_mutually_exclusive_group()
	sch_sandbox.add_argument(
		"--sandbox",
		dest="sandbox",
		action="store_const",
		const=True,
		help="Should the publish be performed to the sandbox service?",
	)

	sch_sandbox.add_argument(
		"--publish",
		dest="sandbox",
		action="store_const",
		const=False,
		help="Should the publish be performed to the production service?",
	)

	ap.add_argument(
		"--input-id",
		dest="benchmarking_event_id",
		help="OEB benchmarking event entry id where to search the participants",
		required=True,
	)

	ap.add_argument(
		"--dry-run",
		dest="dryrun",
		help="Just show which participant datasets are candidate, and which are not",
		action="store_true",
	)

	ap.add_argument(
		"--skip-participant-update",
		dest="skip_participant_update",
		help="Should this program skip the update of the processed participant dataset?",
		action="store_true",
	)

	args, uploader, _flavor, oeb_api_creds = parse_args_and_init_app(
		ap, clazz=OEBUploader
	)

	logger = logging.getLogger("b2share-participant-publish")

	if oeb_api_creds is None:
		logger.fatal(
			"No credentials file with a 'publishServer' block was provided. Exiting"
		)
		sys.exit(1)

	# Time to instantiate the publisher
	publisher_block = oeb_api_creds.get("publishServer")
	if publisher_block is None:
		logger.fatal(
			"No 'publishServer' block was provided in the credentials file. Exiting"
		)
		sys.exit(1)

	publisher_name = publisher_block.get("type")
	if publisher_name is None:
		logger.fatal(
			"No publisher type was defined in the 'publishServer' block. Exiting"
		)
		sys.exit(1)

	publisher_clazz = PUBLISHERS.get(publisher_name)
	if publisher_clazz is None:
		logger.fatal(
			f"Unknown publisher type {publisher_name} declared in 'publishServer' block. Exiting"
		)
		sys.exit(1)

	publish_api_token = publisher_block.get("token", args.publish_api_token)
	if publish_api_token is None:
		logger.fatal(
			f"No API token was provided for publisher type {publisher_name}, either using 'publishServer' config block or -ptk parameter. Exiting."
		)
		sys.exit(2)

	sandbox = publisher_block.get("sandbox", args.sandbox)
	if sandbox is None:
		# By default, go to sandbox service
		sandbox = True

	schemas_manager = uploader.schemas_manager
	# Fetching benchmarking data model, in case
	# it is not fetched yet
	data_model_dir: "Union[str, Tuple[str, Sequence[SchemaHashEntry]]]"
	fetch_schemas = (args.bdmDir is None) or not os.path.isdir(args.bdmDir)
	if not fetch_schemas:
		logger.info(f"Checking directory {args.bdmDir} with contents")
		for entry in os.scandir(args.bdmDir):
			if (
				entry.is_file()
				and not entry.name.startswith(".")
				and (entry.name.endswith(".json") or entry.name.endswith(".yaml"))
			):
				break
		else:
			fetch_schemas = True
	if fetch_schemas:
		logger.info("Schemas being fetched")
		data_model_dir = schemas_manager.checkoutSchemas(
			checkoutDir=args.bdmDir,
			tag=args.bdmTag,
			fetchFromREST=args.trustREST,
			override_cache=args.override_cache,
		)
	else:
		data_model_dir = args.bdmDir

	retval = main_method(
		benchmarking_event_id=args.benchmarking_event_id,
		dryrun=args.dryrun,
		uri_prefix=args.dataset_uri_prefix,
		publisher_name=publisher_name,
		publisher_clazz=publisher_clazz,
		publish_api_token=publish_api_token,
		sandbox=sandbox,
		uploader=uploader,
		override_cache=args.override_cache,
		skip_participant_update=args.skip_participant_update,
		logger=logger,
		data_model_dir=data_model_dir,
	)

	logger.warning(
		f"Remember to check uploaded entries with next command\n\noeb-sandbox.py -cr {args.oeb_api_creds} --base_url {args.baseUrl} status\n"
	)
	logger.warning(
		f"And next command to promote from OpenEBench sandbox to stage\n\noeb-sandbox.py -cr {args.oeb_api_creds} --base_url {args.baseUrl} stage\n"
	)

	sys.exit(retval)


if __name__ == "__main__":
	main()
