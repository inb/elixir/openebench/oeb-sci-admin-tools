#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import json
import logging
import os
import sys

from typing import TYPE_CHECKING

if TYPE_CHECKING:
	from typing import (
		Optional,
	)

from . import (
	gen_base_param_parser,
	parse_args_and_init_app,
)

from ..common import (
	filterPayloads,
)

from ..uploader import OEBUploader


def main() -> None:
	ap = gen_base_param_parser(
		description="oeb-payload-uploader", input_filtering_flags=True
	)

	# ap.add_argument('--community-id', dest='community_id', help='The community id for new entries')

	ntgroup = ap.add_mutually_exclusive_group()
	cur = datetime.datetime.now()
	ntgroup.add_argument(
		"-n",
		"--newer-than",
		dest="newer_than",
		help=f"Only process files newer than the timestamp provided with this parameter. Both ISO-8601 (e.g. {cur.isoformat()}) and epoch seconds (e.g. {cur.timestamp()}) are supported",
	)
	ntgroup.add_argument(
		"--newer-than-file",
		dest="newer_than_file",
		help=f"Only process files newer than the file provided with this parameter",
	)

	ap.add_argument("files_or_dirs", nargs="*")

	# Let's do it!
	args, uploader, _flavor, _oeb_api_creds = parse_args_and_init_app(
		ap, clazz=OEBUploader
	)

	logger = logging.getLogger("oeb-payload-uploader")

	newer_than: "Optional[float]"
	if args.newer_than is not None:
		try:
			newer_than = float(args.newer_than)
		except ValueError as ve:
			newer_than = datetime.datetime.fromisoformat(args.newer_than).timestamp()
	elif args.newer_than_file is not None:
		newer_than = os.path.getmtime(args.newer_than_file)
	else:
		newer_than = None

	# Now, it is time to upload
	entries = list(
		filterPayloads(args.files_or_dirs, newer_than=newer_than, logger=logger)
	)
	# elif args.community_id is None:
	# 	logger.error('Community id is needed to try uploading new entries')
	# 	sys.exit(1)
	# else:
	logger.info("Here you go!")
	uploaded_payloads = uploader.payload_uploader(
		entries,
	)
	print("* Uploaded payloads:")
	for local_file, payload_url in uploaded_payloads:
		print(f"\t{local_file} => {payload_url}")


if __name__ == "__main__":
	main()
