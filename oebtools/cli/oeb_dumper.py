#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import os
import shutil
import sys
import urllib.parse
import urllib.request

from typing import TYPE_CHECKING

if TYPE_CHECKING:
	from typing import (
		Mapping,
		Union,
	)

from . import (
	gen_base_param_parser,
	parse_args_and_init_app,
)

from ..fetch import (
	DEFAULT_CONCEPTS,
	OEBFetcher,
)


def main() -> None:
	ap = gen_base_param_parser(description="oeb-dumper", add_flavor_and_url=True)
	ap.add_argument(
		"--output-dir",
		dest="outputDir",
		help="Output directory where all the fetched entries will be written",
		default=".",
	)
	ap.add_argument(
		"--concepts",
		dest="concepts",
		nargs="*",
		help="Concepts to fetch. If you use this flag with no concept name, it means 'fetch no concept'",
		default=DEFAULT_CONCEPTS,
	)
	ap.add_argument(
		"--input-ids-file",
		dest="input_ids_files",
		action="append",
		help="File(s) which contain the list of ids to fetch in order to translate",
	)
	ap.add_argument(
		"--input-id",
		dest="input_ids",
		action="append",
		help="OEB entry id to fetch in order to translate",
	)
	ap.add_argument(
		"--skip-spreading",
		dest="do_spread",
		help="If this flag is used, the bundle of entries for each concept is not spread over single files",
		action="store_false",
		default=True,
	)

	args, fetcher, flavor, _ = parse_args_and_init_app(ap, clazz=OEBFetcher)

	logger = logging.getLogger("oeb-dumper")

	concepts = args.concepts
	if len(concepts) > 0:
		fetcher.fetchIds(
			args.outputDir,
			flavor=flavor,
			concepts=concepts,
			override_cache=args.override_cache,
		)
		fetcher.fetchConceptEntries(
			args.outputDir,
			flavor=flavor,
			concepts=concepts,
			do_spread=args.do_spread,
			override_cache=args.override_cache,
		)

	input_ids = []

	# First to be processed
	if args.input_ids is not None:
		input_ids.extend(args.input_ids)

	if args.input_ids_files is not None:
		for input_ids_filename in args.input_ids_files:
			logger.info(f"Reading input ids file {input_ids_filename}")
			with open(input_ids_filename, mode="r", encoding="utf-8") as iiF:
				for line in iiF:
					input_id = line.strip()
					# Skip commented out lines
					if input_id.startswith("#"):
						continue

					input_ids.append(input_id)

	if (args.input_ids_files is not None) or (args.input_ids is not None):
		if len(input_ids) == 0:
			logger.error(f"No identifiers were read from the provided input ids files")
			sys.exit(1)

		fetcher.fetchEntriesFromIdsIntoDirectory(
			input_ids,
			args.outputDir,
			flavor=flavor,
			override_cache=args.override_cache,
		)


if __name__ == "__main__":
	main()
