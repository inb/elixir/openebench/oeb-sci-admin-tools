#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import json
import logging
import os
import sys

from typing import TYPE_CHECKING

if TYPE_CHECKING:
	from typing import (
		Optional,
		MutableSequence,
	)

from . import (
	gen_base_param_parser,
	parse_args_and_init_app,
)

from ..fetch import (
	DEFAULT_DEEP_REL_PATH,
	DEFAULT_DEEP_REPOURL,
)

from ..common import (
	FLAVOR_STAGED,
	processInputs,
)

from ..uploader import (
	OEBUploader,
	PayloadMode,
)


def main() -> None:
	ap = gen_base_param_parser(
		description="oeb-uploader",
		add_bdm_flags=True,
		add_report_validation_flags=True,
		input_filtering_flags=True,
	)

	ap.add_argument(
		"--deep-bdm-dir",
		dest="deepBdmDir",
		help="Directory with JSON Schemas to validate either inline or remote datasets, either existing or to be created",
	)
	ap.add_argument(
		"--deep-repo-tag",
		dest="deepRepoTag",
		help="Inline datasets schemas version and tag, to be checked out",
		default="master",
	)
	ap.add_argument(
		"--deep-repo-rel",
		dest="deepRepoRel",
		help="Inline datasets schemas relative directory",
		default=DEFAULT_DEEP_REL_PATH,
	)
	ap.add_argument(
		"--deep_repo_url",
		dest="deepRepoUrl",
		help="Repo URL for all the schemas",
		default=DEFAULT_DEEP_REPOURL,
	)

	ap.add_argument(
		"--payload-mode",
		dest="payload_mode",
		help="On Dataset entries, how to deal with inline and external payloads",
		choices=PayloadMode,
		type=PayloadMode,
		default=PayloadMode.AS_IS,
	)

	ap.add_argument(
		"--rm-ids-file",
		dest="ids_to_remove_file",
		help="File with the ids to be removed",
	)

	commit_group = ap.add_mutually_exclusive_group(required=True)
	commit_group.add_argument(
		"--dry-run",
		dest="dry_run",
		help="Validates the input, but it does no modifications",
		action="store_true",
		default=False,
	)
	commit_group.add_argument(
		"--community-id", dest="community_id", help="The community id for new entries"
	)

	ap.add_argument("files_or_dirs", nargs="*")

	# Let's do it!
	args, uploader, _flavor, _oeb_api_creds = parse_args_and_init_app(
		ap, clazz=OEBUploader
	)

	logger = logging.getLogger("oeb-uploader")

	schemas_manager = uploader.schemas_manager
	# Fetching benchmarking data model, in case
	# it is not fetched yet
	fetch_schemas = (args.bdmDir is None) or not os.path.isdir(args.bdmDir)
	if not fetch_schemas:
		logger.info(f"Checking directory {args.bdmDir} with contents")
		for entry in os.scandir(args.bdmDir):
			if (
				entry.is_file()
				and not entry.name.startswith(".")
				and (entry.name.endswith(".json") or entry.name.endswith(".yaml"))
			):
				break
		else:
			fetch_schemas = True
	if fetch_schemas:
		logger.info("Schemas being fetched")
		data_model_dir = schemas_manager.checkoutSchemas(
			checkoutDir=args.bdmDir,
			tag=args.bdmTag,
			fetchFromREST=args.trustREST,
			override_cache=args.override_cache,
		)
	else:
		data_model_dir = args.bdmDir

	# Fetching deep validation schemas, in case
	# they have not been fetched yet
	if (
		args.deepBdmDir is not None and os.path.isdir(args.deepBdmDir)
	) or args.deepRepoUrl is not None:
		deep_schemas_dir = schemas_manager.checkoutRepo(
			checkout_dir=args.deepBdmDir,
			git_repo=args.deepRepoUrl,
			tag=args.deepRepoTag,
			rel_path=args.deepRepoRel,
		)

	newer_than: "Optional[float]"
	if args.newer_than is not None:
		try:
			newer_than = float(args.newer_than)
		except ValueError as ve:
			newer_than = datetime.datetime.fromisoformat(args.newer_than).timestamp()
	elif args.newer_than_file is not None:
		newer_than = os.path.getmtime(args.newer_than_file)
	else:
		newer_than = None

	ignore_entries: "Optional[MutableSequence[str]]" = None
	if args.ids_to_ignore_files is not None:
		ignore_entries = []
		for ids_to_ignore_file in args.ids_to_ignore_files:
			with open(ids_to_ignore_file, mode="r", encoding="utf-8") as iH:
				for line in iH:
					hashidx = line.find("#")
					if hashidx != -1:
						line = line[0:hashidx]
					line = line.strip()
					if len(line) > 0:
						ignore_entries.append(line)

	# Now, it is time to validate and upload
	entries = list(
		processInputs(
			args.files_or_dirs,
			yieldFName=True,
			newer_than=newer_than,
			ignore_entries=ignore_entries,
			logger=logger,
		)
	)
	if args.dry_run:
		logger.info("Dry-run enabled, only validations")

		concept_ids_map = uploader.fetchIdsAndOrigIds(
			override_cache=args.override_cache
		)
		schema_prefix = schemas_manager.validate(
			entries,
			args.reportFilename,
			flavor=FLAVOR_STAGED,
			concept_ids_map=concept_ids_map,
			data_model_dir=data_model_dir,
			deep_schemas_dir=deep_schemas_dir,
			override_cache=args.override_cache,
		)
	elif args.community_id is None:
		logger.error("Community id is needed to try uploading new entries")
		sys.exit(1)
	else:
		logger.info("Dry-run disabled, here you go!")
		uploader.data_upload(
			args.community_id,
			entries,
			args.reportFilename,
			data_model_dir=data_model_dir,
			deep_schemas_dir=deep_schemas_dir,
			payload_mode=args.payload_mode,
			override_cache=args.override_cache,
		)

		ids_to_remove = []
		if args.ids_to_remove_file is not None:
			logger.info(f"Reading ids to remove from {args.ids_to_remove_file}")
			with open(args.ids_to_remove_file, mode="r", encoding="utf-8") as iH:
				for line in iH:
					if not line.startswith("#"):
						ids_to_remove.append(line)

		if len(ids_to_remove) > 0:
			uploader.remove_ids(ids_to_remove)


if __name__ == "__main__":
	main()
