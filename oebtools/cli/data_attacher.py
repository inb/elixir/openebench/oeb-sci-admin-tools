#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import atexit
import json
import logging
import sys

from ..data_scheme_handling import (
	file2data,
	attach_to_json,
)


def main() -> None:
	ap = argparse.ArgumentParser(
		description="data-attacher",
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
	)
	ap.add_argument(
		"--json-file",
		dest="json_file",
		help="File where the data url is going to be added",
	)
	ap.add_argument(
		"--jq-expr",
		dest="jq_expr",
		help="Expression, in jq format, where the data url is going to be assigned (default, .link)",
		default=".link",
	)
	ap.add_argument(
		"-o",
		"--output",
		dest="output_file",
		help="Output file where to store either the data URL or the updated JSON content (default stdout)",
		default="-",
	)
	ap.add_argument("input_file", help="The file to translate to data url")

	# Let's do it!
	args = ap.parse_args()

	data_url = file2data(args.input_file)

	if args.output_file == "-":
		oH = sys.stdout
	else:
		oH = open(args.output_file, mode="w", encoding="utf-8")
		atexit.register(oH.close)

	if args.json_file is None:
		print(data_url, file=oH)
	else:
		with open(args.json_file, mode="r", encoding="utf-8") as jH:
			json_input = json.load(jH)

		json_output = attach_to_json(json_input, args.jq_expr, data_url)

		json.dump(json_output, fp=oH)


if __name__ == "__main__":
	main()
