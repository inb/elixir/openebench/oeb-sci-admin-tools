#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import json
import logging
import os
import os.path
import sys
from typing import (
	cast,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	from typing import (
		Any,
		Mapping,
		MutableMapping,
		Optional,
		Sequence,
		Tuple,
		Union,
	)

from . import (
	gen_base_param_parser,
	parse_args_and_init_app,
)

from ..fetch import (
	OEBFetcher,
)

GRAPHQL_QUERY = """\
getBenchmarkingEvents(benchmarkingEventFilters: {id: $id}) {
  _id
  community_id
  challenges {
    _id
    acronym
    datasets(datasetFilters: {type: "aggregation"}) {
      _id
      type
      datalink {
        inline_data
      }
    }
    _metadata
  }
}
"""

this_command = "be_offline_aggregation_generator"


def queryAggregationEntries(
	benchmarking_event_id: "str",
	fetcher: "OEBFetcher",
	override_cache: "bool",
	logger: "logging.Logger",
) -> "Sequence[Tuple[str, Mapping[str, Sequence[Mapping[str, Any]]]]]":
	response = fetcher.query_graphql(
		GRAPHQL_QUERY, {"id": benchmarking_event_id}, override_cache=override_cache
	)

	if not isinstance(response, dict):
		logger.error(
			f"No answer from server was obtained using {benchmarking_event_id}"
		)
		sys.exit(1)

	d_response = response.get("data")
	if not isinstance(d_response, dict):
		logger.error(f"GraphQL query returned errors\n\n{response}")
		sys.exit(2)

	benchmarking_events_ql = d_response.get("getBenchmarkingEvents")
	retval = []
	if isinstance(benchmarking_events_ql, list):
		for b_ql in benchmarking_events_ql:
			community_id = b_ql["community_id"]
			for concept_name, community in fetcher.fetchEntriesFromIds(
				[community_id], override_cache=override_cache
			):
				community_acronym = community["acronym"]
				retval.append((community_acronym, b_ql))
				break

	return retval


METRICS_SEP = "+"
CHALLENGE_METRICS_SEP = "__"


def readMinAggregationDatasets(
	filename: "str",
	logger: "logging.Logger",
) -> "Mapping[str, Mapping[str, Mapping[str, Any]]]":
	with open(filename, mode="r", encoding="utf-8") as fH:
		min_seq_all = cast("Sequence[Mapping[str, Any]]", json.load(fH))

	min_map: "MutableMapping[str, MutableMapping[str, MutableMapping[str, Any]]]" = (
		dict()
	)
	# TODO: add some kind of validation here?
	if isinstance(min_seq_all, list):
		for min_entry in min_seq_all:
			if not isinstance(min_entry, dict):
				# Skipping "strange" entries
				continue
			if ("_id" not in min_entry) or min_entry.get("type") != "aggregation":
				# Skipping other dataset and entries
				continue
			# Let's process the minimal aggregation datasets
			# The key based on metric labels to use for the match
			# TODO: consider all the types of charts
			vis = (
				min_entry.get("datalink", {})
				.get("inline_data", {})
				.get("visualization", {})
			)
			chart_type = vis.get("type")
			if chart_type is None:
				logger.info(f"Skipping minimal dataset doe missing chart type")
				continue
			series_type = (
				min_entry.get("datalink", {}).get("inline_data", {}).get("series_type")
			)
			if series_type == "aggregation-data-series":
				metrics_labels_compound_key = METRICS_SEP.join(
					vis.get("available_metrics", [])
				)
			elif chart_type == "2D-plot":
				metrics_labels_compound_key = (
					vis.get("x_axis", "") + METRICS_SEP + vis.get("y_axis", "")
				)
			elif chart_type == "bar-plot":
				metrics_labels_compound_key = vis.get("metric")
			else:
				raise KeyError(
					f"Unknown type of chart {chart_type} or series {series_type}"
				)
			# Last, the reference
			for cha_label in min_entry.get("challenge_ids", []):
				min_map.setdefault(cha_label, {}).setdefault(chart_type, {})[
					metrics_labels_compound_key
				] = min_entry

	return min_map


def writeMinAggregationEntries(
	community_acronym: "str",
	be_ql: "Mapping[str, Sequence[Mapping[str, Any]]]",
	min_aggregation_datasets_by_cha_type_met: "Mapping[str, Mapping[str, Mapping[str, Any]]]",
	outputDir: "str",
	avoid_html_chars: "bool",
	logger: "logging.Logger",
) -> None:
	logger.info(f"Creating directory {outputDir}")
	os.makedirs(outputDir, exist_ok=True)

	file_counter = 0
	view_labels = []
	manifest = []
	for challenge_ql in be_ql.get("challenges", []):
		challenge_label: "Optional[str]" = challenge_ql.get("acronym")
		ch_metadata_ql = challenge_ql.get("_metadata")
		if ch_metadata_ql is not None:
			ch_metadata = json.loads(ch_metadata_ql)
			challenge_label_ql = ch_metadata.get("level_2:challenge_id")
			if challenge_label_ql is not None:
				challenge_label = challenge_label_ql

		assert challenge_label is not None
		by_cha_type_met = min_aggregation_datasets_by_cha_type_met.get(challenge_label)

		# Should be skip this challenge?
		if by_cha_type_met is None:
			continue

		for dataset_ql in challenge_ql.get("datasets", []):
			min_dataset_ql = dataset_ql.get("datalink", {}).get("inline_data")
			if min_dataset_ql is None:
				# Maybe some message should be emitted here
				continue
			# Fixing the issues from the graphql answer
			min_dataset_inline = json.loads(min_dataset_ql)

			# Classify the dataset, in order to get
			# a minimal one matching it
			vis = min_dataset_inline.get("visualization", {})
			chart_type = vis.get("type")
			if chart_type is None:
				logger.info(f"Chart type is empty for {min_dataset_ql['_id']}")
				continue
			series_type = min_dataset_inline.get("series_type")
			if series_type == "aggregation-data-series":
				metrics_labels_compound_key = METRICS_SEP.join(
					vis.get("available_metrics", [])
				)
			elif chart_type == "2D-plot":
				metrics_labels_compound_key = (
					vis.get("x_axis", "") + METRICS_SEP + vis.get("y_axis", "")
				)
			elif chart_type == "bar-plot":
				metrics_labels_compound_key = vis.get("metric")
			else:
				# We don't know what to do, skip it
				logger.info(
					f"Skipping {min_dataset_ql['_id']} due unknown type of chart {chart_type} or series {series_type}"
				)
				continue

			min_dataset_from_file = by_cha_type_met.get(chart_type, {}).get(
				metrics_labels_compound_key
			)

			if min_dataset_from_file is None:
				# This dataset does not have a min_dataset
				# counterpart, so skip it
				continue
			from_file_inline_data = min_dataset_from_file.get("datalink", {}).get(
				"inline_data"
			)

			challenge_participants = min_dataset_inline["challenge_participants"]

			# Now, let's merge both entries
			challenge_participants_dict = dict()
			merged_challenge_participants = []
			participant_labels = []
			new_participant_labels = []
			for from_file_cp in from_file_inline_data["challenge_participants"]:
				participant_labels.append(from_file_cp["participant_id"])
				new_participant_labels.append(from_file_cp["participant_id"])
				challenge_participants_dict[
					from_file_cp["participant_id"]
				] = from_file_cp
				merged_challenge_participants.append(from_file_cp)

			if isinstance(challenge_participants, list):
				# Minor fixes and merge
				for cha_par in challenge_participants:
					participant_id = cha_par.get("tool_id")
					if participant_id is not None:
						cha_par["participant_id"] = participant_id

					if cha_par["participant_id"] in challenge_participants_dict:
						# Already in, so old value is discarded
						continue

					merged_challenge_participants.append(cha_par)
					participant_labels.append(cha_par["participant_id"])

			# Last, but important step
			min_dataset_inline["challenge_participants"] = merged_challenge_participants

			min_dataset = {
				"_id": community_acronym + ":" + challenge_label + "_agg",
				"type": dataset_ql["type"],
				"challenge_ids": [challenge_label],
				"datalink": {"inline_data": min_dataset_inline},
			}

			# if challenge_label in view_labels:
			# 	view_label = f"{challenge_label}_{file_counter}"
			# else:
			# 	view_label = challenge_label
			view_label = (
				challenge_label
				+ CHALLENGE_METRICS_SEP
				+ chart_type
				+ CHALLENGE_METRICS_SEP
				+ metrics_labels_compound_key
			)

			if avoid_html_chars:
				view_label = view_label.replace("%", "_percent_")
				view_label = view_label.replace("+", "_plus_")
				view_label = view_label.replace("/", "_slash_")
				view_label = view_label.replace("?", "_question_")
				view_label = view_label.replace("#", "_hash_")

			output_dir_cha = os.path.join(outputDir, view_label)
			os.makedirs(output_dir_cha, exist_ok=True)
			output_file = os.path.join(output_dir_cha, view_label + ".json")
			logger.info(f"Writing {output_file}")
			with open(output_file, mode="w", encoding="utf-8") as wH:
				file_counter += 1
				json.dump(min_dataset, wH, indent=2, sort_keys=True)
			view_labels.append(view_label)
			manifest.append(
				{
					"id": view_label,
					"participants": participant_labels,
					"new_participants": new_participant_labels,
					"timestamp": datetime.datetime.now().astimezone().isoformat(),
				}
			)

	# And now, the manifest
	manifest_path = os.path.join(outputDir, "Manifest.json")
	with open(manifest_path, mode="w", encoding="utf-8") as hM:
		json.dump(manifest, hM, indent=4)


def main() -> None:
	ap = gen_base_param_parser(
		description=this_command,
		required_creds_params=False,
		base_url="https://openebench.bsc.es/api/scientific",
	)
	ap.add_argument(
		"--output-dir",
		dest="outputDir",
		help="Output directory where all the minimal aggregation entries will be written",
		required=True,
	)
	ap.add_argument(
		"--input-id",
		dest="benchmarking_event_id",
		help="OEB benchmarking event entry id to use as reference",
		required=True,
	)
	ap.add_argument(
		"--avoid-html-chars",
		dest="avoidHTMLchars",
		action="store_true",
	)
	ap.add_argument(
		"minimal_dataset_file",
		help="The minimal dataset to be integrated with the reference ones fetched from the event",
	)

	args, fetcher, _flavor, _oeb_api_creds = parse_args_and_init_app(
		ap, clazz=OEBFetcher
	)
	logger = logging.getLogger(this_command)

	min_aggregation_datasets_by_cha_type_met = readMinAggregationDatasets(
		args.minimal_dataset_file,
		logger=logger,
	)

	l_res_pairs = queryAggregationEntries(
		args.benchmarking_event_id,
		fetcher=fetcher,
		override_cache=args.override_cache,
		logger=logger,
	)

	if len(l_res_pairs) == 0:
		logger.error(
			f"An empty list of entries was obtained using {args.benchmarking_event_id}"
		)
		sys.exit(1)

	writeMinAggregationEntries(
		l_res_pairs[0][0],
		l_res_pairs[0][1],
		min_aggregation_datasets_by_cha_type_met,
		outputDir=args.outputDir,
		avoid_html_chars=args.avoidHTMLchars,
		logger=logger,
	)

	sys.exit(0)


if __name__ == "__main__":
	main()
