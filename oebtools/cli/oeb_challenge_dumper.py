#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import os
import os.path
import sys
from typing import TYPE_CHECKING

if TYPE_CHECKING:
	from typing import (
		Any,
		Mapping,
		Optional,
		Sequence,
		Set,
		Tuple,
		Union,
	)

from . import (
	gen_base_param_parser,
	parse_args_and_init_app,
)

from ..fetch import (
	OEBFetcher,
)

GRAPHQL_QUERY = """\
  getChallenges(challengeFilters: {id: $challenge_id}) {
    _id
    benchmarking_event_id
    challenge_contact_ids
    metrics_categories {
      metrics {
        metrics_id
        tool_id
      }
    }
    references
    datasets {
      _id
      dataset_contact_ids
      depends_on {
        metrics_id
        tool_id
        rel_dataset_ids {
          dataset_id
        }
      }
    }
    test_actions {
      _id
      tool_id
      involved_datasets {
        dataset_id
      }
      test_contact_ids
    }
  }
"""

this_command = "oeb-challenge-dumper"


def queryChallengeRelatedEntriesAndSave(
	challenge_id: "str",
	fetcher: "OEBFetcher",
	override_cache: "bool",
	outputDir: "str",
	logger: "logging.Logger",
) -> "Set[str]":
	response = fetcher.query_graphql(
		GRAPHQL_QUERY, {"challenge_id": challenge_id}, override_cache=override_cache
	)

	if not isinstance(response, dict):
		logger.error(f"No answer from server was obtained using {challenge_id}")
		sys.exit(1)

	d_response = response.get("data")
	if not isinstance(d_response, dict):
		logger.error(f"GraphQL query returned errors\n\n{response}")
		sys.exit(2)

	challenges_ql = d_response.get("getChallenges")
	fetched_ids: "Set[str]" = set()
	fetched_challenges: "Set[str]" = set()
	fetched_benchmarking_events: "Set[str]" = set()
	fetched_communities: "Set[str]" = set()
	fetched_dataset_paths: "Set[str]" = set()
	if isinstance(challenges_ql, list):
		logger.info(f"Creating directory {outputDir}")
		os.makedirs(outputDir, exist_ok=True)

		for c_ql in challenges_ql:
			benchmarking_event_id = c_ql["benchmarking_event_id"]
			# These are the entries to save pointed from this challenge
			entries_to_save = [
				challenge_id,
				benchmarking_event_id,
			]
			# The contacts
			entries_to_save.extend(
				map(lambda cci: ("Contact", cci), c_ql["challenge_contact_ids"])
			)
			# The references
			references_ql = c_ql["references"]
			if isinstance(references_ql, list):
				entries_to_save.extend(map(lambda rf: ("Reference", rf), references_ql))
			# The tools and metrics pointed out from metrics categories
			for mc_ql in c_ql["metrics_categories"]:
				for m_ql in mc_ql["metrics"]:
					if m_ql["metrics_id"] is not None:
						entries_to_save.append(m_ql["metrics_id"])
					if m_ql["tool_id"] is not None:
						entries_to_save.append(m_ql["tool_id"])

			# Gather all the datasets
			for d_ql in c_ql["datasets"]:
				entries_to_save.append(d_ql["_id"])

				# And add its contact ids
				entries_to_save.extend(
					map(lambda dci: ("Contact", dci), d_ql["dataset_contact_ids"])
				)

				do_ql = d_ql["depends_on"]
				if isinstance(do_ql, list):
					for doe_ql in do_ql:
						if doe_ql["metrics_id"] is not None:
							entries_to_save.append(doe_ql["metrics_id"])
						if doe_ql["tool_id"] is not None:
							entries_to_save.append(doe_ql["tool_id"])
						if isinstance(doe_ql["rel_dataset_ids"], list):
							for rdi_ql in doe_ql["rel_dataset_ids"]:
								if rdi_ql["dataset_id"] is not None:
									entries_to_save.append(rdi_ql["dataset_id"])

			for ta_ql in c_ql["test_actions"]:
				entries_to_save.append(ta_ql["_id"])
				entries_to_save.append(ta_ql["tool_id"])
				if isinstance(ta_ql["test_contact_ids"], list):
					entries_to_save.extend(
						map(lambda tci: ("Contact", tci), ta_ql["test_contact_ids"])
					)
				for id_ql in ta_ql["involved_datasets"]:
					if id_ql["dataset_id"] is not None:
						entries_to_save.append(id_ql["dataset_id"])

			# Now, time so save and process
			# And all the test actions
			logger.info(f"Fetching {len(entries_to_save)} entries")
			while len(entries_to_save) > 0:
				# First, is there something to fetch?
				new_entries_to_save = set(entries_to_save) - fetched_ids
				if len(new_entries_to_save) == 0:
					break
				# Then, fetch
				fetched_entries = fetcher.fetchEntriesFromIdsIntoDirectory(
					new_entries_to_save,
					outputDir,
					override_cache=override_cache,
				)
				# Then, gather
				entries_to_save = []
				for concept_name, entry_pointers in fetched_entries.items():
					for entry_id, entry_path in entry_pointers:
						# Fetch only once
						fetched_ids.add(entry_id)
						with open(entry_path, mode="r", encoding="utf-8") as eH:
							entry = json.load(eH)

						if concept_name == "Challenge":
							fetched_challenges.add(entry_id)

							entries_to_save.append(entry["benchmarking_event_id"])
							entries_to_save.extend(
								map(
									lambda cci: ("Contact", cci),
									entry.get("challenge_contact_ids", []),
								)
							)
							met_cats = entry.get("metrics_categories")
							if isinstance(met_cats, list):
								for met_cat in met_cats:
									for met in met_cat.get("metrics", []):
										if isinstance(met, dict):
											tool_id = met.get("tool_id")
											if tool_id is not None:
												entries_to_save.append(tool_id)
											metrics_id = met.get("metrics_id")
											if metrics_id is not None:
												entries_to_save.append(metrics_id)

						elif concept_name == "BenchmarkingEvent":
							fetched_benchmarking_events.add(entry_id)

							community_id = entry["community_id"]
							entries_to_save.append(community_id)
							entries_to_save.extend(
								map(
									lambda bci: ("Contact", bci),
									entry.get("bench_contact_ids", []),
								)
							)
						elif concept_name == "Community":
							fetched_communities.add(entry_id)
							entries_to_save.extend(
								map(
									lambda cci: ("Contact", cci),
									entry.get("community_contact_ids", []),
								)
							)
							for re_tool in entry.get("reference_tools", []):
								tool_id = re_tool.get("tool_id")
								if tool_id is not None:
									entries_to_save.append(tool_id)
						elif concept_name == "Dataset":
							if entry["type"] == "other" and "Manifest" in entry.get(
								"description", ""
							):
								# Remove this entry and skip its processing
								os.unlink(entry_path)
								continue
							# Needed to remove references to other not materialized elements
							fetched_dataset_paths.add(entry_path)

							entries_to_save.extend(
								map(
									lambda dci: ("Contact", dci),
									entry.get("dataset_contact_ids", []),
								)
							)

							doe = entry.get("depends_on", {})
							metrics_id = doe.get("metrics_id")
							if metrics_id is not None:
								entries_to_save.append(metrics_id)
							tool_id = doe.get("tool_id")
							if tool_id is not None:
								entries_to_save.append(tool_id)
							for rdi in doe.get("rel_dataset_ids", []):
								r_dataset_id = rdi.get("dataset_id")
								if r_dataset_id is not None:
									entries_to_save.append(r_dataset_id)
						elif concept_name == "Tool":
							entries_to_save.extend(
								map(
									lambda tci: ("Contact", tci),
									entry.get("tool_contact_ids", []),
								)
							)
						elif concept_name == "Metrics":
							entries_to_save.extend(
								map(
									lambda mci: ("Contact", mci),
									entry.get("metrics_contact_ids", []),
								)
							)

						# Almost all the concepts have references
						entries_to_save.extend(
							map(
								lambda rf: ("Reference", rf),
								entry.get("references", []),
							)
						)

			# And now, the "post-traslational modifications"
			for dataset_path in fetched_dataset_paths:
				with open(dataset_path, mode="r", encoding="utf-8") as eH:
					dataset = json.load(eH)
					d_modified = False

				unique_d_ch = set(dataset["challenge_ids"])
				new_unique_d_ch = fetched_challenges & unique_d_ch
				if len(new_unique_d_ch) == 0:
					logger.warning(
						f"Dataset {dataset['_id']} (at {dataset_path}) did not keep any challenge"
					)
				if new_unique_d_ch != unique_d_ch:
					dataset["challenge_ids"] = list(new_unique_d_ch)
					d_modified = True

				unique_d_co = set(dataset["community_ids"])
				new_unique_d_co = fetched_communities & unique_d_co
				if len(new_unique_d_co) == 0:
					logger.warning(
						f"Dataset {dataset['_id']} (at {dataset_path}) did not keep any community"
					)
				if new_unique_d_co != unique_d_co:
					dataset["community_ids"] = list(new_unique_d_co)
					d_modified = True

				if d_modified:
					if len(new_unique_d_co) == 0 or len(new_unique_d_ch) == 0:
						logger.info(
							f"=> Removed {dataset['type']} dataset {dataset_path} as it is unrelated"
						)
						os.unlink(dataset_path)
					else:
						logger.info(
							f"=> Modified {dataset['type']} dataset {dataset_path} to remove foreign challenges or communities"
						)
						with open(dataset_path, mode="w", encoding="utf-8") as wH:
							json.dump(dataset, wH, indent=2, sort_keys=True)

	return fetched_ids


def main() -> None:
	ap = gen_base_param_parser(
		description=this_command,
		required_creds_params=False,
		base_url="https://openebench.bsc.es/api/scientific",
	)
	ap.add_argument(
		"--output-dir",
		dest="outputDir",
		help="Output directory where all the minimal aggregation entries will be written",
		required=True,
	)
	ap.add_argument(
		"--input-id",
		dest="challenge_id",
		help="OEB challenge entry id to use as reference",
		required=True,
	)

	args, fetcher, _flavor, _oeb_api_creds = parse_args_and_init_app(
		ap, clazz=OEBFetcher
	)
	logger = logging.getLogger(this_command)

	fetched_ids = queryChallengeRelatedEntriesAndSave(
		args.challenge_id,
		fetcher=fetcher,
		override_cache=args.override_cache,
		outputDir=args.outputDir,
		logger=logger,
	)

	if len(fetched_ids) == 0:
		logger.error(f"An empty list of entries was obtained using {args.challenge_id}")
		sys.exit(1)

	sys.exit(0)


if __name__ == "__main__":
	main()
