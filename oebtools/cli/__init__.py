#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

import argparse
import datetime
import json
import logging

from .. import __version__ as scitools_version

from ..common import (
	DEFAULT_BASEURL,
	DEFAULT_BDM_TAG,
	DEFAULT_FLAVOR,
)

from typing import (
	cast,
	overload,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	from typing import (
		Any,
		Iterable,
		Iterator,
		Mapping,
		MutableSequence,
		Optional,
		Tuple,
		Type,
		TypeVar,
		Union,
	)

	from ..auth import (
		OEBCredentials,
	)

	from ..fetch import (
		OEBAbstractFetcher,
	)

	AF = TypeVar("AF", bound=OEBAbstractFetcher)


LOGGING_FORMAT = "%(asctime)-15s - [%(levelname)s] %(message)s"
DEBUG_LOGGING_FORMAT = (
	"%(asctime)-15s - [%(name)s %(funcName)s %(lineno)d][%(levelname)s] %(message)s"
)


def gen_base_param_parser(
	description: "str",
	required_creds_params: "bool" = True,
	add_flavor_and_url: "bool" = False,
	add_bdm_flags: "bool" = False,
	add_cache: "bool" = True,
	add_report_validation_flags: "bool" = False,
	add_report_flags: "bool" = False,
	input_filtering_flags: "bool" = False,
	base_url: "str" = DEFAULT_BASEURL,
) -> "argparse.ArgumentParser":
	ap = argparse.ArgumentParser(
		description=description,
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
	)
	ap.add_argument(
		"--log-file",
		dest="logFilename",
		help="Store messages in a file instead of using standard error and standard output",
	)
	ap.add_argument(
		"--log-format",
		dest="logFormat",
		help=f"Format of log messages (for instance {LOGGING_FORMAT.replace('%', '%%')})",
	)
	ap.add_argument(
		"-q",
		"--quiet",
		dest="logLevel",
		action="store_const",
		const=logging.WARNING,
		help="Only show engine warnings and errors",
	)
	ap.add_argument(
		"-v",
		"--verbose",
		dest="logLevel",
		action="store_const",
		const=logging.INFO,
		help="Show verbose (informational) messages",
	)
	ap.add_argument(
		"-d",
		"--debug",
		dest="logLevel",
		action="store_const",
		const=logging.DEBUG,
		help="Show debug messages (use with care, as it could potentially disclose sensitive contents)",
	)
	ap.add_argument(
		"--base_url",
		dest="baseUrl",
		help="Base URL for all the OpenEBench Scientific APIs",
		default=base_url,
	)
	if add_flavor_and_url:
		ap.add_argument(
			"--flavor",
			dest="flavor",
			help="Dump staged or sandbox entries?",
			default=DEFAULT_FLAVOR,
		)
		ap.add_argument(
			"--url",
			help="If this parameter is set, this url will be used for the queries, and both --base_url and --flavor will be ignored",
			dest="url",
		)

	if add_cache:
		ap.add_argument(
			"--cache",
			help="If this parameter is set to either an integer or a float, graphql and selected requests will be cached at most the seconds specified in the parameter, in order to speed up some code paths",
			dest="cache_entry_expire",
			const=-1,
			action="store",
			type=float,
			nargs="?",
		)
		ap.add_argument(
			"--invalidate-cache",
			dest="override_cache",
			help="When cache is enabled, this flag teaches to invalidate previously cached OEB contents",
			action="store_true",
			default=False,
		)

	ap.add_argument(
		"-cr",
		"--oeb_api_creds",
		dest="oeb_api_creds",
		help="Credentials and endpoints used to obtain a token for OpenEBench APIs",
		required=required_creds_params,
	)

	ap.add_argument(
		"-tk",
		"--oeb_submit_api_token",
		dest="oeb_submit_api_token",
		help="Token for OpenEBench APIs. If it is provided, it is used instead of requesting a new one.",
	)

	if add_bdm_flags:
		ap.add_argument(
			"--bdm-dir",
			dest="bdmDir",
			help="Benchmarking data model directory, either existing or to be created",
		)
		sch_source = ap.add_mutually_exclusive_group()
		sch_source.add_argument(
			"--bdm-tag",
			dest="bdmTag",
			help="Benchmarking data model version and tag, to be checked out",
			default=DEFAULT_BDM_TAG,
		)
		sch_source.add_argument(
			"--trust-rest-bdm",
			dest="trustREST",
			help="Trust on the copy of Benchmarking data model referred by server, fetching from it instead of GitHub or w3id. A copy of these schemas can be saved if --bdm-dir parameter is used",
			action="store_true",
			default=False,
		)

	if add_report_validation_flags:
		ap.add_argument(
			"--report",
			dest="reportFilename",
			help="Store validation report (in JSON format) in a file",
			default="error-report.json",
		)

	if input_filtering_flags:
		ntgroup = ap.add_mutually_exclusive_group()
		cur = datetime.datetime.now()
		ntgroup.add_argument(
			"-n",
			"--newer-than",
			dest="newer_than",
			help=f"Only process files newer than the timestamp provided with this parameter. Both ISO-8601 (e.g. {cur.isoformat()}) and epoch seconds (e.g. {cur.timestamp()}) are supported",
		)
		ntgroup.add_argument(
			"--newer-than-file",
			dest="newer_than_file",
			help=f"Only process files newer than the file provided with this parameter",
		)

		ap.add_argument(
			"--ignore-ids-file",
			dest="ids_to_ignore_files",
			action="append",
			help="File with the ids to be ignored when the inputs are parsed",
		)

	elif add_report_flags:
		ap.add_argument(
			"--report",
			dest="reportFilename",
			help="Store operation report (usually in JSON format) in a file (default stdout)",
			default="-",
		)

	ap.add_argument(
		"-V",
		"--version",
		action="version",
		version="%(prog)s version " + scitools_version,
	)
	return ap


def parse_args_and_init_app(
	ap: "argparse.ArgumentParser",
	clazz: "Type[AF]",
) -> "Tuple[argparse.Namespace, AF, Optional[str], Optional[OEBCredentials]]":
	args = ap.parse_args()

	logLevel = logging.DEBUG
	if args.logLevel is not None:
		logLevel = args.logLevel

	if args.logFormat:
		logFormat = args.logFormat
	elif logLevel < logging.INFO:
		logFormat = LOGGING_FORMAT
	else:
		logFormat = DEBUG_LOGGING_FORMAT

	loggingConfig = {
		"level": logLevel,
		"format": logFormat,
	}

	if args.logFilename is not None:
		loggingConfig["filename"] = args.logFilename
	# 	loggingConfig['encoding'] = 'utf-8'

	# tan goes into a stack overflow unless we tell not process it
	logging.basicConfig(**loggingConfig)  # fmt: skip

	oeb_credentials: "Optional[Union[str, OEBCredentials]]" = None

	# Credentials
	oeb_api_creds: "Optional[OEBCredentials]" = None
	if args.oeb_submit_api_token is not None:
		oeb_credentials = args.oeb_submit_api_token
	elif args.oeb_api_creds is not None:
		with open(args.oeb_api_creds, mode="r", encoding="utf-8") as cF:
			oeb_api_creds = json.load(cF)

		oeb_credentials = oeb_api_creds

	if hasattr(args, "url") and getattr(args, "url") is not None:
		url = args.url
		flavor = None
	else:
		url = args.baseUrl
		if hasattr(args, "flavor"):
			flavor = args.flavor
		else:
			flavor = None

	if hasattr(args, "cache_entry_expire"):
		cache_entry_expire = args.cache_entry_expire
	else:
		cache_entry_expire = None

	instance = clazz(
		url, oeb_credentials=oeb_credentials, cache_entry_expire=cache_entry_expire
	)

	return args, instance, flavor, oeb_api_creds
