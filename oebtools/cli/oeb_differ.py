#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import os
import shutil
import sys
import urllib.parse
import urllib.request

from typing import TYPE_CHECKING

if TYPE_CHECKING:
	from typing import (
		Mapping,
		Optional,
		Union,
	)
	from oebtools.auth import (
		OEBCredentials,
	)

import ijson  # type: ignore[import]

from . import (
	gen_base_param_parser,
	parse_args_and_init_app,
)

from ..fetch import (
	DEFAULT_CONCEPTS,
	OEBFetcher,
)

DEFAULT_DEST_BASEURL = "https://openebench.bsc.es/api/scientific/"


def main() -> None:
	ap = gen_base_param_parser(
		description="oeb-differ", required_creds_params=False, add_flavor_and_url=True
	)
	ap.add_argument(
		"--dest-url",
		dest="destBaseUrl",
		help="Base URL for all the OpenEBench Scientific APIs used as future destination of entries",
		default=DEFAULT_DEST_BASEURL,
	)
	ap.add_argument(
		"--concepts",
		dest="concepts",
		nargs="+",
		help="Concepts to fetch",
		default=DEFAULT_CONCEPTS,
	)
	ap.add_argument(
		"--output-dir",
		dest="outputDir",
		help="Output directory where all the fetched entries and comparisons will be written",
		default=".",
	)

	args, baseFetcher, flavor, _oeb_api_creds = parse_args_and_init_app(
		ap, clazz=OEBFetcher
	)

	logger = logging.getLogger("oeb-differ")

	baseUrl = args.baseUrl
	if not baseUrl.endswith("/"):
		baseUrl += "/"

	destBaseUrl = args.destBaseUrl
	if not destBaseUrl.endswith("/"):
		destBaseUrl += "/"

	# Being sure the output directory is in place
	outputDir = args.outputDir
	os.makedirs(outputDir, exist_ok=True)

	# Now, let's dump
	concepts = args.concepts
	processed_credentials: "Optional[Union[OEBCredentials, str]]" = (
		baseFetcher.oeb_credentials
		if baseFetcher.oeb_credentials is not None
		else baseFetcher.oeb_token
	)
	if processed_credentials is not None:
		destFetcher = OEBFetcher(destBaseUrl, oeb_credentials=processed_credentials)
		baseConceptEntsMap = baseFetcher.fetchConceptEntries(
			outputDir,
			flavor=flavor,
			relOutputFile="orig_entries-json.array",
			concepts=concepts,
			do_spread=False,
			override_cache=args.override_cache,
		)
		destConceptEntsMap = destFetcher.fetchConceptEntries(
			outputDir,
			flavor=flavor,
			relOutputFile="dest_entries-json.array",
			concepts=concepts,
			do_spread=False,
			override_cache=args.override_cache,
		)

		# TODO: what to do when the names of the concepts mismatch
		sym_diff = set(baseConceptEntsMap.keys()) ^ set(destConceptEntsMap.keys())
		assert (
			len(sym_diff) == 0
		), f"FIXME: What to do when concept sets do not match ({sym_diff})?"

		for conceptName, entspath in baseConceptEntsMap.items():
			destentspath = destConceptEntsMap[conceptName]

			logger.info(f"Concept {conceptName}")
			cDir = os.path.join(outputDir, conceptName)

			# Get the list of identifiers to be transferred
			origIdPairs = set()
			origIdPath = os.path.join(os.path.dirname(entspath), "orig-ids.txt")
			origOrigIdPath = os.path.join(
				os.path.dirname(entspath), "orig-orig_ids.txt"
			)
			with open(entspath, mode="r", encoding="utf-8") as iH, open(
				origIdPath, mode="w", encoding="utf-8"
			) as oiH, open(origOrigIdPath, mode="w", encoding="utf-8") as ooiH:
				for entry in ijson.items(iH, "item"):
					origIdPairs.add((entry.get("_id"), entry.get("orig_id")))
					if entry.get("_id"):
						print(entry.get("_id"), file=oiH)
					if entry.get("orig_id"):
						print(entry.get("orig_id"), file=ooiH)

			# Get the list of identifiers already in destination
			destIdPairs = set()
			destIdPath = os.path.join(os.path.dirname(entspath), "dest-ids.txt")
			destOrigIdPath = os.path.join(
				os.path.dirname(entspath), "dest-orig_ids.txt"
			)
			with open(destentspath, mode="r", encoding="utf-8") as diH, open(
				destIdPath, mode="w", encoding="utf-8"
			) as doiH, open(destOrigIdPath, mode="w", encoding="utf-8") as dooiH:
				for entry in ijson.items(diH, "item"):
					destIdPairs.add((entry.get("_id"), entry.get("orig_id")))
					if entry.get("_id"):
						print(entry.get("_id"), file=doiH)
					if entry.get("orig_id"):
						print(entry.get("orig_id"), file=dooiH)

			commonIdPairs = origIdPairs & destIdPairs
			if len(commonIdPairs) > 0:
				logger.error(f"\tThere are {len(commonIdPairs)} collisions")
				collpath = os.path.join(cDir, "ent_collisions.txt")
				with open(collpath, mode="w", encoding="utf-8") as cH:
					for collId, collOrigId in commonIdPairs:
						print(f"{collId} {collOrigId}", file=cH)

			newIdPairs = origIdPairs - destIdPairs
			if len(newIdPairs) > 0:
				logger.error(f"\tThere are {len(newIdPairs)} new entries")
				newpath = os.path.join(cDir, "ent_new-entries.txt")
				with open(newpath, mode="w", encoding="utf-8") as nH:
					for newId, newOrigId in newIdPairs:
						print(f"{newId} {newOrigId}", file=nH)

			oldIdPairs = destIdPairs - origIdPairs
			if len(oldIdPairs) > 0:
				logger.error(f"\tThere are {len(oldIdPairs)} old entries")
				oldpath = os.path.join(cDir, "ent_old-entries.txt")
				with open(oldpath, mode="w", encoding="utf-8") as oH:
					for oldId, oldOrigId in oldIdPairs:
						print(f"{oldId} {oldOrigId}", file=oH)
	else:
		destFetcher = OEBFetcher(destBaseUrl)
		baseConceptIdsMap = baseFetcher.fetchIds(
			outputDir,
			concepts=concepts,
			relOutputFile="orig-ids.txt",
			override_cache=args.override_cache,
		)
		destConceptIdsMap = destFetcher.fetchIds(
			outputDir,
			concepts=concepts,
			relOutputFile="dest-ids.txt",
			override_cache=args.override_cache,
		)

		# TODO: what to do when the names of the concepts mismatch
		sym_diff = set(baseConceptIdsMap.keys()) ^ set(destConceptIdsMap.keys())
		assert (
			len(sym_diff) == 0
		), f"FIXME: What to do when concept sets do not match ({sym_diff})?"

		for conceptName, idspath in baseConceptIdsMap.items():
			destidspath = destConceptIdsMap[conceptName]

			logger.info(f"Concept {conceptName}")
			cDir = os.path.join(outputDir, conceptName)

			# Get the list of identifiers to be transferred
			origIds = set()
			with open(idspath, mode="r", encoding="utf-8") as iH:
				for _id in iH:
					origIds.add(_id.rstrip())

			# Get the list of identifiers already in destination
			destIds = set()
			with open(destidspath, mode="r", encoding="utf-8") as diH:
				for _id in diH:
					destIds.add(_id.rstrip())

			commonIds = origIds & destIds

			if len(commonIds) > 0:
				logger.error(f"\tThere are {len(commonIds)} collisions")
				collpath = os.path.join(cDir, "collisions.txt")
				with open(collpath, mode="w", encoding="utf-8") as cH:
					for collId in commonIds:
						print(collId, file=cH)

			newIds = origIds - destIds
			if len(newIds) > 0:
				logger.error(f"\tThere are {len(newIds)} new entries")
				newpath = os.path.join(cDir, "new-entries.txt")
				with open(newpath, mode="w", encoding="utf-8") as nH:
					for newId in newIds:
						print(newId, file=nH)

			oldIds = destIds - origIds
			if len(oldIds) > 0:
				logger.error(f"\tThere are {len(oldIds)} old entries")
				oldpath = os.path.join(cDir, "old-entries.txt")
				with open(oldpath, mode="w", encoding="utf-8") as oH:
					for oldId in oldIds:
						print(oldId, file=oH)


if __name__ == "__main__":
	main()
