#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import atexit
import json
import logging
import sys

from ..data_scheme_handling import (
	detach_from_json,
)


def main() -> None:
	ap = argparse.ArgumentParser(
		description="data-detacher",
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
	)
	ap.add_argument(
		"--json-file",
		dest="json_file",
		required=True,
		help="File where the data url is living",
	)
	ap.add_argument(
		"--jq-expr",
		dest="jq_expr",
		help="Expression, in jq format, needed to get the data url (default, .link)",
		default=".link",
	)
	ap.add_argument(
		"-o",
		"--output",
		dest="output_file",
		help="Output file where to store the decoded data URL (default stdout)",
		default="-",
	)

	# Let's do it!
	args = ap.parse_args()

	with open(args.json_file, mode="r", encoding="utf-8") as jH:
		json_input = json.load(jH)

	if args.output_file == "-":
		oH = sys.stdout.buffer
	else:
		oH = open(args.output_file, mode="wb")
		atexit.register(oH.close)

	num_bytes = detach_from_json(json_input, args.jq_expr, oH)
	print(f"{num_bytes} bytes written to {args.output_file}")


if __name__ == "__main__":
	main()
