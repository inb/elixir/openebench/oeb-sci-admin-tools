#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import atexit
import datetime
import json
import logging
import os
import shutil
import sys
import tempfile

import jq  # type: ignore[import]
import yaml

from typing import (
	cast,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	import pathlib

	from types import ModuleType

	from typing import (
		Any,
		Iterable,
		Mapping,
		MutableMapping,
		MutableSequence,
		Optional,
		Sequence,
		Tuple,
		Type,
		Union,
	)

	from typing_extensions import (
		TypedDict,
		Required,
		NotRequired,
	)

	MigrationRecipe = TypedDict(
		"MigrationRecipe",
		{
			"schema": "Required[str]",
			"script": "Required[str]",
			"cscript": "NotRequired[jq._Program]",
			"side_objects_paths": "NotRequired[Mapping[str, str]]",
			"side_objects": "NotRequired[Mapping[str, Mapping[str, Mapping[str, Any]]]]",
		},
	)

from . import (
	gen_base_param_parser,
	parse_args_and_init_app,
)

from ..common import (
	FLAVOR_STAGED,
	processInputs,
)

from ..fetch import (
	DEFAULT_CONCEPTS,
	OEBFetcher,
)

from ..schemas import (
	OEBSchemasManager,
)


def migrateInputs(
	inputs: "Iterable[Mapping[str, Any]]",
	recipesH: "Mapping[str, Sequence[MigrationRecipe]]",
	outputDir: "str",
	logger: "Union[logging.Logger, ModuleType]" = logging,
	max_files_per_subdir: "int" = 10000,
) -> "Tuple[Sequence[str], Mapping[str, Sequence[str]]]":
	# Preparing the destination directory
	os.makedirs(outputDir, exist_ok=True)

	iOut = 0
	prevSuboPath = None
	failed_ids_map: "MutableMapping[str, MutableSequence[str]]" = dict()
	written_files: "MutableSequence[str]" = []
	for iJSON in inputs:
		schStr = iJSON.get("_schema")
		assert isinstance(schStr, str)
		recipes = recipesH.get(schStr)
		# Now we know it can work
		if recipes is not None:
			conceptName = schStr[schStr.rfind("/") + 1 :]
			theid = iJSON.get("_id")
			for recipe in recipes:
				# Applying the recipe
				res = recipe["cscript"].input_value(iJSON)
				try:
					oJSON = res.first()
					if oJSON is not None:
						thenewid = oJSON.get("_id")
						suboPath = "{0:06d}".format(iOut // max_files_per_subdir)
						# Assuring the subpath exists
						if prevSuboPath != suboPath:
							prevSuboPath = suboPath
						os.makedirs(
							os.path.join(outputDir, conceptName, suboPath),
							exist_ok=True,
						)

						oPath = os.path.join(
							outputDir,
							conceptName,
							suboPath,
							"{0}.json".format(thenewid)
							if thenewid is not None and "/" not in thenewid
							else "{0}_{1:06d}.json".format(conceptName, iOut),
						)
						with open(oPath, mode="w", encoding="utf-8") as oH:
							json.dump(oJSON, oH, indent=4)
						written_files.append(oPath)
						iOut += 1
					elif theid is not None:
						failed_ids_map.setdefault(conceptName, []).append(theid)
				except (ValueError, StopIteration) as ve:
					if theid is not None:
						failed_ids_map.setdefault(conceptName, []).append(theid)
					logger.exception("Mira esto")

	if len(failed_ids_map) > 0:
		logger.error(
			f"jq failed transforming entries from next concepts: { ' '.join(failed_ids_map.keys()) }"
		)
		for conceptName, failed_ids in failed_ids_map.items():
			logger.error(f"\tConcept {conceptName}: { ' '.join(failed_ids) }")

	return written_files, failed_ids_map


def main() -> None:
	ap = gen_base_param_parser(
		description="oeb-migrator",
		add_flavor_and_url=True,
		add_bdm_flags=True,
		add_cache=True,
		add_report_validation_flags=True,
		input_filtering_flags=True,
	)
	ap.add_argument(
		"--input",
		dest="inputs",
		action="append",
		help="File(s) or directory whose files are either JSON inputs or arrays of JSON inputs to be migrated",
	)
	ap.add_argument(
		"--concepts",
		dest="concepts",
		nargs="*",
		help=f"Concepts to fetch from. It should be one or more of this list: {', '.join(DEFAULT_CONCEPTS)}",
	)
	ap.add_argument(
		"--input-ids-file",
		dest="input_ids_files",
		action="append",
		help="File(s) which contain the list of ids to fetch in order to translate",
	)
	ap.add_argument(
		"--fetched-concept-dir",
		dest="fetched_concept_dir",
		help="If some data is fetched from the API, store a copy in this directory",
	)
	ap.add_argument(
		"--recipes",
		dest="recipes",
		required=True,
		help="File in YAML format with the jq migration recipes to apply",
	)
	ap.add_argument(
		"--output-dir",
		dest="outputDir",
		help="Directory where to save the files",
		default=".",
	)
	ap.add_argument(
		"--validate",
		dest="doValidate",
		help="Validate generated contents after migration (useful for development scenarios)",
		action="store_true",
		default=False,
	)

	args, fetcher, flavor, _ = parse_args_and_init_app(ap, clazz=OEBFetcher)
	logger = logging.getLogger("oeb-migrator")

	# Time to parse the recipes
	abs_recipes_path = os.path.abspath(args.recipes)
	with open(abs_recipes_path, mode="r", encoding="utf-8") as rH:
		recipes = cast("Sequence[MigrationRecipe]", yaml.safe_load(rH))

	# Preparing the playground once
	recipesH: "MutableMapping[str, MutableSequence[MigrationRecipe]]" = dict()
	for recipe in recipes:
		# Gathering entries to be used in the migration process
		side_objects: "MutableMapping[str, Mapping[str, Mapping[str, Any]]]" = dict()
		side_objects_paths = recipe.get("side_objects_paths")

		if side_objects_paths is not None:
			# Normalize the path
			for varname, side_objects_path in side_objects_paths.items():
				this_side_objects = dict()
				side_objects_path = os.path.normpath(
					os.path.join(os.path.dirname(abs_recipes_path), side_objects_path)
				)
				for side_object in cast(
					"Iterable[Mapping[str, Any]]",
					processInputs([side_objects_path], yieldFName=False, logger=logger),
				):
					this_side_objects[side_object["_id"]] = side_object
				side_objects[varname] = this_side_objects
		# recipe["side_objects"] = side_objects

		# Compiling the jq script
		recipe["cscript"] = jq.compile(recipe["script"], args=side_objects)

		# Last, save
		recipesH.setdefault(recipe["schema"], []).append(recipe)

	# Time to obtain the remote inputs
	inputs: "MutableSequence[str]" = (
		list(args.inputs) if args.inputs is not None else []
	)
	if args.concepts is not None or args.input_ids_files is not None:
		fetched_concept_dir = args.fetched_concept_dir
		# Creation of the temporary directory where we are going to
		# check out the benchmarking data model
		if fetched_concept_dir is None:
			fetched_concept_dir = tempfile.mkdtemp(prefix="oeb", suffix="schema")
			atexit.register(shutil.rmtree, fetched_concept_dir, ignore_errors=True)

		# Validating and fetching
		if len(args.concepts) > 0:
			concepts_set = set(args.concepts)
			unknown_concepts = concepts_set - set(DEFAULT_CONCEPTS)
			if len(unknown_concepts) > 0:
				logger.error(
					f"Unrecognized concepts to be fetched: {', '.join(unknown_concepts)}"
				)
				sys.exit(1)

			# Do fetch
			fetcher.fetchConceptEntries(
				fetched_concept_dir,
				flavor=flavor,
				concepts=args.concepts,
				do_spread=True,
				keep_monolithic=False,
				override_cache=args.override_cache,
			)

		if (args.input_ids_files is not None) and len(args.input_ids_files) > 0:
			input_ids = []

			for input_ids_filename in args.input_ids_files:
				logger.info(f"Reading input ids file {input_ids_filename}")
				with open(input_ids_filename, mode="r", encoding="utf-8") as iiF:
					for line in iiF:
						input_id = line.strip()
						# Skip commented out lines
						if input_id.startswith("#"):
							continue

						input_ids.append(input_id)

			if len(input_ids) == 0:
				logger.error(
					f"No identifiers were read from the provided input ids files"
				)
				sys.exit(1)

			fetcher.fetchEntriesFromIdsIntoDirectory(
				input_ids,
				fetched_concept_dir,
				flavor=flavor,
				override_cache=args.override_cache,
			)

		inputs.append(fetched_concept_dir)

	# Time to obtain and filter the inputs
	newer_than: "Optional[float]"
	if args.newer_than is not None:
		try:
			newer_than = float(args.newer_than)
		except ValueError as ve:
			newer_than = datetime.datetime.fromisoformat(args.newer_than).timestamp()
	elif args.newer_than_file is not None:
		newer_than = os.path.getmtime(args.newer_than_file)
	else:
		newer_than = None

	ignore_entries: "Optional[MutableSequence[str]]" = None
	if args.ids_to_ignore_files is not None:
		ignore_entries = []
		for ids_to_ignore_file in args.ids_to_ignore_files:
			with open(ids_to_ignore_file, mode="r", encoding="utf-8") as iH:
				for line in iH:
					hashidx = line.find("#")
					if hashidx != -1:
						line = line[0:hashidx]
					line = line.strip()
					if len(line) > 0:
						ignore_entries.append(line)

	logger.debug("Migration starts")
	written_files, _ = migrateInputs(
		cast(
			"Iterable[Mapping[str, Any]]",
			processInputs(
				inputs,
				newer_than=newer_than,
				ignore_entries=ignore_entries,
				logger=logger,
			),
		),
		recipesH,
		args.outputDir,
		logger,
	)
	logger.debug(f"Migration ends. {len(written_files)} files written.")

	# Should we validate?
	if args.doValidate and len(written_files) > 0:
		logger.info(f"Validating migrated entries using model release {args.bdmTag}")

		entries = list(processInputs(written_files, yieldFName=True, logger=logger))

		schemas_manager = OEBSchemasManager(fetcher)
		# Fetching benchmarking data model, in case
		# it is not fetched yet
		fetch_schemas = (args.bdmDir is None) or not os.path.isdir(args.bdmDir)
		if not fetch_schemas:
			logger.info(f"Checking directory {args.bdmDir} with contents")
			for entry in os.scandir(args.bdmDir):
				if (
					entry.is_file()
					and not entry.name.startswith(".")
					and (entry.name.endswith(".json") or entry.name.endswith(".yaml"))
				):
					break
			else:
				fetch_schemas = True
		if fetch_schemas:
			logger.info("Schemas being fetched")
			data_model_dir = schemas_manager.checkoutSchemas(
				checkoutDir=args.bdmDir,
				tag=args.bdmTag,
				fetchFromREST=args.trustREST,
				override_cache=args.override_cache,
			)
		else:
			data_model_dir = args.bdmDir

		schema_prefix = schemas_manager.validate(
			entries,
			args.reportFilename,
			flavor=FLAVOR_STAGED,
			data_model_dir=data_model_dir,
			override_cache=args.override_cache,
		)


if __name__ == "__main__":
	main()
