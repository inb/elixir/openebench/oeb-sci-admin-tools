#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2024 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import atexit
import json
import logging
import sys

from typing import TYPE_CHECKING

if TYPE_CHECKING:
	from typing import (
		Mapping,
		Union,
	)

from . import (
	gen_base_param_parser,
	parse_args_and_init_app,
)

from ..sandbox import OEBSandboxManager


def main() -> None:
	ap = gen_base_param_parser(
		description="oeb-sandbox", add_cache=False, add_report_flags=True
	)

	ap.add_argument(
		"command",
		help="Sandbox command to run",
		choices=["status", "discard", "dry-run", "stage"],
	)

	# Let's do it!
	args, sandbox, _flavor, _oeb_api_creds = parse_args_and_init_app(
		ap, clazz=OEBSandboxManager
	)
	logger = logging.getLogger("oeb-sandbox")

	# Report file
	if args.reportFilename == "-":
		rH = sys.stdout.buffer
	else:
		rH = open(args.reportFilename, mode="wb")
		atexit.register(rH.close)

	if args.command == "status":
		logger.info(f"Current status of your sandbox at {args.baseUrl}")
		sandbox.report(rH)
	elif args.command == "discard":
		logger.info(f"Discarding your sandbox at {args.baseUrl}")
		sandbox.discard(rH)
	elif args.command == "dry-run":
		logger.info(f"Dry-run staging your sandbox at {args.baseUrl}")
		sandbox.stage(rH, dryRun=True)
	elif args.command == "stage":
		logger.info(f"Staging your sandbox at {args.baseUrl}")
		transferred_entries = sandbox.stage(rH, dryRun=False)
		print("\n* Transferred entries:")
		for t_entry in transferred_entries:
			print(f"\t{t_entry['input_id']} -> {t_entry['_id']} ({t_entry['orig_id']})")


if __name__ == "__main__":
	main()
