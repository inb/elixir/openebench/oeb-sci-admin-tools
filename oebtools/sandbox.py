#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2023 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import os
import shutil
import urllib.error
import urllib.parse
import urllib.request

from typing import (
	cast,
	TYPE_CHECKING,
)

if TYPE_CHECKING:
	from types import ModuleType
	from typing import (
		IO,
		Mapping,
		Optional,
		Sequence,
		Union,
	)

	from .auth import (
		OEBCredentials,
	)

from .common import FLAVOR_SANDBOX
from .uploader import OEBUploader

FLAVOR_EXEC_STAGE = "execute"
CONCEPT_EXEC_STAGE = "migrate"


class OEBSandboxManager(OEBUploader):
	def __init__(
		self,
		baseUrl: "str",
		oeb_credentials: "Optional[Union[OEBCredentials, str]]" = None,
		cache_entry_expire: "Optional[Union[int, float]]" = None,
		cache_dir: "Optional[str]" = None,
		raw_cache_dir: "Optional[str]" = None,
	):
		super().__init__(
			baseUrl,
			oeb_credentials=oeb_credentials,
			cache_entry_expire=cache_entry_expire,
			cache_dir=cache_dir,
			raw_cache_dir=raw_cache_dir,
		)

	def report(self, outputH: "IO[bytes]") -> None:
		sandboxReq = self.oebRequest(flavor=FLAVOR_SANDBOX)

		self.logger.debug(f"{sandboxReq.get_method()} {sandboxReq.get_full_url()}")
		try:
			with urllib.request.urlopen(sandboxReq) as ir:
				irbytes = ir.read()
				self.logger.debug("Server result")
				self.logger.debug(irbytes.decode())
				outputH.write(irbytes)
		except urllib.error.HTTPError as he:
			irbytes = he.read()
			outputH.write(irbytes)
			self.logger.error(f"Error {he.code} {he.reason} . Server report:")
			self.logger.exception(irbytes.decode())
			raise he

	def discard(self, outputH: "IO[bytes]") -> None:
		sandboxReq = self.oebRequest(flavor=FLAVOR_SANDBOX, method="DELETE")

		self.logger.debug(f"{sandboxReq.get_method()} {sandboxReq.get_full_url()}")
		try:
			with urllib.request.urlopen(sandboxReq) as ir:
				irbytes = ir.read()
				self.logger.debug("Server result")
				self.logger.debug(irbytes.decode())
				outputH.write(irbytes)
		except urllib.error.HTTPError as he:
			irbytes = he.read()
			outputH.write(irbytes)
			self.logger.error(f"Error {he.code} {he.reason} . Server report:")
			self.logger.exception(irbytes.decode())
			raise he

	def stage(
		self, outputH: "IO[bytes]", dryRun: "bool" = True
	) -> "Sequence[Mapping[str, str]]":
		params = None
		if not dryRun:
			params = {"dryrun": "false"}
		sandboxReq = self.oebRequest(
			flavor=FLAVOR_EXEC_STAGE,
			concept=CONCEPT_EXEC_STAGE,
			method="GET",
			params=params,
		)

		self.logger.debug(f"{sandboxReq.get_method()} {sandboxReq.get_full_url()}")
		try:
			with urllib.request.urlopen(sandboxReq) as ir:
				irbytes = ir.read()
				self.logger.debug("Server result")
				self.logger.debug(irbytes.decode())
				outputH.write(irbytes)

				return cast("Sequence[Mapping[str, str]]", json.loads(irbytes.decode()))
		except urllib.error.HTTPError as he:
			irbytes = he.read()
			outputH.write(irbytes)
			self.logger.error(f"Error {he.code} {he.reason} . Server report:")
			self.logger.exception(irbytes.decode())
			raise he
