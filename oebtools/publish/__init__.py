#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2023 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import abc
import inspect
import logging
from typing import TYPE_CHECKING

if TYPE_CHECKING:
	from typing import (
		Any,
		IO,
		Mapping,
		Optional,
		Tuple,
		Union,
	)

	from typing_extensions import (
		Final,
	)


class AbstractPublisher(abc.ABC):
	def __init__(self, sandbox: "bool", token: "str", server: "Optional[str]" = None):
		"""
		server is optional because in many cases the publisher
		URLs are pre-defined, like in the cases of
		B2SHARE, Zenodo or osf.io
		"""
		self.logger = logging.getLogger(
			dict(inspect.getmembers(self))["__module__"]
			+ "::"
			+ self.__class__.__name__
		)
		self.sandbox = sandbox
		self.token = token
		self.server = server

	@classmethod
	@abc.abstractmethod
	def NAME(cls) -> "str":
		pass

	@abc.abstractmethod
	def book_pid(
		self, initial_community_specific_metadata: "Optional[Mapping[str, Any]]" = None
	) -> "Tuple[str, str, Mapping[str, Any]]":
		"""
		It returns the publisher internal draft id, the public DOI / link, and the draft record representation
		"""
		pass

	@abc.abstractmethod
	def upload_file_to_draft(
		self,
		draft_record: "Mapping[str, Any]",
		filename: "Union[str, IO[bytes]]",
		remote_filename: "Optional[str]",
	) -> "Mapping[str, Any]":
		"""
		It takes as input the draft record representation, a local filename and optionally the remote filename to use
		"""
		pass

	@abc.abstractmethod
	def update_record_metadata(
		self,
		record: "Mapping[str, Any]",
		metadata: "Mapping[str, Any]",
		community_specific_metadata: "Optional[Mapping[str, Any]]" = None,
	) -> "Mapping[str, Any]":
		"""
		This method updates the (draft or not) record metadata,
		both the general one, and the specific of the community.
		This one could not make sense for some providers.
		"""
		pass

	@abc.abstractmethod
	def publish_draft_record(
		self, draft_record: "Mapping[str, Any]"
	) -> "Mapping[str, Any]":
		"""
		This method publishes a draft record
		"""
		pass
