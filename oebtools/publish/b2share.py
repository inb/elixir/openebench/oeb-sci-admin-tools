#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2023 Barcelona Supercomputing Center, José M. Fernández
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from . import AbstractPublisher

import datetime
import json
import os
from typing import (
	cast,
	TYPE_CHECKING,
)
import urllib.parse
import urllib.request
import uuid

if TYPE_CHECKING:
	from typing import (
		Any,
		IO,
		Mapping,
		MutableMapping,
		MutableSequence,
		Optional,
		Tuple,
		Union,
	)

	from typing_extensions import (
		Final,
	)


class B2SHAREPublisher(AbstractPublisher):
	"""
	See https://eudat.eu/services/userdoc/b2share-http-rest-api
	"""

	B2SHARE_API_PREFIX: "Final[str]" = "https://b2share.eudat.eu/api/"
	SANDBOX_B2SHARE_API_PREFIX: "Final[str]" = "https://trng-b2share.eudat.eu/api/"

	OPENEBENCH_B2SHARE_COMMUNITY_ID: "Final[str]" = (
		"591815be-a801-47c5-80e2-0a39f95c7def"
	)
	SANDBOX_OPENEBENCH_B2SHARE_COMMUNITY_ID: "Final[str]" = (
		"f60ff069-c8fa-4b48-8442-903bffa3acb1"
	)

	def __init__(self, sandbox: "bool", token: "str", server: "Optional[str]" = None):
		super().__init__(
			sandbox=sandbox,
			token=token,
			server=server,
		)

		if self.sandbox:
			self.api_prefix = self.SANDBOX_B2SHARE_API_PREFIX
			self.community_id = self.SANDBOX_OPENEBENCH_B2SHARE_COMMUNITY_ID
		else:
			self.api_prefix = self.B2SHARE_API_PREFIX
			self.community_id = self.OPENEBENCH_B2SHARE_COMMUNITY_ID

	@classmethod
	def NAME(cls) -> "str":
		return "b2share"

	def _get_community_api_prefix(self) -> "str":
		return (
			self.api_prefix
			+ f"communities/{urllib.parse.quote_plus(self.community_id)}"
		)

	def _get_community_metadata(self) -> "Mapping[str, Any]":
		req = urllib.request.Request(self._get_community_api_prefix())
		with urllib.request.urlopen(req) as res:
			return cast("Mapping[str, Any]", json.load(res))

	def _get_community_schema(self) -> "Tuple[Mapping[str, Any], Optional[str]]":
		community_metadata = self._get_community_metadata()

		community_specific_uuid = community_metadata.get("schema", {}).get(
			"block_schema_id"
		)

		# Now, let's build the schema URL
		req = urllib.request.Request(community_metadata["links"]["schemas"] + "/last")
		with urllib.request.urlopen(req) as res:
			community_schema_metadata = json.load(res)

		return community_schema_metadata, community_specific_uuid

	def _get_records_prefix(self) -> "str":
		return self.api_prefix + f"records/"

	def _get_query_params(self, include_credentials: "bool", **kwargs: "str") -> "str":
		query_params = {key: value for key, value in kwargs.items()}
		if include_credentials:
			query_params["access_token"] = self.token

		return urllib.parse.urlencode(query_params, encoding="utf-8")

	def _create_draft_record(
		self, community_specific_metadata: "Optional[Mapping[str, Any]]"
	) -> "Mapping[str, Any]":
		headers = {
			"Content-Type": "application/json",
		}
		minimal_metadata: "MutableMapping[str, Any]" = {
			"titles": [
				{
					"title": f"Draft record created at {datetime.datetime.utcnow().isoformat()}"
				}
			],
			"community": self.community_id,
			"open_access": True,
		}
		if community_specific_metadata is not None:
			(
				community_schema_metadata,
				community_specific_uuid,
			) = self._get_community_schema()
			if community_specific_uuid is not None:
				minimal_metadata["community_specific"][
					community_specific_uuid
				] = community_specific_metadata

		req = urllib.request.Request(
			self._get_records_prefix()
			+ "?"
			+ self._get_query_params(include_credentials=True),
			headers=headers,
			data=json.dumps(minimal_metadata).encode("utf-8"),
		)

		with urllib.request.urlopen(req) as creares:
			response = json.load(creares)

		return cast("Mapping[str, Any]", response)

	def book_pid(
		self, initial_community_specific_metadata: "Optional[Mapping[str, Any]]" = None
	) -> "Tuple[str, str, Mapping[str, Any]]":
		"""
		It returns the publisher internal draft id, the public DOI / link, and the draft record representation
		"""
		draft_record = self._create_draft_record(
			community_specific_metadata=initial_community_specific_metadata
		)

		return draft_record["id"], draft_record["metadata"]["$future_doi"], draft_record

	def _get_file_bucket_prefix_from_draft(
		self, draft_record: "Mapping[str, Any]"
	) -> "str":
		return cast("str", draft_record["links"]["files"])

	def _get_record_prefix_from_record(self, record: "Mapping[str, Any]") -> "str":
		return cast("str", record["links"]["self"])

	def upload_file_to_draft(
		self,
		draft_record: "Mapping[str, Any]",
		filename: "Union[str, IO[bytes]]",
		remote_filename: "Optional[str]",
	) -> "Mapping[str, Any]":
		"""
		It takes as input the draft record representation, a local filename and optionally the remote filename to use
		"""
		file_bucket_prefix = self._get_file_bucket_prefix_from_draft(draft_record)

		# file_size = os.stat(filename).st_size
		headers = {
			"Accept": "application/json",
			"Content-Type": "application/octet-stream",
			# 	"Content-Length": str(file_size),
		}

		if remote_filename is None:
			assert isinstance(
				filename, str
			), "When filename is a data stream, remote_filename must be declared"
			remote_filename = os.path.basename(filename)

		fH: "IO[bytes]"
		if isinstance(filename, str):
			fH = open(filename, mode="rb")
		else:
			fH = filename

		try:
			req = urllib.request.Request(
				file_bucket_prefix
				+ "/"
				+ urllib.parse.quote_plus(remote_filename)
				+ "?"
				+ self._get_query_params(include_credentials=True),
				headers=headers,
				data=fH,
				method="PUT",
			)

			with urllib.request.urlopen(req) as answer:
				upload_response = json.load(answer)

			return cast("Mapping[str, Any]", upload_response)
		finally:
			if fH != filename:
				fH.close()

	@staticmethod
	def __patch_ops(
		draft_record_metadata: "Mapping[str, Any]",
		metadata: "Mapping[str, Any]",
		patch_ops: "MutableSequence[Mapping[str, Any]]" = [],
		prefix: "str" = "/",
	) -> "MutableSequence[Mapping[str, Any]]":
		"""
		Generator of JSON Patch operations
		"""
		for key, val in metadata.items():
			if val is None:
				patch_ops.append(
					{
						"op": "remove",
						"path": prefix + key,
					}
				)
			elif key in draft_record_metadata:
				# Is it a list?
				# if isinstance(draft_record_metadata[key], list):
				# 	# Is the value a list itself
				# 	if isinstance(val, list):
				# 		# Let's concatenate
				# 		the_val = val
				# 	else:
				# 		the_val = [ val ]
				#
				# 	for a_idx, a_val in enumerate(the_val, len(draft_record_metadata[key])):
				# 		patch_ops.append({
				# 			"op": "add",
				# 			"path": prefix + key + '/' + str(a_idx),
				# 			"value": a_val,
				# 		})
				# else:
				patch_ops.append(
					{
						"op": "replace",
						"path": prefix + key,
						"value": val,
					}
				)
			else:
				patch_ops.append(
					{
						"op": "add",
						"path": prefix + key,
						"value": val,
					}
				)

		return patch_ops

	def update_record_metadata(
		self,
		record: "Mapping[str, Any]",
		metadata: "Mapping[str, Any]",
		community_specific_metadata: "Optional[Mapping[str, Any]]" = None,
	) -> "Mapping[str, Any]":
		"""
		This method updates the (draft or not) record metadata,
		both the general one, and the specific of the community.
		This one could not make sense for some providers.
		"""

		patch_ops = self.__patch_ops(record["metadata"], metadata)
		if community_specific_metadata is not None:
			(
				community_schema_metadata,
				community_specific_uuid,
			) = self._get_community_schema()
			if community_specific_uuid is not None:
				patch_ops = self.__patch_ops(
					record["metadata"]["community_specific"].get(
						community_specific_uuid, {}
					),
					community_specific_metadata,
					patch_ops=patch_ops,
					prefix="/community_specific/" + community_specific_uuid + "/",
				)

		if len(patch_ops) > 0:
			record_url = self._get_record_prefix_from_record(record)
			headers = {
				"Content-Type": "application/json-patch+json",
			}
			req = urllib.request.Request(
				record_url + "?" + self._get_query_params(include_credentials=True),
				headers=headers,
				data=json.dumps(patch_ops).encode("utf-8"),
				method="PATCH",
			)
			try:
				with urllib.request.urlopen(req) as answer:
					updated_record = json.load(answer)
			except urllib.error.HTTPError as he:
				self.logger.exception(
					"PATCH ERROR BODY:\n" + json.dumps(json.load(he.fp), indent=4)
				)
				raise he

			return cast("Mapping[str, Any]", updated_record)
		else:
			return record

	def publish_draft_record(
		self, draft_record: "Mapping[str, Any]"
	) -> "Mapping[str, Any]":
		"""
		This method publishes a draft record
		"""
		published_record = self.update_record_metadata(
			draft_record,
			metadata={
				"publication_state": "submitted",
			},
		)

		return published_record
